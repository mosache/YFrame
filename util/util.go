package util

import (
	"github.com/mozillazg/go-pinyin"
	"strings"
)

func GetFirstWordPinYinLetter(src string) string {
	args := pinyin.Args{
		Style:     pinyin.FIRST_LETTER,
		Heteronym: false,
		Separator: "",
		Fallback:  nil,
	}

	fw := pinyin.Pinyin(src, args)

	if len(fw) == 0 {
		return ""
	}

	if len(fw[0]) == 0 {
		return ""
	}

	return strings.ToUpper(fw[0][0])
}

func GetWholeNamePinYinLetter(src string) string {
	args := pinyin.Args{
		Style:     pinyin.Normal,
		Heteronym: false,
		Separator: "",
		Fallback:  nil,
	}

	fw := pinyin.Pinyin(src, args)

	if len(fw) == 0 {
		return ""
	}

	wordLetters := make([]string, 0)

	for _, v := range fw {
		wordLetters = append(wordLetters, v[0])
	}

	if len(fw[0]) == 0 {
		return ""
	}

	return strings.ToUpper(strings.Join(wordLetters, ""))
}
