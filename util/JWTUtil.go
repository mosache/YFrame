package util

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
)

const (
	KEY = "JWTKEY"
)

func NewJWT(data jwt.Claims, key ...string) string {
	var k string
	if len(key) == 0 {
		k = KEY
	} else {
		k = key[0]
	}
	var token *jwt.Token
	if data == nil {
		token = jwt.New(jwt.SigningMethodHS256)
	} else {
		token = jwt.NewWithClaims(jwt.SigningMethodHS256, data)
	}
	signedString, err := token.SignedString([]byte(k))
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return signedString
}

func VerityToken(token string, data jwt.Claims, key ...string) bool {
	var k string
	if len(key) == 0 {
		k = KEY
	} else {
		k = key[0]
	}
	t, err := jwt.ParseWithClaims(token, data, func(token *jwt.Token) (interface{}, error) {
		return []byte(k), nil
	})

	if err != nil {
		return false
	}

	return t.Valid
}

func VerityTokenWithErr(token string, data jwt.Claims, key ...string) error {
	var k string
	if len(key) == 0 {
		k = KEY
	} else {
		k = key[0]
	}
	t, err := jwt.ParseWithClaims(token, data, func(token *jwt.Token) (interface{}, error) {
		return []byte(k), nil
	})

	if err != nil {
		return err
	}

	if !t.Valid {
		return errors.New("token is valid")
	}

	return nil
}
