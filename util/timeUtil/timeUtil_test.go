package timeUtil

import (
	"testing"
	"time"
)

func TestTime(t *testing.T) {
	sub := time.Now().Sub(time.Date(2023, 6, 7, 8, 0, 0, 0, time.Local))
	t.Log(sub)
}

func TestGetAllMonthWeek(t *testing.T) {
	week := GetAllMonthWeek(2024, 6, time.Sunday)
	t.Log(week)
}
