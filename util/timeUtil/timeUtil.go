package timeUtil

import (
	"strconv"
	"time"
)

var (
	MaxDayOfMonth = map[int]int{
		1:  31,
		2:  29,
		3:  31,
		4:  30,
		5:  31,
		6:  30,
		7:  31,
		8:  31,
		9:  30,
		10: 31,
		11: 30,
		12: 31,
	}
)

/*
获取月份的最后一天
*/
func GetMonthMaxDay(year, month int) int {
	if month != 2 {
		return MaxDayOfMonth[month]
	} else {
		/// 判断是否是闰年
		if year%4 == 0 {
			return 29
		} else {
			return 28
		}
	}
}

type MonthWeek struct {
	StartUnix int64
	EndUnix   int64
}

func GetMonthWeek(nowYear, nowMonth int) []MonthWeek {
	r := make([]MonthWeek, 0)

	day1WeekDay := time.Date(nowYear, time.Month(nowMonth), 1, 0, 0, 0, 0, time.Local).Weekday()

	var startDay = 1
	///  sunday
	if day1WeekDay == time.Sunday {
		startDay = 2
	} else if day1WeekDay == time.Monday {
		startDay = 1
	} else {
		startDay = 1 + (8 - int(day1WeekDay))
	}

	week1StartUnix := time.Date(nowYear, time.Month(nowMonth), startDay, 0, 0, 0, 0, time.Local).Unix()
	week1EndUnix := time.Date(nowYear, time.Month(nowMonth), startDay+6, 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week1StartUnix,
		EndUnix:   week1EndUnix,
	})

	week2StartUnix := time.Date(nowYear, time.Month(nowMonth), startDay+7, 0, 0, 0, 0, time.Local).Unix()
	week2EndUnix := time.Date(nowYear, time.Month(nowMonth), startDay+13, 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week2StartUnix,
		EndUnix:   week2EndUnix,
	})

	week3StartUnix := time.Date(nowYear, time.Month(nowMonth), startDay+14, 0, 0, 0, 0, time.Local).Unix()
	week3EndUnix := time.Date(nowYear, time.Month(nowMonth), startDay+20, 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week3StartUnix,
		EndUnix:   week3EndUnix,
	})

	week4StartUnix := time.Date(nowYear, time.Month(nowMonth), startDay+21, 0, 0, 0, 0, time.Local).Unix()
	var week4EndUnix int64
	if startDay+27 > GetMonthMaxDay(nowYear, nowMonth) {
		var yearAdded = 0
		if nowMonth == 12 {
			yearAdded = 1
		}
		week4EndUnix = time.Date(nowYear+yearAdded, time.Month((nowMonth+1)%12), (startDay+27)-GetMonthMaxDay(nowYear, nowMonth), 23, 59, 59, 0, time.Local).Unix()
	} else {
		week4EndUnix = time.Date(nowYear, time.Month(nowMonth), GetMonthMaxDay(nowYear, nowMonth), 23, 59, 59, 0, time.Local).Unix()
	}
	r = append(r, MonthWeek{
		StartUnix: week4StartUnix,
		EndUnix:   week4EndUnix,
	})

	/// 可能有week5
	if startDay+28 <= GetMonthMaxDay(nowYear, nowMonth) {
		week5tartUnix := time.Date(nowYear, time.Month(nowMonth), startDay+28, 0, 0, 0, 0, time.Local).Unix()
		var week5EndUnix int64
		var yearAdded = 0
		if nowMonth == 12 {
			yearAdded = 1
		}
		week5EndUnix = time.Date(nowYear+yearAdded, time.Month((nowMonth+1)%12), (startDay+34)-GetMonthMaxDay(nowYear, nowMonth), 23, 59, 59, 0, time.Local).Unix()
		r = append(r, MonthWeek{
			StartUnix: week5tartUnix,
			EndUnix:   week5EndUnix,
		})
	}

	return r
}

func GetMonthWeek2(nowYear, nowMonth int) []MonthWeek {
	r := make([]MonthWeek, 0)

	week1StartUnix := time.Date(nowYear, time.Month(nowMonth), 1, 0, 0, 0, 0, time.Local).Unix()
	week1EndUnix := time.Date(nowYear, time.Month(nowMonth), 7, 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week1StartUnix,
		EndUnix:   week1EndUnix,
	})

	week2StartUnix := time.Date(nowYear, time.Month(nowMonth), 8, 0, 0, 0, 0, time.Local).Unix()
	week2EndUnix := time.Date(nowYear, time.Month(nowMonth), 15, 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week2StartUnix,
		EndUnix:   week2EndUnix,
	})

	week3StartUnix := time.Date(nowYear, time.Month(nowMonth), 16, 0, 0, 0, 0, time.Local).Unix()
	week3EndUnix := time.Date(nowYear, time.Month(nowMonth), 22, 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week3StartUnix,
		EndUnix:   week3EndUnix,
	})

	week4StartUnix := time.Date(nowYear, time.Month(nowMonth), 23, 0, 0, 0, 0, time.Local).Unix()
	week4EndUnix := time.Date(nowYear, time.Month(nowMonth), GetMonthMaxDay(nowYear, nowMonth), 23, 59, 59, 0, time.Local).Unix()
	r = append(r, MonthWeek{
		StartUnix: week4StartUnix,
		EndUnix:   week4EndUnix,
	})

	return r
}

func GetAgeWithIdentificationNumber(idCard string) int {
	birYear, _ := strconv.Atoi(Substr(idCard, 6, 4))
	birMonth, _ := strconv.Atoi(Substr(idCard, 10, 2))
	age := time.Now().Year() - birYear
	if int(time.Now().Month()) < birMonth {
		age--
	}
	return age
}

func Substr(str string, start, length int) string {

	rs := []rune(str)
	rl := len(rs)
	end := 0
	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}
	return string(rs[start:end])
}

/*
获取指定月的开始和结束的时间戳
unix 输入的时间戳
*/
func GetMonthStartAndEndUnixTime(unix int64) (int64, int64) {
	unixTime := time.Unix(unix, 0)

	year, month, _ := unixTime.Date()

	/// 当月第一天结束时间
	firstDayEnd := time.Date(year, month, 1, 23, 59, 59, 0, unixTime.Location())

	/// 当月最后一天结束时间
	lastDayEnd := firstDayEnd.AddDate(0, 1, -1)

	firstDayStart := time.Date(firstDayEnd.Year(), firstDayEnd.Month(), 1, 0, 0, 0, 0, unixTime.Location())

	return firstDayStart.Unix(), lastDayEnd.Unix()
}

/*
获取指定天的开始或结束的时间戳
flag 1 startTime 2 endTime
unix 输入的时间戳
*/
func GetDateStartOrEndUnixTime(unix int64, needMilliTime bool) (int64, int64) {

	unixTime := time.Unix(unix, 0)

	sUnixTime := time.Date(unixTime.Year(), unixTime.Month(), unixTime.Day(), 0, 0, 0, 0, unixTime.Location())
	eUnixTime := time.Date(unixTime.Year(), unixTime.Month(), unixTime.Day(), 23, 59, 59, 0, unixTime.Location())

	if needMilliTime {
		return sUnixTime.UnixMilli(), eUnixTime.UnixMilli()
	}
	return sUnixTime.Unix(), eUnixTime.Unix()
}

/*
是否是今天
*/
func IsToday(unix int64) bool {
	unixTime := time.Unix(unix, 0)
	now := time.Now()
	return unixTime.Year() == now.Year() && unixTime.Month() == now.Month() && unixTime.Day() == now.Day()
}

/*
是否是昨天
*/
func IsYesterday(unix int64) bool {
	unixTime := time.Unix(unix, 0)
	yesterday := time.Now().Add(-24 * time.Hour)
	return unixTime.Year() == yesterday.Year() && unixTime.Month() == yesterday.Month() && unixTime.Day() == yesterday.Day()
}

/*
是否是本周
*/
func IsThisWeek(unix int64) bool {
	unixTime := time.Unix(unix, 0)
	now := time.Now()
	year1, week1 := unixTime.ISOWeek()
	year2, week2 := now.ISOWeek()
	return year1 == year2 && week1 == week2
}

/*
是否是当月
*/
func IsThisMonth(unix int64) bool {
	unixTime := time.Unix(unix, 0)
	now := time.Now()
	return unixTime.Year() == now.Year() && unixTime.Month() == now.Month()
}

/*
获取指定年的开始和结束的时间戳
unix 输入的时间戳
*/
func GetYearStartAndEndUnixTime(unix int64) (int64, int64) {
	unixTime := time.Unix(unix, 0)

	year, _, _ := unixTime.Date()

	startTime := time.Date(year, 1, 1, 0, 0, 0, 0, unixTime.Location())

	endTime := time.Date(year, 12, 31, 23, 59, 59, 0, unixTime.Location())

	return startTime.Unix(), endTime.Unix()
}

func GetWeekDay(unix int64) int {
	return int(time.Unix(unix, 0).Weekday())
}

func GetWeekDayStr(unix int64) string {
	return weekDayMap[int(time.Unix(unix, 0).Weekday())]
}

var (
	weekDayMap = map[int]string{
		0: "星期日",
		1: "星期一",
		2: "星期二",
		3: "星期三",
		4: "星期四",
		5: "星期五",
		6: "星期六",
	}
)

/*
获取月的所有周，包含跨月的
*/
func GetAllMonthWeek(nowYear, nowMonth int, weekStartDay time.Weekday) []MonthWeek {
	r := make([]MonthWeek, 0)

	day1Time := time.Date(nowYear, time.Month(nowMonth), 1, 0, 0, 0, 0, time.Local)
	day1WeekDay := day1Time.Weekday()

	var deltaDay int

	if day1WeekDay != weekStartDay {
		deltaDay = int(weekStartDay - day1WeekDay)
	}

	var (
		weekStartUnix = day1Time.AddDate(0, 0, deltaDay).Unix()
		weekEndUnix   = time.Unix(weekStartUnix, 0).AddDate(0, 0, 7).Unix() - 1
	)

	r = append(r, MonthWeek{
		StartUnix: weekStartUnix,
		EndUnix:   weekEndUnix,
	})

	_, et := GetMonthStartAndEndUnixTime(day1Time.Unix())

	for weekEndUnix < et {
		weekStartUnix = time.Unix(weekStartUnix, 0).AddDate(0, 0, 7).Unix()
		weekEndUnix = time.Unix(weekStartUnix, 0).AddDate(0, 0, 7).Unix() - 1

		r = append(r, MonthWeek{
			StartUnix: weekStartUnix,
			EndUnix:   weekEndUnix,
		})
	}

	return r
}
