package rand

import (
	"fmt"
	"math/rand"
	"time"
)

/*
随机生成4位数的随机数字
*/
func CreateCaptcha4() string {
	return fmt.Sprintf("%04v", rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(10000))
}

/*
随机生成6位数的随机数字
*/
func CreateCaptcha6() string {
	return fmt.Sprintf("%06v", rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(1000000))
}

/*
随机生成12位数的随机数字
*/
func CreateCaptcha12() string {
	return fmt.Sprintf("%012v", rand.New(rand.NewSource(time.Now().UnixNano())).Int63n(100000000000))
}
