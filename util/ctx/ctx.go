package ctx

import (
	"context"
	"time"
)

type Result struct {
	Ctx        context.Context
	CancelFunc context.CancelFunc
}

func WithTimeOut(timeOut time.Duration) Result {
	ctx, cancelFunc := context.WithTimeout(context.Background(), timeOut)
	return Result{
		Ctx:        ctx,
		CancelFunc: cancelFunc,
	}
}
