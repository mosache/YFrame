package str

import "strconv"

func ToInt64(source string) int64 {
	var r int64

	r = 0

	if n, err := strconv.Atoi(source); err == nil {
		r = int64(n)
	}

	return r
}

func ToFloat64(source string) float64 {
	var (
		r   float64
		err error
	)

	if r, err = strconv.ParseFloat(source, 64); err != nil {
		return 0
	}

	return r
}
