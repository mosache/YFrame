package errs

import (
	"fmt"
)

func ErrKeyNotFound(key string) error {
	return fmt.Errorf("[cache]: key(%s) NOT FOUND", key)
}
