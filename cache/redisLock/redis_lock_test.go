package redisLock

import (
	"context"
	"gitee.com/mosache/YFrame/cache/redisLock/mocks"
	"github.com/golang/mock/gomock"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestClient_TryLock(t *testing.T) {
	testCases := []struct {
		name string
		mock func(ctrl *gomock.Controller) redis.Cmdable
		key  string

		wantErr  error
		wantLock *Lock
	}{
		{
			name: "set nx err",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewBoolResult(false, context.DeadlineExceeded)
				cmd.EXPECT().SetNX(context.Background(), "key1", gomock.Any(), time.Minute).
					Return(res)

				return cmd
			},
			key:     "key1",
			wantErr: context.DeadlineExceeded,
		},

		{
			name: "fail to get lock",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewBoolResult(false, nil)
				cmd.EXPECT().SetNX(context.Background(), "key1", gomock.Any(), time.Minute).
					Return(res)

				return cmd
			},
			key:     "key1",
			wantErr: ErrorFailToPreemptLock,
		},

		{
			name: "success",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewBoolResult(true, nil)
				cmd.EXPECT().SetNX(context.Background(), "key1", gomock.Any(), time.Minute).
					Return(res)

				return cmd
			},
			key:      "key1",
			wantLock: &Lock{key: "key1"},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			client := New(tc.mock(ctrl))
			lock, err := client.TryLock(context.Background(), tc.key, time.Minute)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantLock.key, lock.key)
			assert.NotEmpty(t, lock.val)
		})
	}
}

func TestLock_UnLock(t *testing.T) {
	testCases := []struct {
		name string

		mock  func(ctrl *gomock.Controller) redis.Cmdable
		key   string
		value string

		wantErr error
	}{
		{
			name: "eval err",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewCmd(context.Background())
				res.SetErr(context.DeadlineExceeded)
				cmd.EXPECT().Eval(context.Background(), unlockLua, []string{"key1"}, []any{"value"}).
					Return(res)

				return cmd
			},
			key:     "key1",
			value:   "value",
			wantErr: context.DeadlineExceeded,
		},

		{
			name: "lock not exist",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewCmd(context.Background())
				res.SetVal(int64(0))
				cmd.EXPECT().Eval(context.Background(), unlockLua, []string{"key1"}, []any{"value"}).
					Return(res)

				return cmd
			},
			key:     "key1",
			value:   "value",
			wantErr: ErrorLockNotExist,
		},

		{
			name: "get lock",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewCmd(context.Background())
				res.SetVal(int64(1))
				cmd.EXPECT().Eval(context.Background(), unlockLua, []string{"key1"}, []any{"value"}).
					Return(res)

				return cmd
			},
			key:   "key1",
			value: "value",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			lock := &Lock{
				client: tc.mock(ctrl),
				key:    tc.key,
				val:    tc.value,
			}
			err := lock.UnLock(context.Background())
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func TestLock_Refresh(t *testing.T) {
	testCases := []struct {
		name string

		mock  func(ctrl *gomock.Controller) redis.Cmdable
		key   string
		value string

		expiration time.Duration
		wantErr    error
	}{
		{
			name: "eval err",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewCmd(context.Background())
				res.SetErr(context.DeadlineExceeded)
				cmd.EXPECT().Eval(context.Background(), refreshLua, []string{"key1"}, []any{"value", float64(60)}).
					Return(res)

				return cmd
			},
			key:        "key1",
			value:      "value",
			expiration: time.Minute,
			wantErr:    context.DeadlineExceeded,
		},

		{
			name: "lock not exist",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewCmd(context.Background())
				res.SetVal(int64(0))
				cmd.EXPECT().Eval(context.Background(), refreshLua, []string{"key1"}, []any{"value", float64(60)}).
					Return(res)

				return cmd
			},
			key:        "key1",
			value:      "value",
			expiration: time.Minute,
			wantErr:    ErrorLockNotExist,
		},

		{
			name: "refresh lock success",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := mocks.NewMockCmdable(ctrl)
				res := redis.NewCmd(context.Background())
				res.SetVal(int64(1))
				cmd.EXPECT().Eval(context.Background(), refreshLua, []string{"key1"}, []any{"value", float64(60)}).
					Return(res)

				return cmd
			},
			key:        "key1",
			expiration: time.Minute,
			value:      "value",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			lock := &Lock{
				client:     tc.mock(ctrl),
				key:        tc.key,
				val:        tc.value,
				expiration: tc.expiration,
			}
			err := lock.Refresh(context.Background())
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
