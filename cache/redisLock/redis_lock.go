package redisLock

import (
	"context"
	_ "embed"
	"errors"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"time"
)

var (
	ErrorFailToPreemptLock = errors.New("[redis-lock] 加锁失败")
	ErrorLockNotExist      = errors.New("[redis-lock] 锁不存在")
)

//go:embed unlock.lua
var unlockLua string

//go:embed refresh.lua
var refreshLua string

type Client struct {
	client redis.Cmdable
}

func New(client redis.Cmdable) *Client {
	return &Client{client: client}
}

type Lock struct {
	client     redis.Cmdable
	key        string
	val        string
	expiration time.Duration
}

func (l *Lock) UnLock(ctx context.Context) error {
	result, err := l.client.Eval(ctx, unlockLua, []string{l.key}, l.val).Int64()
	if err != nil {
		return err
	}
	if result != 1 {
		return ErrorLockNotExist
	}
	return nil
}

/*
锁续期
*/
func (l *Lock) Refresh(ctx context.Context) error {
	result, err := l.client.Eval(ctx, refreshLua, []string{l.key}, l.val, l.expiration.Seconds()).Int64()
	if err != nil {
		return err
	}
	if result != 1 {
		return ErrorLockNotExist
	}
	return nil
}

func (c *Client) TryLock(ctx context.Context, key string, expiration time.Duration) (*Lock, error) {
	val := uuid.NewString()
	result, err := c.client.SetNX(ctx, key, val, expiration).Result()
	if err != nil {
		return nil, err
	}

	if !result {
		return nil, ErrorFailToPreemptLock
	}

	return &Lock{
		client:     c.client,
		key:        key,
		val:        val,
		expiration: expiration,
	}, nil
}
