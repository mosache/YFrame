//go:build e2e

package redisLock

import (
	"context"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestClient_e2e_TryLock(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{Addr: "127.0.0.1:6379"})

	_, err := rdb.Ping(context.Background()).Result()
	require.NoError(t, err)

	testCases := []struct {
		name   string
		before func(t *testing.T)
		after  func(t *testing.T)

		key      string
		wantErr  error
		wantLock *Lock
	}{
		{
			name: "other hold the lock",
			before: func(t *testing.T) {
				/// 模拟别人有锁
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.Set(ctx, "key1", "value1", time.Minute).Result()
				require.NoError(t, err)
				assert.Equal(t, "OK", result)
			},
			key: "key1",
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.GetDel(ctx, "key1").Result()
				require.NoError(t, err)
				assert.Equal(t, "value1", result)
			},
			wantErr: ErrorFailToPreemptLock,
		},

		{
			name:   "lock success",
			before: func(t *testing.T) {},
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.GetDel(ctx, "key2").Result()
				require.NoError(t, err)
				require.NotEmpty(t, result)
			},
			key:      "key2",
			wantLock: &Lock{key: "key2"},
		},
	}

	client := New(rdb)
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
			defer cancelFunc()

			tc.before(t)
			lock, err := client.TryLock(ctx, tc.key, time.Minute)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantLock.key, lock.key)
			assert.NotEmpty(t, lock.val)
			assert.NotNil(t, lock.client)
			tc.after(t)
		})
	}
}

func TestClient_e2e_UnLock(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{Addr: "127.0.0.1:6379"})

	_, err := rdb.Ping(context.Background()).Result()
	require.NoError(t, err)

	testCases := []struct {
		name   string
		before func(t *testing.T)
		after  func(t *testing.T)
		lock   *Lock

		wantErr error
	}{
		{
			name:   "lock not hold",
			before: func(t *testing.T) {},
			after:  func(t *testing.T) {},
			lock: &Lock{
				client: rdb,
				key:    "unLock key1",
				val:    "123",
			},
			wantErr: ErrorLockNotExist,
		},

		{
			name: "lock hold by others",
			before: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.Set(ctx, "unLock key2", "value2", time.Minute).Result()
				require.NoError(t, err)
				assert.Equal(t, "OK", result)
			},
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.GetDel(ctx, "unLock key2").Result()
				require.NoError(t, err)
				assert.Equal(t, "value2", result)
			},
			lock: &Lock{
				client: rdb,
				key:    "unLock key2",
				val:    "123",
			},
			wantErr: ErrorLockNotExist,
		},

		{
			name: "unlock success",
			before: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.Set(ctx, "unLock key3", "123", time.Minute).Result()
				require.NoError(t, err)
				assert.Equal(t, "OK", result)
			},
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.Exists(ctx, "unLock key3").Result()
				require.NoError(t, err)
				require.Equal(t, int64(0), result)
			},
			lock: &Lock{
				client: rdb,
				key:    "unLock key3",
				val:    "123",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
			defer cancelFunc()
			tc.before(t)
			err = tc.lock.UnLock(ctx)
			assert.Equal(t, tc.wantErr, err)
			tc.after(t)
			if err != nil {
				return
			}
		})
	}
}

func TestClient_e2e_Refresh(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{Addr: "127.0.0.1:6379"})

	_, err := rdb.Ping(context.Background()).Result()
	require.NoError(t, err)

	testCases := []struct {
		name   string
		before func(t *testing.T)
		after  func(t *testing.T)
		lock   *Lock

		wantErr error
	}{
		{
			name:   "lock not hold",
			before: func(t *testing.T) {},
			after:  func(t *testing.T) {},
			lock: &Lock{
				client: rdb,
				key:    "refresh key1",
				val:    "123",
			},
			wantErr: ErrorLockNotExist,
		},

		{
			name: "lock hold by others",
			before: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.Set(ctx, "refresh key2", "value2", time.Second*10).Result()
				require.NoError(t, err)
				assert.Equal(t, "OK", result)
			},
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.TTL(ctx, "refresh key2").Result()
				require.NoError(t, err)
				require.LessOrEqual(t, result, time.Second*10)

				_, err = rdb.Del(ctx, "refresh key2").Result()
				require.NoError(t, err)
			},
			lock: &Lock{
				client:     rdb,
				key:        "refresh key2",
				val:        "123",
				expiration: time.Minute,
			},
			wantErr: ErrorLockNotExist,
		},

		{
			name: "refresh success",
			before: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.Set(ctx, "refresh key3", "123", time.Minute).Result()
				require.NoError(t, err)
				assert.Equal(t, "OK", result)
			},
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
				defer cancel()
				result, err := rdb.TTL(ctx, "refresh key3").Result()
				require.NoError(t, err)
				require.Greater(t, result, time.Second*50)

				_, err = rdb.Del(ctx, "refresh key3").Result()
				require.NoError(t, err)
			},
			lock: &Lock{
				client:     rdb,
				key:        "refresh key3",
				val:        "123",
				expiration: time.Minute,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
			defer cancelFunc()
			tc.before(t)
			err = tc.lock.Refresh(ctx)
			assert.Equal(t, tc.wantErr, err)
			tc.after(t)
			if err != nil {
				return
			}
		})
	}
}
