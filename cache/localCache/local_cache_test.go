package localCache

import (
	"context"
	"gitee.com/mosache/YFrame/cache/errs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestLocalCache_Get(t *testing.T) {
	testCases := []struct {
		name    string
		c       func() *LocalCache
		key     string
		wantVal any
		wantErr error
	}{
		{
			name: "key not exist",
			c: func() *LocalCache {
				return New()
			},
			key:     "key not exist",
			wantErr: errs.ErrKeyNotFound("key not exist"),
		},

		{
			name: "get val",
			c: func() *LocalCache {
				cache := New()
				err := cache.Set(context.Background(), "key1", "key1 val", time.Second*10)
				require.NoError(t, err)
				return cache
			},
			key:     "key1",
			wantVal: "key1 val",
		},

		{
			name: "key expired",
			c: func() *LocalCache {
				cache := New()
				err := cache.Set(context.Background(), "key expired", "key1 val", time.Second)
				require.NoError(t, err)
				time.Sleep(time.Second * 3)
				return cache
			},
			key:     "key expired",
			wantErr: errs.ErrKeyNotFound("key expired"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			val, err := tc.c().Get(context.Background(), tc.key)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantVal, val)
		})
	}
}

func TestLocalCache_Loop(t *testing.T) {
	var cnt int
	c := New(WithCheckInterval(time.Second), WithEvictFunc(func(key string, val any) {
		cnt++
	}))

	err := c.Set(context.Background(), "key", 123, time.Second)
	require.NoError(t, err)

	time.Sleep(time.Second * 3)

	c.mutex.Lock()
	defer c.mutex.Unlock()
	_, ok := c.data["key"]
	assert.False(t, ok, "key must expired")

	assert.Equal(t, 1, cnt)
}
