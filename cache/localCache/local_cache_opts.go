package localCache

import "time"

func WithCheckInterval(interval time.Duration) Opt {
	return func(c *LocalCache) {
		c.checkInterval = interval
	}
}

func WithCheckLoopMaxCheckCount(maxCount int) Opt {
	return func(c *LocalCache) {
		c.checkLoopMaxCheckCount = maxCount
	}
}

func WithEvictFunc(evictFunc EvictFunc) Opt {
	return func(c *LocalCache) {
		c.evictFunc = evictFunc
	}
}
