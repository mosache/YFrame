package gzip

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestCompressAndUnCompress(t *testing.T) {
	testCases := []struct {
		name   string
		source string
	}{
		{
			name:   "noting",
			source: "",
		},
		{
			name:   "ABC",
			source: "ABC",
		},
		{
			name:   "with blank space",
			source: "a b c",
		},
		{
			name:   "withUpAndLow",
			source: "A b C",
		},
		{
			name:   "with \n \r",
			source: "a\nb\rb",
		},
	}

	c := Compressor{}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			cBs, err := c.Compress([]byte(tc.source))
			require.NoError(t, err)

			r, err := c.UnCompress(cBs)
			require.NoError(t, err)
			result := string(r)
			assert.Equal(t, result, tc.source)
		})
	}
}
