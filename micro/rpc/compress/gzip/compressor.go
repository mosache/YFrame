package gzip

import (
	"bytes"
	"compress/gzip"
	"errors"
	"io"
)

type Compressor struct {
}

func (c Compressor) Code() uint8 {
	return 1
}

func (c Compressor) Compress(data []byte) ([]byte, error) {
	res := bytes.NewBuffer(nil)
	w := gzip.NewWriter(res)
	defer w.Close()
	_, err := w.Write(data)
	if err != nil {
		return nil, err
	}
	err = w.Flush()
	if err != nil {
		return nil, err
	}

	return res.Bytes(), nil
}

func (c Compressor) UnCompress(data []byte) ([]byte, error) {
	res := bytes.NewBuffer(data)
	reader, err := gzip.NewReader(res)
	if err != nil {
		return nil, err
	}
	defer reader.Close()
	r, err := io.ReadAll(reader)
	if err != nil && !errors.Is(err, io.ErrUnexpectedEOF) {
		return nil, err
	}
	return r, nil
}
