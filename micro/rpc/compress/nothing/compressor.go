package nothing

type Compressor struct {
}

func (c Compressor) Code() uint8 {
	return 0
}

func (c Compressor) Compress(data []byte) ([]byte, error) {
	return data, nil
}

func (c Compressor) UnCompress(data []byte) ([]byte, error) {
	return data, nil
}
