package testdata

import (
	"context"
	"gitee.com/mosache/YFrame/micro/rpc/testdata/gen"
)

type UserService struct {
	GetUserByID func(ctx context.Context, req *GetUserByIDReq) (*GetUserByIDResp, error)

	GetUserByIDProto func(ctx context.Context, req *gen.GetUserByIdReq) (*gen.GetUserByIdResp, error)
}

func (u UserService) Name() string {
	return "user-service"
}

type GetUserByIDReq struct {
	ID int
}

type GetUserByIDResp struct {
	ID int
}
