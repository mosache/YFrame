package errs

import "errors"

var (
	ErrOneWayCall = errors.New("[micro] 这是一个 oneway 调用，你不应该处理任何结果")

	ErrServerOneWayCall = errors.New("[micro] 微服务服务端 oneway 请求")

	ErrServerSerializerNotFound = errors.New(" [micro] 微服务服务端未注册需要的serializer")

	ErrServerCompressorNotFound = errors.New(" [micro] 微服务服务端未注册需要的compressor")
)
