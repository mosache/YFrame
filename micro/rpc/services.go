package rpc

import (
	"context"
	"errors"
	"gitee.com/mosache/YFrame/micro/rpc/testdata"
	"gitee.com/mosache/YFrame/micro/rpc/testdata/gen"
	"time"
)

type UserServiceImpl struct {
	testdata.UserService
}

func (u *UserServiceImpl) GetUserByID(ctx context.Context, req *testdata.GetUserByIDReq) (*testdata.GetUserByIDResp, error) {

	return &testdata.GetUserByIDResp{ID: 11}, nil
}

func (u *UserServiceImpl) GetUserByIDProto(ctx context.Context, req *gen.GetUserByIdReq) (*gen.GetUserByIdResp, error) {
	return &gen.GetUserByIdResp{Id: 11}, nil
}

type UserServiceTimeOut struct {
	sleep time.Duration
}

func (u *UserServiceTimeOut) GetUserByID(ctx context.Context, req *testdata.GetUserByIDReq) (*testdata.GetUserByIDResp, error) {
	if _, ok := ctx.Deadline(); !ok {
		panic(errors.New("deadline must be set"))
	}

	time.Sleep(u.sleep)

	return &testdata.GetUserByIDResp{ID: 11}, nil
}

func (u *UserServiceTimeOut) Name() string {
	return "user-service"
}
