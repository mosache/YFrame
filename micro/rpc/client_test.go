package rpc

import (
	"context"
	"gitee.com/mosache/YFrame/micro/rpc/compress/gzip"
	"gitee.com/mosache/YFrame/micro/rpc/message"
	"gitee.com/mosache/YFrame/micro/rpc/serialize/json"
	"gitee.com/mosache/YFrame/micro/rpc/testdata"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_setFuncField(t *testing.T) {

	testCases := []struct {
		name string
		srv  Service

		mockProxy func(ctrl *gomock.Controller) Proxy

		wantErr error
	}{
		{
			name: "nil service",
			srv:  nil,
			mockProxy: func(ctrl *gomock.Controller) Proxy {
				p := testdata.NewMockProxy(ctrl)
				return p
			},
			wantErr: ErrServiceIsNil,
		},

		{
			name: "not ptr",
			srv:  testdata.UserService{},
			mockProxy: func(ctrl *gomock.Controller) Proxy {
				p := testdata.NewMockProxy(ctrl)
				return p
			},
			wantErr: ErrStructPtrOnly,
		},

		{
			name: "user service",
			srv:  &testdata.UserService{},
			mockProxy: func(ctrl *gomock.Controller) Proxy {
				p := testdata.NewMockProxy(ctrl)
				p.EXPECT().Invoke(gomock.Any(), gomock.Any()).
					Return(&message.Response{Data: []byte("{\"id\":9}")}, nil)
				return p
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	s := json.Serializer{}
	c := gzip.Compressor{}

	for _, tc := range testCases {

		t.Run(tc.name, func(t *testing.T) {
			err := setFuncField(tc.srv, tc.mockProxy(ctrl), s, c)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			resp, err := tc.srv.(*testdata.UserService).GetUserByID(context.Background(), &testdata.GetUserByIDReq{ID: 123})
			assert.Equal(t, tc.wantErr, err)
			t.Log(resp)
		})
	}
}
