package rpc

import (
	"encoding/binary"
	"net"
)

func ReadMsg(conn net.Conn) ([]byte, error) {
	/// 协议头和协议体一起
	lenBs := make([]byte, sendDataHeaderBytes)
	_, err := conn.Read(lenBs)
	if err != nil {
		return nil, err
	}
	headerLength := binary.BigEndian.Uint32(lenBs[:4])
	bodyLength := binary.BigEndian.Uint32(lenBs[4:])

	dataLen := headerLength + bodyLength
	dataBs := make([]byte, dataLen)

	_, err = conn.Read(dataBs[8:])
	if err != nil {
		return nil, err
	}
	copy(dataBs[:8], lenBs)
	return dataBs, nil
}
