//go:build e2e

package rpc

import (
	"context"
	"gitee.com/mosache/YFrame/micro/rpc/compress/gzip"
	"gitee.com/mosache/YFrame/micro/rpc/internal/errs"
	"gitee.com/mosache/YFrame/micro/rpc/serialize/json"
	"gitee.com/mosache/YFrame/micro/rpc/serialize/proto"
	"gitee.com/mosache/YFrame/micro/rpc/testdata"
	"gitee.com/mosache/YFrame/micro/rpc/testdata/gen"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestClient(t *testing.T) {
	/// start server
	server := NewServer()
	server.Register(&UserServiceImpl{})
	server.RegisterSerializer(json.Serializer{})
	go func() {
		err := server.Start(":8081")
		if err != nil {
			t.Fatal(err)
		}
	}()

	///// 保证server启动了
	//time.Sleep(time.Second)

	us := &testdata.UserService{}
	client := NewClient("localhost:8081")
	err := client.initService(us)
	require.NoError(t, err)

	resp, err := us.GetUserByID(context.Background(), &testdata.GetUserByIDReq{ID: 1})
	require.NoError(t, err)

	assert.Equal(t, resp, &testdata.GetUserByIDResp{ID: 11})

}

func TestClientProto(t *testing.T) {
	/// start server
	server := NewServer()
	server.Register(&UserServiceImpl{})
	server.RegisterSerializer(proto.Serializer{})
	go func() {
		err := server.Start(":8081")
		if err != nil {
			t.Fatal(err)
		}
	}()

	///// 保证server启动了
	//time.Sleep(time.Second)

	us := &testdata.UserService{}
	client := NewClient("localhost:8081", ClientWithSerializer(proto.Serializer{}))
	err := client.initService(us)
	require.NoError(t, err)

	resp, err := us.GetUserByIDProto(context.Background(), &gen.GetUserByIdReq{Id: 1})
	require.NoError(t, err)

	assert.Equal(t, resp.Id, int64(11))

}

func TestOneWay(t *testing.T) {
	/// start server
	server := NewServer()
	server.Register(&UserServiceImpl{})
	server.RegisterSerializer(proto.Serializer{})
	go func() {
		err := server.Start(":8081")
		if err != nil {
			t.Fatal(err)
		}
	}()

	///// 保证server启动了
	//time.Sleep(time.Second)

	us := &testdata.UserService{}
	client := NewClient("localhost:8081")
	err := client.initService(us)
	require.NoError(t, err)

	ctx := ContextWithOneway(context.Background())

	_, err = us.GetUserByIDProto(ctx, &gen.GetUserByIdReq{Id: 1})

	assert.Equal(t, err, errs.ErrOneWayCall)

}

func TestTimeOut(t *testing.T) {
	/// start server
	server := NewServer()
	server.Register(&UserServiceTimeOut{sleep: time.Second * 3})
	go func() {
		err := server.Start(":8081")
		if err != nil {
			t.Fatal(err)
		}
	}()

	///// 保证server启动了
	//time.Sleep(time.Second)

	us := &testdata.UserService{}
	client := NewClient("localhost:8081")
	err := client.initService(us)
	require.NoError(t, err)

	ctx, _ := context.WithTimeout(context.Background(), time.Second)

	_, err = us.GetUserByID(ctx, &testdata.GetUserByIDReq{ID: 1})

	assert.Equal(t, err, context.DeadlineExceeded)

}

func TestGzipCompressor(t *testing.T) {
	/// start server
	server := NewServer()
	server.Register(&UserServiceImpl{})
	server.RegisterSerializer(json.Serializer{})
	server.RegisterCompressor(gzip.Compressor{})
	go func() {
		err := server.Start(":8081")
		if err != nil {
			t.Fatal(err)
		}
	}()

	///// 保证server启动了
	//time.Sleep(time.Second)

	us := &testdata.UserService{}
	client := NewClient("localhost:8081", ClientWithCompressor(gzip.Compressor{}))
	err := client.initService(us)
	require.NoError(t, err)

	resp, err := us.GetUserByID(context.Background(), &testdata.GetUserByIDReq{ID: 1})
	require.NoError(t, err)

	assert.Equal(t, resp, &testdata.GetUserByIDResp{ID: 11})

}
