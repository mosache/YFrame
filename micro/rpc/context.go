package rpc

import "context"

type oneWayKey struct {
}

func ContextWithOneway(ctx context.Context) context.Context {
	return context.WithValue(ctx, oneWayKey{}, true)
}

func IsOneWayContext(ctx context.Context) bool {
	val := ctx.Value(oneWayKey{})
	onWay, ok := val.(bool)
	return ok && onWay
}
