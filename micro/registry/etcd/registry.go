package etcd

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/mosache/YFrame/micro/registry"
	clientv3 "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
	"sync"
)

type Registry struct {
	c       *clientv3.Client
	sess    *concurrency.Session
	cancels []func()
	mutex   sync.Mutex
}

func NewRegistry(c *clientv3.Client) (*Registry, error) {
	sess, err := concurrency.NewSession(c)
	if err != nil {
		return nil, err
	}

	r := &Registry{
		c:    c,
		sess: sess,
	}

	return r, nil
}

func (r *Registry) Register(ctx context.Context, si registry.ServiceInstance) error {
	val, err := json.Marshal(si)

	if err != nil {
		return err
	}

	_, err = r.c.Put(ctx, r.instanceKey(si), string(val), clientv3.WithLease(r.sess.Lease()))

	return err
}

func (r *Registry) UnRegister(ctx context.Context, si registry.ServiceInstance) error {
	_, err := r.c.Delete(ctx, r.instanceKey(si))
	return err
}

func (r *Registry) ListService(ctx context.Context, serviceName string) ([]registry.ServiceInstance, error) {
	resp, err := r.c.Get(ctx, r.serviceKey(serviceName), clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}

	instances := make([]registry.ServiceInstance, 0, len(resp.Kvs))

	for _, e := range resp.Kvs {
		var si registry.ServiceInstance
		err = json.Unmarshal(e.Value, &si)
		if err != nil {
			return nil, err
		}

		instances = append(instances, si)
	}

	return instances, nil
}

func (r *Registry) Subscribe(serviceName string) (<-chan registry.Event, error) {
	ctx, cancel := context.WithCancel(context.Background())
	r.mutex.Lock()
	r.cancels = append(r.cancels, cancel)
	r.mutex.Unlock()
	ctx = clientv3.WithRequireLeader(ctx)
	watcher := r.c.Watch(ctx, r.serviceKey(serviceName), clientv3.WithPrefix())

	eventChan := make(chan registry.Event)

	go func() {
		for {
			select {
			case w := <-watcher:
				events := w.Events
				if w.Err() != nil {
					continue
				}

				if w.Canceled {
					return
				}

				for range events {
					eventChan <- registry.Event{}
				}
			case <-ctx.Done():
				return
			}
		}
	}()

	return eventChan, nil

}

func (r *Registry) Close() error {
	r.mutex.Lock()
	cancels := r.cancels
	r.cancels = nil
	r.mutex.Unlock()
	for _, c := range cancels {
		c()
	}
	return r.sess.Close()
}

func (r *Registry) instanceKey(si registry.ServiceInstance) string {
	return fmt.Sprintf("/micro/%s/%s", si.Name, si.Addr)
}

func (r *Registry) serviceKey(serviceName string) string {
	return fmt.Sprintf("/micro/%s", serviceName)
}
