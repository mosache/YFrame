package filter

import (
	"google.golang.org/grpc/resolver"
)

type Filter func(info resolver.Address) bool
