package group_filter

import (
	"gitee.com/mosache/YFrame/micro/filter"
	"google.golang.org/grpc/resolver"
)

func GroupFilter(group string) filter.Filter {
	return func(info resolver.Address) bool {
		g, ok := info.Attributes.Value("group").(string)
		if !ok {
			return false
		}

		if g == group {
			return true
		}

		return false
	}
}
