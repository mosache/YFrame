package grpc

import (
	"context"
	"gitee.com/mosache/YFrame/micro/example/proto/gen"
	"google.golang.org/grpc"
	"net"
	"testing"
)

type Server struct {
	gen.UnimplementedUserServiceServer
}

func (s Server) GetUserByID(ctx context.Context, req *gen.GetUserByIDReq) (*gen.GetUserByIDResp, error) {
	return &gen.GetUserByIDResp{Name: "hello word"}, nil
}

func TestServer(t *testing.T) {
	grpcServer := grpc.NewServer()

	gen.RegisterUserServiceServer(grpcServer, Server{})

	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		t.Fatal(err)
	}

	grpcServer.Serve(l)
}
