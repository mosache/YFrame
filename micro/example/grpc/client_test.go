package grpc

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/micro/example/proto/gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"testing"
	"time"
)

func TestClient(t *testing.T) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	grpcClient, err := grpc.DialContext(ctx, "discovery://userService",
		grpc.WithResolvers(Builder{}),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatal(err)
	}
	c := gen.NewUserServiceClient(grpcClient)

	resp, err := c.GetUserByID(ctx, &gen.GetUserByIDReq{Id: 1})
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(resp)
}
