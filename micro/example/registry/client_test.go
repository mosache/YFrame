package grpc

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/micro"
	"gitee.com/mosache/YFrame/micro/example/proto/gen"
	"gitee.com/mosache/YFrame/micro/registry/etcd"
	"github.com/stretchr/testify/require"
	clientv3 "go.etcd.io/etcd/client/v3"
	"testing"
	"time"
)

func TestClient(t *testing.T) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	etcdClient, err := clientv3.New(clientv3.Config{
		Endpoints: []string{"localhost:2379"},
	})

	registry, err := etcd.NewRegistry(etcdClient)
	require.NoError(t, err)

	client, err := micro.NewClient(micro.ClientWithInsecure(true), micro.ClientWithRegistry(registry))
	require.NoError(t, err)

	grpcClient, err := client.Dial(ctx, "user-service")
	require.NoError(t, err)

	c := gen.NewUserServiceClient(grpcClient)

	resp, err := c.GetUserByID(ctx, &gen.GetUserByIDReq{Id: 1})
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(resp)
}
