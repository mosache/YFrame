package grpc

import (
	"context"
	"gitee.com/mosache/YFrame/micro"
	"gitee.com/mosache/YFrame/micro/example/proto/gen"
	"gitee.com/mosache/YFrame/micro/registry/etcd"
	"github.com/stretchr/testify/require"
	clientv3 "go.etcd.io/etcd/client/v3"
	"testing"
)

type Server struct {
	gen.UnimplementedUserServiceServer
}

func (s Server) GetUserByID(ctx context.Context, req *gen.GetUserByIDReq) (*gen.GetUserByIDResp, error) {
	return &gen.GetUserByIDResp{Name: "hello word"}, nil
}

func TestServer(t *testing.T) {
	etcdClient, err := clientv3.New(clientv3.Config{
		Endpoints: []string{"localhost:2379"},
	})
	require.NoError(t, err)

	registry, err := etcd.NewRegistry(etcdClient)
	require.NoError(t, err)

	server := micro.NewServer("user-service", micro.ServerWithRegistry(registry))

	gen.RegisterUserServiceServer(server, Server{})

	err = server.Start(":8080")
	require.NoError(t, err)

}
