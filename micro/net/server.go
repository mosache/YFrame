package net

import (
	"errors"
	"fmt"
	"net"
)

func Serve(network, address string) error {
	listen, err := net.Listen(network, address)
	if err != nil {
		return err
	}

	for {
		conn, err := listen.Accept()
		if err != nil {
			return err
		}

		go func() {
			if err := handlerConn(conn); err != nil {
				fmt.Println(err.Error())
			}

		}()
	}

	return nil
}

func handlerConn(conn net.Conn) error {
	defer conn.Close()

	for {
		bs := make([]byte, 8)
		n, err := conn.Read(bs)
		if err != nil {
			return err
		}
		if n != 8 {
			return errors.New("data not 8 bit")
		}

		//res := handleMsg(bs)
	}

	return nil
}
