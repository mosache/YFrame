package micro

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/micro/registry"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"time"
)

type Client struct {
	r        registry.Registry
	insecure bool
	balancer string // lb name
}

type ClientOption func(c *Client)

func ClientWithInsecure(insecure bool) ClientOption {
	return func(c *Client) {
		c.insecure = insecure
	}
}

func ClientWithRegistry(r registry.Registry) ClientOption {
	return func(c *Client) {
		c.r = r
	}
}

func ClientWithBalancer(balancer string) ClientOption {
	return func(c *Client) {
		c.balancer = balancer
	}
}

func NewClient(opts ...ClientOption) (*Client, error) {
	c := &Client{insecure: false}

	for _, opt := range opts {
		opt(c)
	}

	return c, nil
}

func (c *Client) Dial(ctx context.Context, serviceName string) (*grpc.ClientConn, error) {
	dialOpts := make([]grpc.DialOption, 0)

	if c.r != nil {
		builder := NewRegisterBuilder(c.r, GrpcResolverBuilderWithTimeOut(time.Second*3))
		dialOpts = append(dialOpts, grpc.WithResolvers(builder))
	}

	if c.insecure {
		dialOpts = append(dialOpts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	}

	/// 用了lb
	if len(c.balancer) > 0 {
		dialOpts = append(dialOpts, grpc.WithDefaultServiceConfig(fmt.Sprintf(`{"LoadBalancingPolicy":"%s"}`, c.balancer)))
	}

	grpcClient, err := grpc.DialContext(ctx, fmt.Sprintf("discovery:///%s", serviceName),
		dialOpts...)
	if err != nil {
		return nil, err
	}

	return grpcClient, err
}
