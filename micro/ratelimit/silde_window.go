package ratelimit

import (
	"container/list"
	"context"
	"gitee.com/mosache/YFrame/micro/ratelimit/internal/errs"
	"google.golang.org/grpc"
	"sync"
	"time"
)

type SlideWindowLimiter struct {
	cnt      int64
	interval int64
	rate     int

	queue *list.List

	mutex sync.Mutex
}

func NewSlideWindowLimiter(interval time.Duration, rate int) *SlideWindowLimiter {
	return &SlideWindowLimiter{
		interval: interval.Nanoseconds(),
		rate:     rate,
		queue:    list.New(),
	}
}

func (s *SlideWindowLimiter) Build() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		s.mutex.Lock()

		if s.queue.Len() < s.rate {
			resp, err = handler(ctx, req)
			s.queue.PushBack(time.Now().UnixNano())
			s.mutex.Unlock()
			return
		}

		now := time.Now().UnixNano()
		boundary := now - s.interval

		ts := s.queue.Front()

		for ts != nil && ts.Value.(int64) < boundary {
			s.queue.Remove(ts)
			ts = s.queue.Front()
		}

		length := s.queue.Len()

		s.mutex.Unlock()
		if length >= s.rate {
			err = errs.ErrLimiterOverCapacity
			return
		}

		resp, err = handler(ctx, req)
		s.queue.PushBack(now)

		return
	}
}
