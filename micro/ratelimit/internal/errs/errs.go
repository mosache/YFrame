package errs

import "errors"

var (
	ErrLimiterOverCapacity = errors.New("处理能力已满")
)
