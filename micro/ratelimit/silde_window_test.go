package ratelimit

import (
	"context"
	"gitee.com/mosache/YFrame/micro/ratelimit/internal/errs"
	"gitee.com/mosache/YFrame/micro/rpc/testdata/gen"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"testing"
	"time"
)

func TestSlideWindowLimiter_BuildServerInterceptor(t *testing.T) {
	// 三秒钟只能有一个请求
	interceptor := NewSlideWindowLimiter(time.Second*3, 1).Build()
	cnt := 0
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		cnt++
		return &gen.GetUserByIdReq{}, nil
	}
	resp, err := interceptor(context.Background(), &gen.GetUserByIdReq{}, &grpc.UnaryServerInfo{}, handler)
	require.NoError(t, err)
	assert.Equal(t, &gen.GetUserByIdReq{}, resp)

	resp, err = interceptor(context.Background(), &gen.GetUserByIdReq{}, &grpc.UnaryServerInfo{}, handler)
	require.Equal(t, errs.ErrLimiterOverCapacity, err)
	assert.Nil(t, resp)

	// 睡一个三秒，确保窗口新建了
	time.Sleep(time.Second * 3)
	resp, err = interceptor(context.Background(), &gen.GetUserByIdReq{}, &grpc.UnaryServerInfo{}, handler)
	require.NoError(t, err)
	assert.Equal(t, &gen.GetUserByIdReq{}, resp)
}
