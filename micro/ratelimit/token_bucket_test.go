package ratelimit

import (
	"context"
	"gitee.com/mosache/YFrame/micro/rpc/testdata/gen"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"testing"
	"time"
)

func TestTokenBucketLimiter_Build(t *testing.T) {
	testCases := []struct {
		name    string
		b       func() *TokenBucketLimiter
		ctx     func() (context.Context, context.CancelFunc)
		handler grpc.UnaryHandler

		wantErr  error
		wantResp any
	}{
		{
			name: "context timeout",
			b: func() *TokenBucketLimiter {
				return NewTokenBucketLimiter(0, time.Second*10)
			},
			ctx: func() (context.Context, context.CancelFunc) {
				return context.WithTimeout(context.Background(), time.Second)
			},
			handler: func(ctx context.Context, req interface{}) (interface{}, error) {
				return nil, nil
			},
			wantErr:  context.DeadlineExceeded,
			wantResp: nil,
		},

		{
			name: "context canceled",
			b: func() *TokenBucketLimiter {
				return NewTokenBucketLimiter(0, time.Second*10)
			},
			ctx: func() (context.Context, context.CancelFunc) {
				ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second)
				cancelFunc()
				return ctx, cancelFunc
			},
			handler: func(ctx context.Context, req interface{}) (interface{}, error) {
				return nil, nil
			},
			wantErr:  context.Canceled,
			wantResp: nil,
		},

		{
			name: "limiter close",
			b: func() *TokenBucketLimiter {
				l := NewTokenBucketLimiter(0, time.Second)

				l.Close()

				return l
			},
			ctx: func() (context.Context, context.CancelFunc) {
				return context.Background(), nil
			},
			handler: func(ctx context.Context, req interface{}) (interface{}, error) {
				return &gen.GetUserByIdReq{Id: 1}, nil
			},
			wantResp: &gen.GetUserByIdReq{Id: 1},
		},

		{
			name: "get token",
			b: func() *TokenBucketLimiter {
				l := NewTokenBucketLimiter(0, time.Second)

				l.Close()

				return l
			},
			ctx: func() (context.Context, context.CancelFunc) {
				return context.Background(), nil
			},
			handler: func(ctx context.Context, req interface{}) (interface{}, error) {
				return &gen.GetUserByIdReq{Id: 2}, nil
			},
			wantResp: &gen.GetUserByIdReq{Id: 2},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			interceptor := tc.b().Build()
			ctx, _ := tc.ctx()
			resp, err := interceptor(ctx, gen.GetUserByIdReq{},
				&grpc.UnaryServerInfo{}, tc.handler)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantResp, resp)
		})
	}
}
