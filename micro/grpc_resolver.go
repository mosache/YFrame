package micro

import (
	"context"
	"gitee.com/mosache/YFrame/micro/registry"
	"github.com/zeromicro/go-zero/core/syncx"
	"google.golang.org/grpc/attributes"
	"google.golang.org/grpc/resolver"
	"time"
)

type GrpcResolverBuilderOption func(b *GrpcResolverBuilder)

type GrpcResolverBuilder struct {
	r       registry.Registry
	timeOut time.Duration
}

func GrpcResolverBuilderWithTimeOut(timeOut time.Duration) GrpcResolverBuilderOption {
	return func(b *GrpcResolverBuilder) {
		b.timeOut = timeOut
	}
}

func NewRegisterBuilder(r registry.Registry, opts ...GrpcResolverBuilderOption) *GrpcResolverBuilder {
	builder := &GrpcResolverBuilder{
		r,
		time.Second * 3,
	}

	for _, opt := range opts {
		opt(builder)
	}

	return builder
}

func (b *GrpcResolverBuilder) Build(target resolver.Target,
	cc resolver.ClientConn, opts resolver.BuildOptions) (resolver.Resolver, error) {

	r := &grpcResolver{
		cc:      cc,
		target:  target,
		r:       b.r,
		timeOut: b.timeOut,
	}

	r.resolve()

	go r.watch()

	return r, nil
}

func (b *GrpcResolverBuilder) Scheme() string {
	return "discovery"
}

type grpcResolver struct {
	cc      resolver.ClientConn
	target  resolver.Target
	r       registry.Registry
	timeOut time.Duration
	close   chan struct{}
}

func (g *grpcResolver) ResolveNow(options resolver.ResolveNowOptions) {
	g.resolve()
}

func (g *grpcResolver) watch() {
	events, err := g.r.Subscribe(g.target.Endpoint())
	if err != nil {
		g.cc.ReportError(err)
		return
	}

	for {
		select {
		case <-events:
			g.resolve()
		case <-g.close:
			return
		}
	}

}

func (g *grpcResolver) resolve() {
	ctx, cancel := context.WithTimeout(context.Background(), g.timeOut)
	defer cancel()
	instances, err := g.r.ListService(ctx, g.target.Endpoint())
	if err != nil {
		g.cc.ReportError(err)
		return
	}

	address := make([]resolver.Address, 0, len(instances))

	for _, e := range instances {
		address = append(address, resolver.Address{
			Addr:       e.Addr,
			Attributes: attributes.New("weight", e.Weight).WithValue("group", e.Group),
		})
	}

	err = g.cc.UpdateState(resolver.State{
		Addresses: address,
	})
	if err != nil {
		g.cc.ReportError(err)
		return
	}
}

func (g *grpcResolver) Close() {
	syncx.Once(func() {
		close(g.close)
	})
}
