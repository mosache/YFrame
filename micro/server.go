package micro

import (
	"context"
	"gitee.com/mosache/YFrame/micro/registry"
	"google.golang.org/grpc"
	"net"
	"time"
)

type Server struct {
	name string
	r    registry.Registry
	*grpc.Server
	registerTimeOut time.Duration

	weight uint32 // 权重

	group string // group
}

type ServerOpt func(s *Server)

func ServerWithWeight(weight uint32) ServerOpt {
	return func(s *Server) {
		s.weight = weight
	}
}

func ServerWithGroupName(group string) ServerOpt {
	return func(s *Server) {
		s.group = group
	}
}

func ServerWithRegistry(r registry.Registry) ServerOpt {
	return func(s *Server) {
		s.r = r
	}
}

func NewServer(name string, opts ...ServerOpt) *Server {
	s := &Server{
		name:            name,
		Server:          grpc.NewServer(),
		registerTimeOut: time.Second * 3,
	}

	for _, opt := range opts {
		opt(s)
	}

	return s
}

func (s *Server) Start(addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	/// 启用注册中心
	if s.r != nil {
		ctx, cancel := context.WithTimeout(context.Background(), s.registerTimeOut)
		defer cancel()
		err = s.r.Register(ctx, registry.ServiceInstance{
			Name:   s.name,
			Addr:   l.Addr().String(),
			Weight: s.weight,
			Group:  s.group,
		})
		if err != nil {
			return err
		}
	}

	return s.Serve(l)
}

func (s *Server) Close() error {
	if s.r != nil {
		err := s.r.Close()
		if err != nil {
			return err
		}
	}

	s.GracefulStop()
	return nil
}
