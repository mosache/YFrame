package loadbalance

import (
	"gitee.com/mosache/YFrame/micro/filter"
	"google.golang.org/grpc/balancer"
	"google.golang.org/grpc/balancer/base"
	"google.golang.org/grpc/resolver"
)

type Builder struct {
	Filter filter.Filter
	picker PickerFunc
}

type PickerFunc func(nodes []*NodeInfo) balancer.Picker

type BalancerBuildOption func(b *Builder)

func BuilderWithPickerFunc(pickerFunc PickerFunc) BalancerBuildOption {
	return func(b *Builder) {
		b.picker = pickerFunc
	}
}

func BuilderWithFilter(f filter.Filter) BalancerBuildOption {
	return func(b *Builder) {
		b.Filter = f
	}
}

func NewBalancerBuilder(opts ...BalancerBuildOption) *Builder {
	builder := &Builder{
		Filter: func(info resolver.Address) bool {
			return true
		}, picker: func(nodes []*NodeInfo) balancer.Picker {
			return Picker{nodes}
		}}

	for _, opt := range opts {
		opt(builder)
	}

	return builder
}

func (b *Builder) Build(info base.PickerBuildInfo) balancer.Picker {
	nodes := make([]*NodeInfo, 0, len(info.ReadySCs))

	for k, v := range info.ReadySCs {
		if b.Filter(v.Address) {
			nodes = append(nodes, &NodeInfo{
				SubConn: k,
			})
		}
	}
	return b.picker(nodes)
}

type Picker struct {
	Nodes []*NodeInfo
}

func (p Picker) Pick(info balancer.PickInfo) (balancer.PickResult, error) {
	return balancer.PickResult{}, balancer.ErrNoSubConnAvailable
}

type NodeInfo struct {
	SubConn balancer.SubConn
}
