package round_robin

import (
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/balancer"
	"testing"
)

func TestPick(t *testing.T) {
	testCases := []struct {
		name  string
		nodes []*weightNode
	}{
		{
			name: "w-3-1",
			nodes: []*weightNode{
				{
					weight:        3,
					currentWeight: 3,
					effectWeight:  3,
					c: subConn{
						name: "w-3",
					},
				},
				{
					weight:        1,
					currentWeight: 1,
					effectWeight:  1,
					c: subConn{
						name: "w-1",
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			b := &weightBalancer{nodes: tc.nodes}

			res, err := b.Pick(balancer.PickInfo{})
			assert.NoError(t, err)
			assert.Equal(t, "w-3", res.SubConn.(subConn).name)

			res, err = b.Pick(balancer.PickInfo{})
			assert.NoError(t, err)
			assert.Equal(t, "w-3", res.SubConn.(subConn).name)

			res, err = b.Pick(balancer.PickInfo{})
			assert.NoError(t, err)
			assert.Equal(t, "w-3", res.SubConn.(subConn).name)

			res, err = b.Pick(balancer.PickInfo{})
			assert.NoError(t, err)
			assert.Equal(t, "w-1", res.SubConn.(subConn).name)

			res, err = b.Pick(balancer.PickInfo{})
			assert.NoError(t, err)
			assert.Equal(t, "w-3", res.SubConn.(subConn).name)
		})
	}
}

type subConn struct {
	name string
	balancer.SubConn
}
