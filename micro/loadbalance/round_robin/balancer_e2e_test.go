//go:build e2e

package round_robin

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/micro"
	"gitee.com/mosache/YFrame/micro/example/proto/gen"
	"gitee.com/mosache/YFrame/micro/filter/group_filter"
	"gitee.com/mosache/YFrame/micro/loadbalance"
	"gitee.com/mosache/YFrame/micro/registry/etcd"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc/balancer"
	"google.golang.org/grpc/balancer/base"
	"testing"
	"time"
)

func TestBalancer(t *testing.T) {
	etcdClient, err := clientv3.New(clientv3.Config{
		Endpoints: []string{"localhost:2379"},
	})
	require.NoError(t, err)

	registry, err := etcd.NewRegistry(etcdClient)
	require.NoError(t, err)

	go func() {
		p, server := ":8502", micro.NewServer(
			"user-service",
			micro.ServerWithRegistry(registry),
			micro.ServerWithWeight(3),
			micro.ServerWithGroupName("B"),
		)
		gen.RegisterUserServiceServer(server, Server{Port: p})
		server.Start(p)
	}()

	go func() {
		p := ":8501"
		server := micro.NewServer("user-service",
			micro.ServerWithRegistry(registry),
			micro.ServerWithWeight(3),
			micro.ServerWithGroupName("B"),
		)
		gen.RegisterUserServiceServer(server, Server{Port: p})
		server.Start(p)
	}()

	go func() {
		p := ":8503"
		server := micro.NewServer("user-service",
			micro.ServerWithRegistry(registry),
			micro.ServerWithWeight(3),
			micro.ServerWithGroupName("B"),
		)
		gen.RegisterUserServiceServer(server, Server{Port: p})
		server.Start(p)
	}()

	time.Sleep(time.Second * 10)

	balancer.Register(base.NewBalancerBuilder("demo_loading", &Builder{Builder: loadbalance.NewBalancerBuilder(
		loadbalance.BuilderWithPickerFunc(func(nodes []*loadbalance.NodeInfo) balancer.Picker {
			return NewBalancer(nodes)
		}), loadbalance.BuilderWithFilter(group_filter.GroupFilter("B")))}, base.Config{HealthCheck: true}))
	balancer.Register(base.NewBalancerBuilder("weight_balancer", &WeightBalancerBuilder{}, base.Config{HealthCheck: true}))
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	conn, err := micro.NewClient(micro.ClientWithInsecure(true),
		micro.ClientWithRegistry(registry),
		micro.ClientWithBalancer("demo_loading"))
	assert.NoError(t, err)

	grpcClient, err := conn.Dial(ctx, "user-service")
	assert.NoError(t, err)

	c := gen.NewUserServiceClient(grpcClient)

	resp, err := c.GetUserByID(ctx, &gen.GetUserByIDReq{Id: 1})
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(resp)

	resp, err = c.GetUserByID(ctx, &gen.GetUserByIDReq{Id: 1})
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(resp)

	resp, err = c.GetUserByID(ctx, &gen.GetUserByIDReq{Id: 1})
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(resp)

	resp, err = c.GetUserByID(ctx, &gen.GetUserByIDReq{Id: 1})
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(resp)

}

type Server struct {
	Port string
	gen.UnimplementedUserServiceServer
}

func (s Server) GetUserByID(ctx context.Context, req *gen.GetUserByIDReq) (*gen.GetUserByIDResp, error) {

	return &gen.GetUserByIDResp{Name: fmt.Sprintf("resp from [%s]", s.Port)}, nil
}
