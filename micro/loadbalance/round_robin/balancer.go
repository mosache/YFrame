/*
普通轮询
*/
package round_robin

import (
	"gitee.com/mosache/YFrame/micro/loadbalance"
	"google.golang.org/grpc/balancer"
	"google.golang.org/grpc/balancer/base"
	"sync/atomic"
)

type Builder struct {
	*loadbalance.Builder
}

func (b *Builder) Build(info base.PickerBuildInfo) balancer.Picker {
	p := b.Builder.Build(info).(*Balancer)
	return p
}

type Balancer struct {
	index  uint64
	length uint64
	loadbalance.Picker
}

func NewBalancer(nodes []*loadbalance.NodeInfo) *Balancer {
	return &Balancer{
		index:  0,
		length: uint64(len(nodes)),
		Picker: loadbalance.Picker{Nodes: nodes},
	}
}

func (b *Balancer) Pick(info balancer.PickInfo) (balancer.PickResult, error) {
	if b.length == 0 {
		return balancer.PickResult{}, balancer.ErrNoSubConnAvailable
	}

	idx := atomic.AddUint64(&b.index, 1)
	return balancer.PickResult{
		SubConn: b.Nodes[idx%b.length].SubConn,
	}, nil
}
