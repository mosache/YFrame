package queue

import "context"

type Queue[T any] interface {
	Push(ctx context.Context, el T) error
	Pop(ctx context.Context) (T, error)
	Size() int
	Clear() error
	IsEmpty() bool
}
