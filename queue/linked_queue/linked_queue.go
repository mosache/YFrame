package linked_queue

import (
	"context"
	"unsafe"
)

type LinkedQueue[T any] struct {
	head unsafe.Pointer
	tail unsafe.Pointer
	zero T
}

type node[T any] struct {
	data T
	next unsafe.Pointer
}

func NewLinkedQueue[T any]() *LinkedQueue[T] {
	head := &node[T]{}
	headPtr := unsafe.Pointer(head)
	return &LinkedQueue[T]{
		head: headPtr,
		tail: headPtr,
	}
}

func (l *LinkedQueue[T]) Push(ctx context.Context, el T) error {
	//valNode := &node[T]{data: el}
	//valPtr := unsafe.Pointer(valNode)
	//
	//for {
	//	tail := atomic.LoadPointer(&l.tail)
	//
	//}
	//
	//tail.next = valNode
	//l.tail = valNode
	return nil
}

func (l *LinkedQueue[T]) Pop(ctx context.Context) (T, error) {
	//l.mutex.Lock()
	//defer l.mutex.Unlock()
	//
	//if l.head == l.tail {
	//	return l.zero, errors.New("empty")
	//}
	//
	//head := l.head
	//
	//l.head = head.next
	//head.next = nil

	return l.zero, nil

}

func (l *LinkedQueue[T]) Size() int {
	//TODO implement me
	panic("implement me")
}

func (l *LinkedQueue[T]) Clear() error {
	//TODO implement me
	panic("implement me")
}

func (l *LinkedQueue[T]) IsEmpty() bool {
	//TODO implement me
	panic("implement me")
}
