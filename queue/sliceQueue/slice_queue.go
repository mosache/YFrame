package sliceQueue

import (
	"context"
	"golang.org/x/sync/semaphore"
	"sync"
)

type SliceQueue[T any] struct {
	data     []T
	capacity int
	count    int
	head     int
	tail     int
	zero     T

	mutex sync.Mutex
	wCond *semaphore.Weighted
	rCond *semaphore.Weighted
}

type ctxCond struct {
	*sync.Cond
}

func newCtxCond(cond *sync.Cond) *ctxCond {
	return &ctxCond{Cond: cond}
}

func (c *ctxCond) wait(ctx context.Context) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	ch := make(chan struct{})
	go func() {
		c.Cond.Wait()
		select {
		case ch <- struct{}{}:
		default:
			c.Cond.Signal()
			c.Cond.L.Unlock()
		}
	}()
	//c.Cond.L.Unlock()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-ch:
		return nil
	}
}

func NewSliceQueue[T any](capacity int) *SliceQueue[T] {
	s := &SliceQueue[T]{
		data:     make([]T, capacity),
		capacity: capacity,
		wCond:    semaphore.NewWeighted(int64(capacity)),
		rCond:    semaphore.NewWeighted(int64(capacity)),
	}
	//s.wCond = newCtxCond(sync.NewCond(&s.mutex))
	//s.rCond = newCtxCond(sync.NewCond(&s.mutex))
	return s
}

func (s *SliceQueue[T]) Push(ctx context.Context, el T) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	if err := s.wCond.Acquire(ctx, 1); err != nil {
		return err
	}
	defer s.rCond.Release(1)

	s.mutex.Lock()
	defer s.mutex.Unlock()
	if ctx.Err() != nil {
		return ctx.Err()
	}

	s.data[s.tail] = el
	s.tail = (s.tail + 1) % s.capacity
	s.count += 1

	return nil
}

func (s *SliceQueue[T]) Pop(ctx context.Context) (T, error) {
	var r T
	if ctx.Err() != nil {
		return r, ctx.Err()
	}

	if err := s.rCond.Acquire(ctx, 1); err != nil {
		return r, err
	}
	defer s.wCond.Release(1)

	s.mutex.Lock()
	defer s.mutex.Unlock()
	if ctx.Err() != nil {
		return s.zero, ctx.Err()
	}

	r = s.data[s.head]
	// 取走的元素置为零值（优化GC）
	s.data[s.head] = s.zero

	s.head = (s.head + 1) % s.capacity
	s.count -= 1

	return r, nil
}

func (s *SliceQueue[T]) Size() int {
	return s.count
}

func (s *SliceQueue[T]) Clear() error {
	//TODO implement me
	panic("implement me")
}

func (s *SliceQueue[T]) IsEmpty() bool {
	return s.count == 0
}

func (s *SliceQueue[T]) IsFull() bool {
	return s.count == s.capacity
}
