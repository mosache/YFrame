package sliceQueue

import (
	"context"
	"sync"
	"testing"
	"time"
)

func TestSliceQueue(t *testing.T) {
	q := NewSliceQueue[int64](0)

	ctx := context.Background()

	var wg sync.WaitGroup

	wg.Add(2)

	go func() {
		defer wg.Done()
		ctx, _ = context.WithTimeout(ctx, time.Second)
		err := q.Push(ctx, 1)
		if err != nil {
			t.Log(err)
		}
	}()

	go func() {
		defer wg.Done()
		ctx2, _ := context.WithTimeout(context.Background(), time.Second)
		if err := q.Push(ctx2, 2); err != nil {
			t.Log(err)
		}
	}()

	//v, _ := q.Pop(ctx)
	//t.Log(v)
	//
	//v, _ = q.Pop(ctx)
	//t.Log(v)

	wg.Wait()

}
