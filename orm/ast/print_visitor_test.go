package ast

import (
	"github.com/stretchr/testify/require"
	"go/ast"
	"go/parser"
	"go/token"
	"testing"
)

func TestPrintVisitor_Visit(t *testing.T) {
	fs := token.NewFileSet()
	f, err := parser.ParseFile(fs, "src.go", `
	package ast

import (
	"go/ast"
	"log"
	"reflect"
)

type PrintVisitor struct {
}

func (p PrintVisitor) Visit(node ast.Node) (w ast.Visitor) {
	typ := reflect.TypeOf(node)
	val := reflect.ValueOf(node)
	for typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
		val = val.Elem()
	}

	log.Printf("type = %v,val = %v", typ.Name(), val.Interface())

	return p
}

	`, parser.ParseComments)
	require.NoError(t, err)
	v := PrintVisitor{}
	ast.Walk(v, f)
}
