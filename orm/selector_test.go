package orm

import (
	"context"
	"database/sql"
	"errors"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"github.com/DATA-DOG/go-sqlmock"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestSelector_Build(t *testing.T) {
	db := MemoryDB(t)

	type testCase[T any] struct {
		name    string
		s       *Selector[T]
		want    *Query
		wantErr error
	}
	tests := []testCase[TestModel]{
		{
			name: "select",
			s:    NewSelector[TestModel](db),
			want: &Query{SQL: "SELECT * FROM `test_model`;"},
		},

		{
			name: "where",
			s:    NewSelector[TestModel](db).Where(C("Id").Eq(18)),
			want: &Query{SQL: "SELECT * FROM `test_model` WHERE `id` = ?;", Args: []any{18}},
		},

		{
			name: "not",
			s:    NewSelector[TestModel](db).Where(Not(C("Id").Eq(18))),
			want: &Query{SQL: "SELECT * FROM `test_model` WHERE NOT (`id` = ?);", Args: []any{18}},
		},

		{
			name: "and",
			s:    NewSelector[TestModel](db).Where(C("Id").Eq(18).And(C("FirstName").Eq("hehe"))),
			want: &Query{SQL: "SELECT * FROM `test_model` WHERE (`id` = ?) AND (`first_name` = ?);", Args: []any{18, "hehe"}},
		},

		{
			name: "or",
			s:    NewSelector[TestModel](db).Where(C("Id").Eq(18).Or(C("FirstName").Eq("hehe"))),
			want: &Query{SQL: "SELECT * FROM `test_model` WHERE (`id` = ?) OR (`first_name` = ?);", Args: []any{18, "hehe"}},
		},

		{
			name:    "invalid col",
			s:       NewSelector[TestModel](db).Where(C("Id2").Eq(18).Or(C("FirstName2").Eq("hehe"))),
			wantErr: errs.NewErrUnknownField("Id2"),
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestSelector_Get(t *testing.T) {
	sqlDB, mock, err := sqlmock.New()
	require.NoError(t, err)
	defer sqlDB.Close()

	db, err := OpenDB(sqlDB)
	require.NoError(t, err)

	mock.ExpectQuery("SELECT .*").WillReturnError(errors.New("query err"))

	// no rows
	mock.ExpectQuery("SELECT .* WHERE `id` < .*").WillReturnError(ErrNoRows)

	//data
	rows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	rows.AddRow("1", "tom", "18", "jerry")
	mock.ExpectQuery("SELECT .* WHERE `id` = .*").WillReturnRows(rows)

	//scan err
	rows = sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	rows.AddRow("aaa", "tom", "18", "jerry")
	mock.ExpectQuery("SELECT .* WHERE `id` = .*").WillReturnRows(rows)

	testCases := []struct {
		name string
		s    *Selector[TestModel]

		wantErr error
		wantRes *TestModel
	}{
		{
			name:    "invalid query",
			s:       NewSelector[TestModel](db).Where(C("xxx").Eq(1)),
			wantErr: errs.NewErrUnknownField("xxx"),
		},

		{
			name:    "query err",
			s:       NewSelector[TestModel](db).Where(C("Id").Eq(1)),
			wantErr: errors.New("query err"),
		},

		{
			name:    "no rows",
			s:       NewSelector[TestModel](db).Where(C("Id").LT(1)),
			wantErr: ErrNoRows,
		},

		{
			name: "data",
			s:    NewSelector[TestModel](db).Where(C("Id").Eq(1)),
			wantRes: &TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "jerry",
					Valid:  true,
				},
			},
		},

		{
			name:    "scan err",
			s:       NewSelector[TestModel](db).Where(C("Id").Eq(3)),
			wantErr: errs.ErrScanErr,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := tc.s.Get(context.Background())
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantRes, res)
		})
	}

}

type TestModel struct {
	Id        int64
	FirstName string
	Age       int8
	LastName  *sql.NullString
}

func MemoryDB(t *testing.T) *DB {
	db, err := Open("sqlite3", "file:test.db?cache=shared&mode=memory")
	require.NoError(t, err)
	return db
}

func TestSelector_GetMulti(t *testing.T) {
	sqlDB, mock, err := sqlmock.New()
	require.NoError(t, err)
	defer sqlDB.Close()

	db, err := OpenDB(sqlDB)
	require.NoError(t, err)

	mock.ExpectQuery("SELECT .*").WillReturnError(errors.New("query err"))

	// no rows
	mock.ExpectQuery("SELECT .* WHERE `id` < .*").WillReturnError(ErrNoRows)

	//data
	rows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	rows.AddRow("1", "tom", "18", "jerry")
	rows.AddRow("2", "haha", "19", "hehe")
	mock.ExpectQuery("SELECT .* WHERE `id` = .*").WillReturnRows(rows)

	//scan err
	rows = sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	rows.AddRow("aaa", "tom", "18", "jerry")
	mock.ExpectQuery("SELECT .* WHERE `id` = .*").WillReturnRows(rows)

	testCases := []struct {
		name string
		s    *Selector[TestModel]

		wantErr error
		wantRes []*TestModel
	}{
		{
			name:    "invalid query",
			s:       NewSelector[TestModel](db).Where(C("xxx").Eq(1)),
			wantErr: errs.NewErrUnknownField("xxx"),
		},

		{
			name:    "query err",
			s:       NewSelector[TestModel](db).Where(C("Id").Eq(1)),
			wantErr: errors.New("query err"),
		},

		{
			name:    "no rows",
			s:       NewSelector[TestModel](db).Where(C("Id").LT(1)),
			wantErr: ErrNoRows,
		},

		{
			name: "data",
			s:    NewSelector[TestModel](db).Where(C("Id").Eq(1)),
			wantRes: []*TestModel{
				{Id: 1,
					FirstName: "tom",
					Age:       18,
					LastName: &sql.NullString{
						String: "jerry",
						Valid:  true,
					},
				},

				{
					Id:        2,
					FirstName: "haha",
					Age:       19,
					LastName: &sql.NullString{
						String: "hehe",
						Valid:  true,
					},
				},
			},
		},

		{
			name:    "scan err",
			s:       NewSelector[TestModel](db).Where(C("Id").Eq(3)),
			wantErr: errs.ErrScanErr,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := tc.s.GetMulti(context.Background())
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantRes, res)
		})
	}
}

func TestSelector_Select(t *testing.T) {
	db := MemoryDB(t)
	testCases := []struct {
		name string
		s    *Selector[TestModel]

		want    *Query
		wantErr error
	}{
		{
			name: "col",
			s:    NewSelector[TestModel](db).Select(C("Id"), C("FirstName")),
			want: &Query{
				SQL: "SELECT `id`,`first_name` FROM `test_model`;",
			},
		},

		{
			name: "no col",
			s:    NewSelector[TestModel](db),
			want: &Query{
				SQL: "SELECT * FROM `test_model`;",
			},
		},

		{
			name: "AVG",
			s:    NewSelector[TestModel](db).Select(Avg("Age")),
			want: &Query{
				SQL: "SELECT AVG(`age`) FROM `test_model`;",
			},
		},

		{
			name: "raw expr",
			s:    NewSelector[TestModel](db).Select(Raw("COUNT(DISTINCT `age`)")),
			want: &Query{
				SQL: "SELECT COUNT(DISTINCT `age`) FROM `test_model`;",
			},
		},

		{
			name: "raw as predicate",
			s:    NewSelector[TestModel](db).Where(Raw("`id` < ?", 18).AsPredicate()),
			want: &Query{
				SQL:  "SELECT * FROM `test_model` WHERE `id` < ?;",
				Args: []any{18},
			},
		},

		{
			name: "raw as expr",
			s:    NewSelector[TestModel](db).Where(C("Id").Eq(Raw("(`id`) + ?", 2))),
			want: &Query{
				SQL:  "SELECT * FROM `test_model` WHERE `id` = (`id`) + ?;",
				Args: []any{2},
			},
		},

		{
			name: "colum alias",
			s:    NewSelector[TestModel](db).Select(C("Id").As("my_id")),
			want: &Query{
				SQL: "SELECT `id` AS `my_id` FROM `test_model`;",
			},
		},

		{
			name: "aggregate alias",
			s:    NewSelector[TestModel](db).Select(Avg(`Id`).As("my_id")),
			want: &Query{
				SQL: "SELECT AVG(`id`) AS `my_id` FROM `test_model`;",
			},
		},

		{
			name: "column alias in where",
			s:    NewSelector[TestModel](db).Where(C("Id").As("my_id").Eq(2)),
			want: &Query{
				SQL:  "SELECT * FROM `test_model` WHERE `id` = ?;",
				Args: []any{2},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestSelector_Join(t *testing.T) {
	db := MemoryDB(t)

	type Order struct {
		Id int64
	}

	type OrderDetail struct {
		OrderID int64
		ItemID  int64
	}

	type Item struct {
		Id int64
	}

	testCase := []struct {
		name      string
		s         QueryBuilder
		wantQuery *Query
		wantErr   error
	}{
		{
			name: "specify table",
			s:    NewSelector[Order](db).From(TableOf(OrderDetail{})),
			wantQuery: &Query{
				SQL: "SELECT * FROM `order_detail`;",
			},
		},

		{
			name: "join-on",
			s: func() QueryBuilder {
				t1 := TableOf(Order{}).As("t1")
				t2 := TableOf(OrderDetail{}).As("t2")
				return NewSelector[Order](db).
					From(t1.Join(t2).On(t1.C("Id").Eq(t2.C("OrderID"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM `order` AS `t1` JOIN `order_detail` AS `t2` ON `t1`.`id` = `t2`.`order_id`;",
			},
		},

		{
			name: "left join",
			s: func() QueryBuilder {
				t1 := TableOf(Order{}).As("t1")
				t2 := TableOf(OrderDetail{}).As("t2")
				return NewSelector[Order](db).
					From(t1.LeftJoin(t2).On(t1.C("Id").Eq(t2.C("OrderID"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM `order` AS `t1` LEFT JOIN `order_detail` AS `t2` ON `t1`.`id` = `t2`.`order_id`;",
			},
		},

		{
			name: "right join",
			s: func() QueryBuilder {
				t1 := TableOf(Order{}).As("t1")
				t2 := TableOf(OrderDetail{}).As("t2")
				return NewSelector[Order](db).
					From(t1.RightJoin(t2).On(t1.C("Id").Eq(t2.C("OrderID"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM `order` AS `t1` RIGHT JOIN `order_detail` AS `t2` ON `t1`.`id` = `t2`.`order_id`;",
			},
		},

		{
			name: "join-join",
			s: func() QueryBuilder {
				t1 := TableOf(Order{}).As("t1")
				t2 := TableOf(OrderDetail{}).As("t2")
				t3 := t1.Join(t2).On(t1.C("Id").Eq(t2.C("OrderID")))
				t4 := TableOf(Item{}).As("t4")
				t5 := t3.Join(t4).On(t2.C("ItemID").Eq(t4.C("Id")))
				return NewSelector[Order](db).
					From(t5)
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM `order` AS `t1` JOIN `order_detail` AS `t2` ON `t1`.`id` = `t2`.`order_id` JOIN `item` AS `t4` ON `t2`.`item_id` = `t4`.`id`;",
			},
		},
	}

	for _, tc := range testCase {
		t.Run(tc.name, func(t *testing.T) {
			q, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantQuery, q)
		})
	}

}
