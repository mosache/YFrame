package schema

import "reflect"

type Schema struct {
	TableName string // 表名

	Fields []*Field

	FieldMap map[string]*Field
	// 列名和属性的射映
	ColumnMap map[string]*Field
}

// Field 字段
type Field struct {
	GoName     string       // 字段名
	ColumnName string       // 列名
	Typ        reflect.Type // 类型
	OffSet     uintptr      // 字段偏移量
}

type TableName interface {
	TableName() string
}
