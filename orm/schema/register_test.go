package schema

import (
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestSchemaWithTableName(t *testing.T) {
	r := NewRegister()

	s, err := r.Register(reflect2.TestModel{}, SchemaWithTableName("test_model_tttt"))

	require.NoError(t, err)

	assert.Equal(t, "test_model_tttt", s.TableName)
}

func TestSchemaWithColumName(t *testing.T) {
	testCases := []struct {
		name string

		field   string
		colName string

		wantColName string
		wantErr     error
	}{
		{
			name:        "colName",
			field:       "FirstName",
			colName:     "first_name_ccc",
			wantColName: "first_name_ccc",
		},

		{
			name:    "err colName",
			field:   "haha",
			wantErr: errs.NewErrUnknownField("haha"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r := NewRegister()
			s, err := r.Register(&reflect2.TestModel{}, WithColumName(tc.field, tc.colName))
			assert.Equal(t, err, tc.wantErr)
			if err != nil {
				return
			}
			fd, ok := s.FieldMap[tc.field]
			require.True(t, ok)
			assert.Equal(t, tc.wantColName, fd.ColumnName)
		})
	}
}
