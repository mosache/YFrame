package schema

import (
	"database/sql"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func Test_parseSchema(t *testing.T) {
	testCases := []struct {
		name string

		entity any
		opts   []SchemaOpt

		want    *Schema
		wantErr error
	}{
		{
			name:   "test model",
			entity: reflect2.TestModel{},
			want: &Schema{
				TableName: "test_model",
				Fields: []*Field{
					{
						GoName:     "Id",
						ColumnName: "id",
						Typ:        reflect.TypeOf(int64(0)),
						OffSet:     0,
					},
					{
						GoName:     "FirstName",
						ColumnName: "first_name",
						Typ:        reflect.TypeOf(""),
						OffSet:     8,
					},
					{
						GoName:     "Age",
						ColumnName: "age",
						Typ:        reflect.TypeOf(int8(0)),
						OffSet:     24,
					},
					{
						GoName:     "LastName",
						ColumnName: "last_name",
						Typ:        reflect.TypeOf(&sql.NullString{}),
						OffSet:     32,
					},
				},
			},
		},

		{
			name: "tag",
			entity: func() any {
				type TagTable struct {
					Name string `orm:"col=name2"`
				}
				return TagTable{}
			}(),
			want: &Schema{
				TableName: "tag_table",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name2",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},
		{
			name: "no tag",
			entity: func() any {
				type TagTable struct {
					Name string
				}
				return TagTable{}
			}(),
			want: &Schema{
				TableName: "tag_table",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},

		{
			name: "empty col",
			entity: func() any {
				type TagTable struct {
					Name string `orm:"col="`
				}
				return TagTable{}
			}(),
			want: &Schema{
				TableName: "tag_table",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},

		{
			name: "col only",
			entity: func() any {
				type TagTable struct {
					Name string `orm:"col"`
				}
				return TagTable{}
			}(),
			wantErr: errs.NewErrTagFormatErr("col"),
			want: &Schema{
				TableName: "tag_table",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},

		{
			name:   "implement tableName",
			entity: TagTable{},
			want: &Schema{
				TableName: "test_table_name",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},

		{
			name:   "implement tableName ptr",
			entity: &TagTablePtr{},
			want: &Schema{
				TableName: "test_table_name",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},

		{
			name:   "empty tableName returns",
			entity: EmptyTableNameReturns{},
			want: &Schema{
				TableName: "empty_table_name_returns",
				Fields: []*Field{
					{
						GoName:     "Name",
						ColumnName: "name",
						Typ:        reflect.TypeOf(""),
						OffSet:     0,
					},
				},
			},
		},
	}

	r := NewRegister()

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := r.Register(tc.entity, tc.opts...)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			fieldMap := make(map[string]*Field)
			columnMap := make(map[string]*Field)
			for _, e := range tc.want.Fields {
				fieldMap[e.GoName] = e
				columnMap[e.ColumnName] = e
			}

			tc.want.FieldMap = fieldMap
			tc.want.ColumnMap = columnMap

			assert.Equal(t, tc.want, got)
		})
	}
}

func TestRegister_get(t *testing.T) {
	testCases := []struct {
		name string

		entity any

		wantSchema *Schema
		wantErr    error
		cacheSize  int64
	}{
		{
			name:   "TestModel",
			entity: reflect2.TestModel{},

			wantSchema: &Schema{
				TableName: "test_model",
				Fields: []*Field{
					{
						GoName:     "Id",
						ColumnName: "id",
						Typ:        reflect.TypeOf(int64(0)),
						OffSet:     0,
					},
					{
						GoName:     "FirstName",
						ColumnName: "first_name",
						Typ:        reflect.TypeOf(""),
						OffSet:     8,
					},
					{
						GoName:     "Age",
						ColumnName: "age",
						Typ:        reflect.TypeOf(int8(0)),
						OffSet:     24,
					},
					{
						GoName:     "LastName",
						ColumnName: "last_name",
						Typ:        reflect.TypeOf(&sql.NullString{}),
						OffSet:     32,
					},
				},
			},
			cacheSize: 1,
		},
	}

	r := NewRegister()

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := r.Get(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			fieldMap := make(map[string]*Field)
			columnMap := make(map[string]*Field)
			for _, e := range tc.wantSchema.Fields {
				fieldMap[e.GoName] = e
				columnMap[e.ColumnName] = e
			}

			tc.wantSchema.FieldMap = fieldMap
			tc.wantSchema.ColumnMap = columnMap

			assert.Equal(t, tc.wantSchema, got)

			typ := reflect.TypeOf(tc.entity)
			m, ok := r.Schemas.Load(typ)
			assert.True(t, ok)
			assert.Equal(t, tc.wantSchema, m.(*Schema))
		})
	}
}

type TagTable struct {
	Name string `orm:"col=name"`
}

func (TagTable) TableName() string {
	return "test_table_name"
}

type TagTablePtr struct {
	Name string `orm:"col=name"`
}

func (*TagTablePtr) TableName() string {
	return "test_table_name"
}

type EmptyTableNameReturns struct {
	Name string `orm:"col=name"`
}

func (EmptyTableNameReturns) TableName() string {
	return ""
}
