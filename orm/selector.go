package orm

import (
	"context"
	"database/sql"
)

type Selector[T any] struct {
	builder
	columns []Selectable
	table   TableReference
	where   []Predicate
	groupBy []Expression
	having  []Predicate
	limit   int
	offSet  int
}

func NewSelector[T any](sess session) *Selector[T] {
	s := &Selector[T]{
		builder: builder{
			session: sess,
			core:    sess.getCore(),
		},
	}
	return s

}

func (s *Selector[T]) Build() (*Query, error) {

	m, err := s.r.Get(new(T))
	if err != nil {
		return nil, err
	}

	s.m = m

	s.sb.WriteString("SELECT ")

	if err = s.buildColumns(s.columns); err != nil {
		return nil, err
	}

	s.sb.WriteString(" FROM ")

	/// tableName
	if err = s.buildTable(s.table); err != nil {
		return nil, err
	}

	///where
	if len(s.where) > 0 {
		s.sb.WriteString(" WHERE ")

		if err = s.buildPredicate(s.where); err != nil {
			return nil, err
		}
	}

	/// GROUP BY
	if len(s.groupBy) > 0 {
		s.sb.WriteString(" GROUP BY ")

		if err = s.buildGroupBy(s.groupBy); err != nil {
			return nil, err
		}
	}

	/// having
	if len(s.having) > 0 {
		s.sb.WriteString(" HAVING ")

		if err = s.buildPredicate(s.having); err != nil {
			return nil, err
		}
	}

	/// limit,offSet
	if s.limit > 0 {
		s.sb.WriteString(" LIMIT ?")
		s.addArg(s.limit)
		s.sb.WriteString(" OFFSET ?")
		s.addArg(s.offSet)
	}

	s.sb.WriteString(";")

	return &Query{
		SQL:  s.sb.String(),
		Args: s.args,
	}, nil

}

func (s *Selector[T]) Get(ctx context.Context) (*T, error) {

	res := get[T](ctx, s.session, s.core, &QueryContext{
		Type:    Select,
		Builder: s,
	})

	if res.Result != nil {
		return res.Result.(*T), res.Err
	}

	return nil, res.Err
}

//func (s *Selector[T]) getHandler(ctx context.Context, qc *QueryContext) *QueryResult {
//	q, err := qc.Builder.Build()
//	if err != nil {
//		return &QueryResult{
//			Err: err,
//		}
//	}
//
//	var rows *sql.Rows
//	if rows, err = s.queryContext(ctx, q.SQL, q.Args...); err != nil {
//		return &QueryResult{
//			Err: err,
//		}
//	}
//	///// 确认有没有数据
//	for !rows.Next() {
//		return &QueryResult{
//			Err: ErrNoRows,
//		}
//	}
//	result := new(T)
//
//	valuer := s.creator(s.m, result)
//	if err = valuer.SetColumn(rows); err != nil {
//		return &QueryResult{
//			Err: err,
//		}
//	}
//
//	return &QueryResult{
//		Result: result,
//	}
//}

func (s *Selector[T]) GetMulti(ctx context.Context) ([]*T, error) {
	q, err := s.Build()
	if err != nil {
		return nil, err
	}

	var rows *sql.Rows
	if rows, err = s.queryContext(ctx, q.SQL, q.Args...); err != nil {
		return nil, err
	}

	result := make([]*T, 0)

	for rows.Next() {
		m := new(T)
		valuer := s.creator(s.m, m)
		if err = valuer.SetColumn(rows); err != nil {
			return nil, err
		}
		result = append(result, m)
	}

	return result, nil
}
