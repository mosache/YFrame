package orm

// Aggregate 聚合函数
type Aggregate struct {
	Fn    string
	Arg   string
	Alias string
}

func (a Aggregate) Select() {}
func (a Aggregate) expr()   {}

func Avg(col string) Aggregate {
	return Aggregate{
		Fn:  "AVG",
		Arg: col,
	}
}

func (a Aggregate) As(alias string) Aggregate {
	return Aggregate{
		Fn:    a.Fn,
		Arg:   a.Arg,
		Alias: alias,
	}
}

func (a Aggregate) Eq(arg any) Predicate {
	return Predicate{
		left:  a,
		op:    opEq,
		right: valueOf(arg),
	}
}

func (a Aggregate) GT(arg any) Predicate {
	return Predicate{
		left:  a,
		op:    opGt,
		right: valueOf(arg),
	}
}

func (a Aggregate) LT(arg any) Predicate {
	return Predicate{
		left:  a,
		op:    opLt,
		right: valueOf(arg),
	}
}
