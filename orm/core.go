package orm

import (
	"context"
	"database/sql"
	"gitee.com/mosache/YFrame/orm/internal/valuer"
	"gitee.com/mosache/YFrame/orm/schema"
)

type core struct {
	r       *schema.Register
	creator valuer.Creator

	dialect Dialect
	quoter  string

	mdls []Middleware

	//schema *schema.Schema // model
}

func get[T any](ctx context.Context, sess session, c core, qc *QueryContext) *QueryResult {
	var err error
	qc.schema, err = c.r.Get(new(T))
	if err != nil {
		return &QueryResult{Err: err}
	}

	var root Handler = func(ctx context.Context, qc *QueryContext) *QueryResult {
		return getHandler[T](ctx, sess, c, qc)
	}

	for i := len(c.mdls) - 1; i >= 0; i-- {
		root = c.mdls[i](root)
	}

	return root(ctx, qc)
}

func getHandler[T any](ctx context.Context, sess session, c core, qc *QueryContext) *QueryResult {
	q, err := qc.Builder.Build()
	if err != nil {
		return &QueryResult{
			Err: err,
		}
	}

	var rows *sql.Rows
	if rows, err = sess.queryContext(ctx, q.SQL, q.Args...); err != nil {
		return &QueryResult{
			Err: err,
		}
	}
	///// 确认有没有数据
	for !rows.Next() {
		return &QueryResult{
			Err: ErrNoRows,
		}
	}
	result := new(T)

	valuer := c.creator(qc.schema, result)
	if err = valuer.SetColumn(rows); err != nil {
		return &QueryResult{
			Err: err,
		}
	}

	return &QueryResult{
		Result: result,
	}
}

func exec(ctx context.Context, sess session, c core, qc *QueryContext) *QueryResult {
	var root Handler = func(ctx context.Context, qc *QueryContext) *QueryResult {
		return execHandler(ctx, sess, qc)
	}

	for idx := len(c.mdls) - 1; idx >= 0; idx-- {
		root = c.mdls[idx](root)
	}

	return root(ctx, qc)
}

func execHandler(ctx context.Context, sess session, qc *QueryContext) *QueryResult {
	var (
		q         *Query
		sqlResult sql.Result
		err       error
	)
	q, err = qc.Builder.Build()
	if err != nil {
		return &QueryResult{
			Err: err,
		}
	}

	sqlResult, err = sess.execContext(ctx, q.SQL, q.Args...)
	if err != nil {
		return &QueryResult{
			Err: err,
		}
	}

	return &QueryResult{Result: sqlResult}
}
