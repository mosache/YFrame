package orm

import (
	"errors"
	"gitee.com/mosache/YFrame/orm/internal/errs"
)

type Dialect interface {
	// 表名，列名标记符号 mysql `
	Quoter() string

	BuildOnDuplicateKey(b *builder, odk *OnDuplicateKey) error
}

type StandSQL struct{}

func (s StandSQL) Quoter() string {
	return "`"
}

func (s StandSQL) BuildOnDuplicateKey(b *builder, odk *OnDuplicateKey) error {
	b.sb.WriteString(" ON DUPLICATE KEY UPDATE ")
	for idx, e := range odk.assigns {
		if idx != 0 {
			b.sb.WriteString(",")
		}
		switch c := e.(type) {
		case Assignment:
			b.sb.WriteString("`")
			fd, ok := b.m.FieldMap[c.Column]
			if !ok {
				return errs.NewErrUnknownField(c.Column)
			}
			b.sb.WriteString(fd.ColumnName)
			b.sb.WriteString("` ")
			b.sb.WriteString("= ")
			b.sb.WriteString("?")
			b.args = append(b.args, c.Val)
		case Column:
			fd, ok := b.m.FieldMap[c.Name]
			if !ok {
				return errs.NewErrUnknownField(c.Name)
			}
			b.Quoter(fd.ColumnName)
			b.sb.WriteString("=")
			b.sb.WriteString("VALUES(")
			b.Quoter(fd.ColumnName)
			b.sb.WriteString(")")
		default:
			return errors.New("unsupported assign type")
		}
	}

	return nil
}
