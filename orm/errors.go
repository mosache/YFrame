package orm

import "gitee.com/mosache/YFrame/orm/internal/errs"

var (
	ErrNoRows = errs.ErrNoRows
)
