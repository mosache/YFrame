package orm

func (i *Inserter[T]) Values(values ...*T) *Inserter[T] {
	i.values = values
	return i
}

// Columns
// 指定要插入的列
func (i *Inserter[T]) Columns(columns ...string) *Inserter[T] {
	i.columns = columns
	return i
}

// OnDuplicateKey
// 主键冲突时的操作
func (i *Inserter[T]) OnDuplicateKey() *OnDuplicateKeyBuilder[T] {
	return &OnDuplicateKeyBuilder[T]{
		i: i,
	}
}

type OnDuplicateKeyBuilder[T any] struct {
	i *Inserter[T]
}

type OnDuplicateKey struct {
	assigns []Assignable
}

func (b *OnDuplicateKeyBuilder[T]) Update(assigns ...Assignable) *Inserter[T] {
	b.i.onDuplicateKey = &OnDuplicateKey{assigns: assigns}
	return b.i
}
