package unsafe

import (
	"errors"
	"reflect"
	"unsafe"
)

type Accessor struct {
	fields map[string]field
	// 起始地址
	addr unsafe.Pointer
}

type field struct {
	offSet uintptr
	typ    reflect.Type
}

func NewAccessor(entity any) (*Accessor, error) {
	a := &Accessor{}

	typ := reflect.TypeOf(entity)
	if typ.Kind() != reflect.Pointer {
		return nil, errors.New("必须传入指针")
	}
	typ = typ.Elem()
	val := reflect.ValueOf(entity)
	numField := typ.NumField()

	fields := make(map[string]field, numField)

	for i := 0; i < numField; i++ {
		fd := typ.Field(i)
		fields[fd.Name] = field{
			offSet: fd.Offset,
			typ:    fd.Type,
		}
	}

	a.fields = fields
	a.addr = val.UnsafePointer()

	return a, nil
}

func (a *Accessor) Get(field string) (any, error) {
	fd, ok := a.fields[field]
	if !ok {
		return nil, errors.New("非法属性名称")
	}

	addr := unsafe.Pointer(uintptr(a.addr) + fd.offSet)

	return reflect.NewAt(fd.typ, addr).Elem().Interface(), nil
}

func (a *Accessor) Set(field string, val any) error {
	fd, ok := a.fields[field]
	if !ok {
		return errors.New("非法属性名称")
	}

	addr := unsafe.Pointer(uintptr(a.addr) + fd.offSet)

	reflect.NewAt(fd.typ, addr).Elem().Set(reflect.ValueOf(val))

	return nil
}
