package unsafe

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAccessor(t *testing.T) {
	u := User{
		Name: "tom",
		Age:  18,
	}
	a, err := NewAccessor(&u)
	assert.NoError(t, err)

	get, err := a.Get("Name")
	assert.NoError(t, err)
	assert.Equal(t, "tom", get)

	err = a.Set("Age", 19)
	assert.NoError(t, err)
	assert.Equal(t, 19, u.Age)

}

type User struct {
	Name string
	Age  int
}
