package orm

import (
	"context"
	"gitee.com/mosache/YFrame/orm/schema"
)

type QueryType int

const (
	Select QueryType = iota
	Insert
	Delete
	Update
	RAW
)

type QueryContext struct {
	Type QueryType

	Builder QueryBuilder

	schema *schema.Schema
}

type QueryResult struct {
	Result any

	Err error
}

type Handler func(ctx context.Context, qc *QueryContext) *QueryResult

type Middleware func(next Handler) Handler
