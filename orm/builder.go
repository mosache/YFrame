package orm

import (
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"gitee.com/mosache/YFrame/orm/schema"
	"strings"
)

type builder struct {
	m    *schema.Schema
	sb   strings.Builder
	args []any

	session
	core
}

func (b *builder) Quoter(columnName string) {
	b.sb.WriteString(b.quoter)
	b.sb.WriteString(columnName)
	b.sb.WriteString(b.quoter)
}

/*
writeAlias 写别名
*/
func (b *builder) writeAlias(alias string) {
	b.sb.WriteString(" AS ")
	b.sb.WriteString(b.quoter)
	b.sb.WriteString(alias)
	b.sb.WriteString(b.quoter)
}

func (b *builder) buildColumns(columns []Selectable) error {
	if len(columns) == 0 {
		b.sb.WriteString("*")
	}
	for idx, e := range columns {
		if idx != 0 {
			b.sb.WriteString(",")
		}

		switch c := e.(type) {
		case Column:
			if err := b.buildColumn(c); err != nil {
				return err
			}
		case Aggregate:
			b.sb.WriteString(c.Fn)
			b.sb.WriteString("(")
			if err := b.buildColumn(Column{
				Name: c.Arg,
			}); err != nil {
				return err
			}
			b.sb.WriteString(")")

			if len(c.Alias) > 0 {
				b.sb.WriteString(" AS `")
				b.sb.WriteString(c.Alias)
				b.sb.WriteString("`")
			}

		case RawExpression:
			b.sb.WriteString(c.Expr)
			b.addArgs(c.Args)
		}
	}

	return nil
}

func (b *builder) buildColumn(c Column) error {
	var (
		fd         *schema.Field
		ok         bool
		tableAlias string
	)
	switch t := c.Table.(type) {
	case nil:
		fd, ok = b.m.FieldMap[c.Name]
	case Table:
		model, err := b.r.Get(t.entity)
		if err != nil {
			return err
		}
		fd, ok = model.FieldMap[c.Name]
		tableAlias = t.alias
	}
	if !ok {
		return errs.NewErrUnknownField(c.Name)
	}
	if len(tableAlias) > 0 {
		b.Quoter(tableAlias)
		b.sb.WriteString(".")
	}
	b.sb.WriteString("`")
	b.sb.WriteString(fd.ColumnName)
	b.sb.WriteString("`")

	if len(c.Alias) > 0 {
		b.sb.WriteString(" AS `")
		b.sb.WriteString(c.Alias)
		b.sb.WriteString("`")
	}
	return nil
}

func (b *builder) buildPredicate(ps []Predicate) error {
	p := ps[0]

	for i := 1; i < len(ps); i++ {
		p = p.And(ps[i])
	}

	if err := b.buildExpression(p); err != nil {
		return err
	}

	return nil
}

func (b *builder) buildExpression(expression Expression) error {
	switch exp := expression.(type) {
	case Predicate:

		var (
			leftPredicate  bool
			rightPredicate bool
		)
		_, leftPredicate = exp.left.(Predicate)
		_, rightPredicate = exp.right.(Predicate)

		if leftPredicate {
			b.sb.WriteString("(")
		}
		if err := b.buildExpression(exp.left); err != nil {
			return err
		}
		if leftPredicate {
			b.sb.WriteString(")")
		}

		b.sb.WriteString(exp.op.string())

		if rightPredicate {
			b.sb.WriteString("(")
		}
		if err := b.buildExpression(exp.right); err != nil {
			return err
		}
		if rightPredicate {
			b.sb.WriteString(")")
		}

	case Column:
		exp.Alias = ""
		return b.buildColumn(exp)
	case value:
		b.sb.WriteString("?")
		b.addArg(exp.val)
	case RawExpression:
		b.sb.WriteString(exp.Expr)
		b.addArgs(exp.Args)
	case Aggregate:
		b.sb.WriteString(exp.Fn)
		b.sb.WriteString("(")
		b.sb.WriteString("`")
		b.sb.WriteString(exp.Arg)
		b.sb.WriteString("`")
		b.sb.WriteString(")")
	}

	return nil
}

func (b *builder) addArgs(args []any) {
	if len(args) == 0 {
		return
	}

	if b.args == nil {
		b.args = make([]any, 0)
	}

	for _, e := range args {
		b.args = append(b.args, e)
	}
}

func (b *builder) addArg(val any) {
	if b.args == nil {
		b.args = make([]any, 0)
	}

	b.args = append(b.args, val)
}

func (b *builder) buildGroupBy(gb []Expression) error {
	for idx, e := range gb {
		if idx != 0 {
			b.sb.WriteString(",")
		}
		if err := b.buildExpression(e); err != nil {
			return err
		}
	}
	return nil
}

func (b *builder) buildHaving(having []Predicate) error {
	for idx, e := range having {
		if idx != 0 {
			b.sb.WriteString(",")
		}
		if err := b.buildExpression(e); err != nil {
			return err
		}
	}
	return nil
}

func (b *builder) buildJoin(join Join) error {
	for idx, e := range join.on {
		if idx != 0 {
			b.sb.WriteString(",")
		}
		if err := b.buildExpression(e); err != nil {
			return err
		}
	}
	return nil
}

func (b *builder) buildTable(table TableReference) error {
	switch t := table.(type) {
	case nil:
		b.Quoter(b.m.TableName)
	case Table:
		m, err := b.r.Get(t.entity)
		if err != nil {
			return err
		}
		b.Quoter(m.TableName)
		if len(t.alias) > 0 {
			b.writeAlias(t.alias)
		}
	case Join:
		err := b.buildTable(t.left)
		if err != nil {
			return err
		}

		b.sb.WriteString(t.typ)

		err = b.buildTable(t.right)
		if err != nil {
			return err
		}

		if len(t.on) > 0 {
			b.sb.WriteString(" ON ")
			err = b.buildPredicate(t.on)
			if err != nil {
				return err
			}
		}
	default:
		return errs.NewErrUnsupportedTableReferenceErr(t)
	}
	return nil
}
