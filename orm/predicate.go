package orm

type op string

func (o op) string() string {
	return string(o)
}

const (
	opEq   = " = "
	opLt   = " < "
	opGt   = " > "
	opAND  = " AND "
	opOR   = " OR "
	opNot  = "NOT "
	opPlus = " + "
)

type Predicate struct {
	left  Expression
	op    op
	right Expression
}

func (left Predicate) expr() {}

func (left Predicate) And(right Predicate) Predicate {
	return Predicate{
		left:  left,
		op:    opAND,
		right: right,
	}
}

func (left Predicate) Or(right Predicate) Predicate {
	return Predicate{
		left:  left,
		op:    opOR,
		right: right,
	}
}

func Not(right Predicate) Predicate {
	return Predicate{
		op:    opNot,
		right: right,
	}
}
