package orm

import (
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDeleter_Build(t *testing.T) {
	db := MemoryDB(t)
	type testCase[T any] struct {
		name    string
		s       *Deleter[T]
		want    *Query
		wantErr error
	}
	tests := []testCase[TestModel]{
		{
			name: "delete",
			s:    NewDeleter[TestModel](db),
			want: &Query{SQL: "DELETE FROM `test_model`;"},
		},

		{
			name: "from",
			s:    NewDeleter[TestModel](db).From("`test_model`"),
			want: &Query{SQL: "DELETE FROM `test_model`;"},
		},

		{
			name: "empty from",
			s:    NewDeleter[TestModel](db).From(""),
			want: &Query{SQL: "DELETE FROM `test_model`;"},
		},

		{
			name: "where",
			s:    NewDeleter[TestModel](db).Where(C("Id").Eq(18)),
			want: &Query{SQL: "DELETE FROM `test_model` WHERE `id` = ?;", Args: []any{18}},
		},

		{
			name: "not",
			s:    NewDeleter[TestModel](db).Where(Not(C("Id").Eq(18))),
			want: &Query{SQL: "DELETE FROM `test_model` WHERE NOT (`id` = ?);", Args: []any{18}},
		},

		{
			name: "and",
			s:    NewDeleter[TestModel](db).Where(C("Id").Eq(18).And(C("FirstName").Eq("hehe"))),
			want: &Query{SQL: "DELETE FROM `test_model` WHERE (`id` = ?) AND (`first_name` = ?);", Args: []any{18, "hehe"}},
		},

		{
			name: "or",
			s:    NewDeleter[TestModel](db).Where(C("Id").Eq(18).Or(C("FirstName").Eq("hehe"))),
			want: &Query{SQL: "DELETE FROM `test_model` WHERE (`id` = ?) OR (`first_name` = ?);", Args: []any{18, "hehe"}},
		},

		{
			name:    "invalid col",
			s:       NewDeleter[TestModel](db).Where(C("Id2").Eq(18).Or(C("FirstName2").Eq("hehe"))),
			wantErr: errs.NewErrUnknownField("Id2"),
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.want, got)
		})
	}
}
