//go:build e2e

package integration

import (
	"context"
	"gitee.com/mosache/YFrame/orm"
	"gitee.com/mosache/YFrame/orm/internal/test"
	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"testing"
	"time"
)

type InsertSuite struct {
	Suite
}

func (i *InsertSuite) TearDownTest() {
	res := orm.RawQuery[any](i.db, "TRUNCATE TABLE `simple_struct`").Exec(context.Background())
	require.NoError(i.T(), res.Err())
}

func TestMySQLInsert(t *testing.T) {
	suite.Run(t, &InsertSuite{Suite{
		driver: "mysql",
		dsn:    "root:root@tcp(localhost:13306)/integration_test",
	}})
}

func (i *InsertSuite) TestInsert() {
	db := i.db
	t := i.T()
	testCases := []struct {
		name string

		i *orm.Inserter[test.SimpleStruct]

		wantAffected int64
	}{
		{
			name:         "insert one",
			i:            orm.NewInserter[test.SimpleStruct](db).Values(test.NewSimpleStruct(13)),
			wantAffected: 1,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			defer cancel()
			res := tc.i.Exec(ctx)
			assert.NoError(t, res.Err())
			affected, err := res.RowsAffected()
			assert.NoError(t, err)
			assert.Equal(t, tc.wantAffected, affected)
		})
	}
}
