package valuer

import (
	"database/sql/driver"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"gitee.com/mosache/YFrame/orm/schema"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"testing"
)

func BenchmarkSetColumns(b *testing.B) {
	fn := func(b *testing.B, creator Creator) {
		mockRows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
		rowData := []driver.Value{"1", "tom", "18", "jerry"}
		for i := 0; i < b.N; i++ {
			mockRows.AddRow(rowData...)
		}

		r := schema.NewRegister()

		mockDB, mock, err := sqlmock.New()
		defer mockDB.Close()
		require.NoError(b, err)
		schema, err := r.Get(&reflect2.TestModel{})
		require.NoError(b, err)

		mock.ExpectQuery("SELECT *").WillReturnRows(mockRows)
		rows, err := mockDB.Query("SELECT id FROM user")
		require.NoError(b, err)

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			rows.Next()
			val := creator(schema, &reflect2.TestModel{})
			_ = val.SetColumn(rows)
		}
	}

	b.Run("reflect", func(b *testing.B) {
		fn(b, NewReflectValuer)
	})

	b.Run("unsafe", func(b *testing.B) {
		fn(b, NewUnsafeValuer)
	})

}
