package valuer

import (
	"database/sql"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"gitee.com/mosache/YFrame/orm/schema"
	"reflect"
	"unsafe"
)

type unSafeValuer struct {
	schema  *schema.Schema
	entity  any // *T
	address unsafe.Pointer
}

var _ Creator = NewUnsafeValuer

func NewUnsafeValuer(schema *schema.Schema, entity any) Valuer {
	address := reflect.ValueOf(entity).UnsafePointer()
	return &unSafeValuer{
		schema:  schema,
		entity:  entity,
		address: address,
	}
}

func (u *unSafeValuer) SetColumn(rows *sql.Rows) error {
	var (
		cs  []string
		err error
	)
	if cs, err = rows.Columns(); err != nil {
		return err
	}
	vals := make([]any, 0)
	for _, e := range cs {
		if f, ok := u.schema.ColumnMap[e]; ok {
			baseAddr := unsafe.Pointer(uintptr(u.address) + f.OffSet)
			val := reflect.NewAt(f.Typ, baseAddr)
			vals = append(vals, val.Interface())
		} else {
			return errs.NewErrUnknownColumn(e)
		}
	}
	err = rows.Scan(vals...)
	if err != nil {
		return errs.ErrScanErr
	}
	return nil
}

func (u *unSafeValuer) Field(fieldName string) (any, error) {
	fd, ok := u.schema.FieldMap[fieldName]
	if !ok {
		return nil, errs.NewErrUnknownField(fieldName)
	}

	bAddr := unsafe.Pointer(uintptr(u.address) + fd.OffSet)

	val := reflect.NewAt(fd.Typ, bAddr).Elem().Interface()

	return val, nil
}
