package valuer

import (
	"database/sql"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"gitee.com/mosache/YFrame/orm/schema"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_reflectValuer_SetColumn(t *testing.T) {
	testingSetColumn(t, NewReflectValuer)
}

func testingSetColumn(t *testing.T, creator Creator) {
	testCases := []struct {
		name string

		entity any
		rows   func() *sqlmock.Rows

		wantEntity any
	}{
		{
			name:   "set columns",
			entity: &reflect2.TestModel{},
			rows: func() *sqlmock.Rows {
				rows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
				rows.AddRow("1", "tom", "18", "jerry")
				return rows
			},
			wantEntity: &reflect2.TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "jerry",
					Valid:  true,
				},
			},
		},

		{
			name:   "diff order",
			entity: &reflect2.TestModel{},
			rows: func() *sqlmock.Rows {
				rows := sqlmock.NewRows([]string{"id", "last_name", "first_name", "age"})
				rows.AddRow("1", "jerry", "tom", "18")
				return rows
			},
			wantEntity: &reflect2.TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "jerry",
					Valid:  true,
				},
			},
		},

		{
			name:   "partial col",
			entity: &reflect2.TestModel{},
			rows: func() *sqlmock.Rows {
				rows := sqlmock.NewRows([]string{"id", "first_name"})
				rows.AddRow("1", "tom")
				return rows
			},
			wantEntity: &reflect2.TestModel{
				Id:        1,
				FirstName: "tom",
			},
		},
	}

	r := schema.NewRegister()

	mockDB, mock, err := sqlmock.New()
	defer mockDB.Close()
	require.NoError(t, err)
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			schema, err := r.Get(tc.entity)
			require.NoError(t, err)
			val := creator(schema, tc.entity)
			mockRows := tc.rows()
			mock.ExpectQuery("SELECT *").WillReturnRows(mockRows)
			rows, err := mockDB.Query("SELECT id FROM user")
			require.NoError(t, err)

			rows.Next()

			err = val.SetColumn(rows)
			require.NoError(t, err)
			assert.Equal(t, tc.wantEntity, tc.entity)
		})
	}
}
