package valuer

import (
	"database/sql"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"gitee.com/mosache/YFrame/orm/schema"
	"reflect"
)

type reflectValuer struct {
	schema *schema.Schema
	entity any // *T
}

var _ Creator = NewReflectValuer

func NewReflectValuer(schema *schema.Schema, entity any) Valuer {
	return &reflectValuer{
		schema: schema,
		entity: entity,
	}
}

func (r *reflectValuer) SetColumn(rows *sql.Rows) error {
	var (
		cs  []string
		err error
	)
	if cs, err = rows.Columns(); err != nil {
		return err
	}

	vals := make([]any, 0)
	for _, e := range cs {
		if f, ok := r.schema.ColumnMap[e]; ok {
			val := reflect.New(f.Typ)
			vals = append(vals, val.Interface())
		} else {
			return errs.NewErrUnknownColumn(e)
		}
	}
	err = rows.Scan(vals...)
	if err != nil {
		return errs.ErrScanErr
	}

	resultValue := reflect.ValueOf(r.entity)
	for i, c := range cs {
		if f, ok := r.schema.ColumnMap[c]; ok {
			resultValue.Elem().FieldByName(f.GoName).Set(reflect.ValueOf(vals[i]).Elem())
		}
	}
	return nil
}

func (r *reflectValuer) Field(fieldName string) (any, error) {
	_, ok := r.schema.FieldMap[fieldName]
	if !ok {
		return nil, errs.NewErrUnknownField(fieldName)
	}

	return reflect.ValueOf(r.entity).FieldByName(fieldName).Elem().Interface(), nil
}
