package valuer

import (
	"database/sql"
	"gitee.com/mosache/YFrame/orm/schema"
)

type Valuer interface {
	SetColumn(rows *sql.Rows) error

	Field(fieldName string) (any, error)
}

type Creator func(schema *schema.Schema, entity any) Valuer
