package errs

import (
	"errors"
	"fmt"
)

var (
	ErrPointerErr = errors.New("[orm]:pointer only")
	ErrNoRows     = errors.New("[orm]:没有数据")
	ErrScanErr    = errors.New("[orm]:scan err")
)

func NewErrUnknownField(name string) error {
	return fmt.Errorf("[orm]:未知字段:%s", name)
}

func NewErrTagFormatErr(tag string) error {
	return fmt.Errorf("[orm]:tag(%s)格式错误", tag)
}

func NewErrUnknownColumn(columnName string) error {
	return fmt.Errorf("[orm]:未知列明:%s", columnName)
}

func NewErrRollBackErr(bizErr error, rollBackErr error, panicked bool) error {
	return fmt.Errorf("[orm]:业务错误:%w,回滚错误：%s,panicked:%t", bizErr, rollBackErr, panicked)
}

func NewErrUnsupportedTableReferenceErr(table any) error {
	return fmt.Errorf("[orm]:不支持的tableReference类型:%v", table)
}
