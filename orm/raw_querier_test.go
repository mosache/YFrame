package orm

import (
	"context"
	"database/sql"
	"errors"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestRawQuerier_Get(t *testing.T) {
	sqlDB, mock, err := sqlmock.New()
	require.NoError(t, err)
	defer sqlDB.Close()

	db, err := OpenDB(sqlDB)
	require.NoError(t, err)

	mock.ExpectQuery("SELECT .*").WillReturnError(errors.New("query err"))

	// no rows
	mock.ExpectQuery("SELECT .* WHERE `id` < .*").WillReturnError(ErrNoRows)

	//data
	rows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	rows.AddRow("1", "tom", "18", "jerry")
	mock.ExpectQuery("SELECT .* WHERE `id` = .*").WillReturnRows(rows)

	//scan err
	rows = sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	rows.AddRow("aaa", "tom", "18", "jerry")
	mock.ExpectQuery("SELECT .* WHERE `id` = .*").WillReturnRows(rows)

	testCases := []struct {
		name string
		r    *RawQuerier[TestModel]

		wantErr error
		wantRes *TestModel
	}{
		{
			name:    "query err",
			r:       RawQuery[TestModel](db, "SELECT * FROM `test_model`"),
			wantErr: errors.New("query err"),
		},

		{
			name:    "no rows",
			r:       RawQuery[TestModel](db, "SELECT * FROM `test_model` WHERE `id` < ?", -1),
			wantErr: ErrNoRows,
		},

		{
			name: "data",
			r:    RawQuery[TestModel](db, "SELECT * FROM `test_model` WHERE `id` = ?", 1),
			wantRes: &TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "jerry",
					Valid:  true,
				},
			},
		},

		{
			name:    "scan err",
			r:       RawQuery[TestModel](db, "SELECT * FROM `test_model` WHERE `id` = ?", 3),
			wantErr: errs.ErrScanErr,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := tc.r.Get(context.Background())
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantRes, res)
		})
	}

}
