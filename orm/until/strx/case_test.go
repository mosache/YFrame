package strx

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestToSnakeCase(t *testing.T) {
	testCases := []struct {
		name string

		source string

		want string
	}{
		{
			name:   "TestModel",
			source: "TestModel",
			want:   "test_model",
		},

		{
			name:   "testModel",
			source: "testModel",
			want:   "test_model",
		},

		{
			name:   "testSSLModel",
			source: "testSSLModel",
			want:   "test_ssl_model",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := ToSnakeCase(tc.source)
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestToCapitalize(t *testing.T) {
	type args struct {
		source string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "normal",
			args: args{source: "cap"},
			want: "Cap",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, ToCapitalize(tt.args.source), "ToCapitalize(%v)", tt.args.source)
		})
	}
}
