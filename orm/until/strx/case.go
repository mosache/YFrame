package strx

import (
	"unicode"
)

// ToSnakeCase change ex. TestModel to test_model
func ToSnakeCase(source string) string {
	var out []rune

	for idx, r := range source {
		if idx == 0 {
			out = append(out, unicode.ToLower(r))
			continue
		}

		if unicode.IsUpper(r) && idx > 0 {
			/// 判断前一个字符是否是小写
			if unicode.IsLower(rune(source[idx-1])) {
				out = append(out, '_', unicode.ToLower(r))
				continue
			}

			/// 判断后一个字符是否是小写
			if idx < len(source)-1 && unicode.IsLower(rune(source[idx+1])) {
				out = append(out, '_', unicode.ToLower(r))
				continue
			}

			out = append(out, unicode.ToLower(r))
			continue
		}

		out = append(out, r)
	}

	return string(out)
}

/*
首字母转为大写
*/
func ToCapitalize(source string) string {
	ss := []rune(source)

	out := make([]rune, 0)
	for idx, e := range ss {
		if idx == 0 {
			out = append(out, unicode.ToUpper(e))
		} else {
			out = append(out, e)
		}

	}

	return string(out)
}
