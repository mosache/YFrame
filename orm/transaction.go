package orm

import (
	"context"
	"database/sql"
)

type Tx struct {
	tx *sql.Tx
	db *DB
}

func (tx *Tx) Commit() error {
	return tx.tx.Commit()
}

func (tx *Tx) RollBack() error {
	return tx.tx.Rollback()
}

func (tx *Tx) getCore() core {
	return tx.db.getCore()
}

func (tx *Tx) queryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error) {
	return tx.db.queryContext(ctx, query, args...)
}

func (tx *Tx) execContext(ctx context.Context, query string, args ...any) (sql.Result, error) {
	return tx.db.execContext(ctx, query, args...)
}
