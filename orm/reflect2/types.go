package reflect2

import "database/sql"

var (
	U = User{
		Name: "tom",
		age:  20,
	}
)

type User struct {
	Name string
	age  int
}

type TestModel struct {
	Id        int64
	FirstName string
	Age       int8
	LastName  *sql.NullString
}
