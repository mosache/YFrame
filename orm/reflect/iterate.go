package reflect

import "reflect"

func IterateArray(entity any) ([]any, error) {

	val := reflect.ValueOf(entity)

	res := make([]any, 0)

	for i := 0; i < val.Len(); i++ {
		ele := val.Index(i)
		res = append(res, ele.Interface())
	}

	return res, nil
}

// IterateMap 返回[]key,[]value,error
func IterateMap(entity any) ([]any, []any, error) {
	val := reflect.ValueOf(entity)

	keyArr := make([]any, 0, val.Len())
	valueArr := make([]any, 0, val.Len())

	keys := val.MapKeys()

	for _, e := range keys {
		keyArr = append(keyArr, e.Interface())
		valueArr = append(valueArr, val.MapIndex(e).Interface())
	}

	return keyArr, valueArr, nil

}
