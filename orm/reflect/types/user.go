package types

type User struct {
	Name string
	age  int
}

func NewUser(name string, age int) *User {
	return &User{
		Name: name,
		age:  age,
	}
}

func (u *User) GetName() string {
	return u.Name
}

func (u *User) GetAge() int {
	return u.age
}

//func (u *User) ChangeName(newVal string) {
//	u.Name = newVal
//}

func (u *User) private() {
	return
}
