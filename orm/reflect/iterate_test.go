package reflect

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIterateArray(t *testing.T) {

	testCases := []struct {
		name string

		entity any

		want    []any
		wantErr error
	}{
		{
			name:   "[]int",
			entity: []int{1, 2, 3},
			want:   []any{1, 2, 3},
		},

		{
			name:   "[3]int",
			entity: [3]int{1, 2, 3},
			want:   []any{1, 2, 3},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			vals, err := IterateArray(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.want, vals)
		})
	}

}

func TestIterateMap(t *testing.T) {
	testCases := []struct {
		name string

		entity any

		wantKeys   []any
		wantvalues []any
		wantErr    error
	}{
		{
			name:       "map",
			entity:     map[string]string{"A": "a", "B": "b"},
			wantKeys:   []any{"A", "B"},
			wantvalues: []any{"a", "b"},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			keys, values, err := IterateMap(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.EqualValues(t, tc.wantKeys, keys)
			assert.EqualValues(t, tc.wantvalues, values)
		})
	}
}
