package reflect

import "reflect"

type FuncInfo struct {
	Name        string
	InputTypes  []reflect.Type
	OutputTypes []reflect.Type
	Result      []any
}

func IterateFuncs(entity any) (map[string]FuncInfo, error) {
	m := make(map[string]FuncInfo)

	typ := reflect.TypeOf(entity)

	//for typ.Kind() == reflect.Pointer {
	//	typ = typ.Elem()
	//}

	for i := 0; i < typ.NumMethod(); i++ {
		method := typ.Method(i)

		fn := method.Func

		input := fn.Type().NumIn()
		output := fn.Type().NumOut()

		var inTypes []reflect.Type
		var outTypes []reflect.Type

		inputValues := make([]reflect.Value, 0)

		// struct input 第一个值是类型本身
		inputValues = append(inputValues, reflect.ValueOf(entity))
		inTypes = append(inTypes, reflect.TypeOf(entity))
		for i := 1; i < input; i++ {
			in := fn.Type().In(i)
			inTypes = append(inTypes, in)
			inputValues = append(inputValues, reflect.Zero(in))
		}

		for i := 0; i < output; i++ {
			out := fn.Type().Out(i)
			outTypes = append(outTypes, out)
		}

		resultValues := fn.Call(inputValues)

		var result []any

		for _, e := range resultValues {
			result = append(result, e.Interface())
		}

		m[method.Name] = FuncInfo{
			Name:        method.Name,
			InputTypes:  inTypes,
			OutputTypes: outTypes,
			Result:      result,
		}
	}

	return m, nil
}
