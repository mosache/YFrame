package reflect

import (
	"gitee.com/mosache/YFrame/orm/reflect/types"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestIterateFuncs(t *testing.T) {
	testCases := []struct {
		name   string
		entity any

		want    map[string]FuncInfo
		wantErr error
	}{
		{
			name:   "struct",
			entity: types.NewUser("tom", 18),
			want: map[string]FuncInfo{
				"GetName": {
					Name:        "GetName",
					InputTypes:  []reflect.Type{reflect.TypeOf(&types.User{})},
					OutputTypes: []reflect.Type{reflect.TypeOf("")},
					Result:      []any{"tom"},
				},
				"GetAge": {
					Name:        "GetAge",
					InputTypes:  []reflect.Type{reflect.TypeOf(&types.User{})},
					OutputTypes: []reflect.Type{reflect.TypeOf(0)},
					Result:      []any{18},
				},
				//"ChangeName": {
				//	Name:       "ChangeName",
				//	InputTypes: []reflect.Type{reflect.TypeOf("")},
				//},
			},
			wantErr: nil,
		},
	}

	for _, tc := range testCases {
		got, err := IterateFuncs(tc.entity)
		assert.Equal(t, tc.wantErr, err)
		if err != nil {
			return
		}
		assert.Equal(t, tc.want, got)
	}

}
