package reflect

import (
	"errors"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIterateFields(t *testing.T) {
	type User struct {
		Name string
		age  int
	}

	testCases := []struct {
		name string

		entity any

		wantErr error
		wantRes map[string]any
	}{
		{
			name: "struct",
			entity: User{
				Name: "tom",
				age:  20,
			},
			wantErr: nil,
			wantRes: map[string]any{"Name": "tom", "age": 0},
		},

		{
			name: "pointer",
			entity: &User{
				Name: "tom",
				age:  20,
			},
			wantErr: nil,
			wantRes: map[string]any{"Name": "tom", "age": 0},
		},

		{
			name: "multi pointer",
			entity: func() interface{} {
				u := &User{
					Name: "tom",
					age:  20,
				}
				pu := &u
				return &pu
			}(),
			wantErr: nil,
			wantRes: map[string]any{"Name": "tom", "age": 0},
		},

		{
			name:    "base type",
			entity:  18,
			wantErr: errors.New("[orm] 不支持类型"),
		},

		{
			name:    "nil",
			entity:  nil,
			wantErr: errors.New("[orm] 不支持类型"),
		},

		{
			name:    "user nil",
			entity:  (*User)(nil),
			wantErr: errors.New("[orm] 不支持零值"),
		},

		{
			name:    "other pkg user",
			entity:  reflect2.U,
			wantErr: nil,
			wantRes: map[string]any{"Name": "tom", "age": 0},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := IterateFields(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantRes, res)
		})
	}
}

func TestSetField(t *testing.T) {
	type User struct {
		Name string
		age  int
	}

	testCases := []struct {
		name string

		entity    any
		fieldName string
		newVal    any
		wantErr   error

		wantEntity any
	}{
		{
			name: "struct",
			entity: User{
				Name: "tom",
			},
			fieldName: "Name",
			newVal:    "hehe",
			wantErr:   errors.New("[orm] 无法设置值"),
		},

		{
			name: "struct pointer",
			entity: &User{
				Name: "tom",
			},
			fieldName: "Name",
			newVal:    "hehe",
			wantEntity: &User{
				Name: "hehe",
			},
		},

		{
			name: "field unexported",
			entity: &User{
				Name: "tom",
			},
			fieldName: "age",
			newVal:    20,
			wantErr:   errors.New("[orm] 无法设置值"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := SetField(tc.entity, tc.fieldName, tc.newVal)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantEntity, tc.entity)

		})
	}

}
