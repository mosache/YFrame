package reflect

import (
	"errors"
	"reflect"
)

// IterateFields 遍历属性
func IterateFields(entity any) (map[string]any, error) {
	if entity == nil {
		return nil, errors.New("[orm] 不支持类型")
	}

	m := make(map[string]any)

	typ := reflect.TypeOf(entity)
	val := reflect.ValueOf(entity)

	for typ.Kind() == reflect.Pointer {
		typ = typ.Elem()
		val = val.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return nil, errors.New("[orm] 不支持类型")
	}

	if !val.IsValid() {
		return nil, errors.New("[orm] 不支持零值")
	}

	if val.IsValid() && val.IsZero() {
		return nil, errors.New("[orm] 不支持零值")
	}

	for i := 0; i < typ.NumField(); i++ {
		fieldType := typ.Field(i)
		fieldValue := val.Field(i)

		if fieldType.IsExported() {
			m[fieldType.Name] = fieldValue.Interface()
		} else {
			m[fieldType.Name] = reflect.Zero(fieldType.Type).Interface()
		}

	}

	return m, nil
}

func SetField(entity any, fieldName string, newVal any) error {

	val := reflect.ValueOf(entity)

	for val.Kind() == reflect.Pointer {
		val = val.Elem()
	}

	fieldVal := val.FieldByName(fieldName)

	if !fieldVal.CanSet() {
		return errors.New("[orm] 无法设置值")
	}

	fieldVal.Set(reflect.ValueOf(newVal))

	return nil
}
