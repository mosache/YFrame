package orm

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestInserter_Build(t *testing.T) {
	db := MemoryDB(t)
	testCases := []struct {
		name string

		q QueryBuilder

		wantErr   error
		wantQuery *Query
	}{
		{
			name: "one row",
			q: NewInserter[TestModel](db).Values(&TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "haha",
					Valid:  true,
				},
			}),
			wantQuery: &Query{
				SQL: "INSERT INTO `test_model`(`id`,`first_name`,`age`,`last_name`) VALUES (?,?,?,?);",
				Args: []any{int64(1), "tom", int8(18), &sql.NullString{
					String: "haha",
					Valid:  true,
				}},
			},
		},

		{
			name: "multi row",
			q: NewInserter[TestModel](db).Values(&TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "haha",
					Valid:  true,
				},
			},
				&TestModel{
					Id:        2,
					FirstName: "tom2",
					Age:       19,
					LastName: &sql.NullString{
						String: "haha2",
						Valid:  true,
					},
				},
			),
			wantQuery: &Query{
				SQL: "INSERT INTO `test_model`(`id`,`first_name`,`age`,`last_name`) VALUES (?,?,?,?),(?,?,?,?);",
				Args: []any{
					int64(1), "tom",
					int8(18), &sql.NullString{
						String: "haha",
						Valid:  true,
					},
					int64(2), "tom2",
					int8(19),
					&sql.NullString{
						String: "haha2",
						Valid:  true,
					},
				},
			},
		},

		{
			name: "with columns",
			q: NewInserter[TestModel](db).Columns("Id", "FirstName").Values(&TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "haha",
					Valid:  true,
				},
			}),
			wantQuery: &Query{
				SQL:  "INSERT INTO `test_model`(`id`,`first_name`) VALUES (?,?);",
				Args: []any{int64(1), "tom"},
			},
		},

		{
			name: "multi row with column",
			q: NewInserter[TestModel](db).Columns("Id", "LastName").Values(&TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "haha",
					Valid:  true,
				},
			},
				&TestModel{
					Id:        2,
					FirstName: "tom2",
					Age:       19,
					LastName: &sql.NullString{
						String: "haha2",
						Valid:  true,
					},
				},
			),
			wantQuery: &Query{
				SQL: "INSERT INTO `test_model`(`id`,`last_name`) VALUES (?,?),(?,?);",
				Args: []any{
					int64(1), &sql.NullString{
						String: "haha",
						Valid:  true,
					},
					int64(2),
					&sql.NullString{
						String: "haha2",
						Valid:  true,
					},
				},
			},
		},

		{
			name: "upsert",
			q: NewInserter[TestModel](db).Values(&TestModel{
				Id:        1,
				FirstName: "tom",
				Age:       18,
				LastName: &sql.NullString{
					String: "haha",
					Valid:  true,
				},
			}).OnDuplicateKey().Update(Assign("Age", 1), C("LastName")),
			wantQuery: &Query{
				SQL: "INSERT INTO `test_model`(`id`,`first_name`,`age`,`last_name`) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE `age` = ?,`last_name`=VALUES(`last_name`);",
				Args: []any{int64(1), "tom", int8(18), &sql.NullString{
					String: "haha",
					Valid:  true,
				}, 1},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			q, err := tc.q.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantQuery, q)
		})
	}

}

func TestInserter_Exec(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	require.NoError(t, err)

	db, err := OpenDB(mockDB)
	require.NoError(t, err)

	testCases := []struct {
		name string

		i *Inserter[TestModel]

		wantErr error

		affected int64
	}{
		{
			name: "invalid col",
			i: func() *Inserter[TestModel] {
				i := NewInserter[TestModel](db).Columns("invalidCol").Values(&TestModel{
					Id:        1,
					FirstName: "hehe",
					Age:       18,
					LastName: &sql.NullString{
						String: "haha",
						Valid:  true,
					},
				})

				return i
			}(),
			wantErr: errs.NewErrUnknownField("invalidCol"),
		},

		{
			name: "db err",
			i: func() *Inserter[TestModel] {
				i := NewInserter[TestModel](db).Values(&TestModel{})

				mock.ExpectExec("INSERT .*").WillReturnError(errors.New("db err"))

				return i
			}(),
			wantErr: errors.New("db err"),
		},

		{
			name: "result",
			i: func() *Inserter[TestModel] {
				res := driver.RowsAffected(1)
				i := NewInserter[TestModel](db).Values(&TestModel{})

				mock.ExpectExec("INSERT .*").WillReturnResult(res)

				return i
			}(),
			affected: 1,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r := tc.i.Exec(context.Background())
			assert.Equal(t, tc.wantErr, r.Err())
			if r.Err() != nil {
				return
			}
			affected, err := r.RowsAffected()
			require.NoError(t, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.affected, affected)

		})
	}
}
