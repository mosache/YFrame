package orm

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSelector_GroupBy(t *testing.T) {
	db := MemoryDB(t)
	testCases := []struct {
		name string
		s    *Selector[TestModel]

		wantErr error

		wantQuery *Query
	}{
		{
			name: "group by col",
			s:    NewSelector[TestModel](db).GroupBy(C("FirstName")),
			wantQuery: &Query{
				SQL: "SELECT * FROM `test_model` GROUP BY `first_name`;",
			},
		},

		{
			name: "group by multi col",
			s:    NewSelector[TestModel](db).GroupBy(C("FirstName"), C("Id")),
			wantQuery: &Query{
				SQL: "SELECT * FROM `test_model` GROUP BY `first_name`,`id`;",
			},
		},

		{
			name: "group by expression",
			s:    NewSelector[TestModel](db).GroupBy(C("FirstName").Plus(C("Id"))),
			wantQuery: &Query{
				SQL: "SELECT * FROM `test_model` GROUP BY `first_name` + `id`;",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			q, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantQuery, q)
		})
	}
}

func TestSelector_Having(t *testing.T) {
	db := MemoryDB(t)
	testCases := []struct {
		name string
		s    *Selector[TestModel]

		wantErr error

		wantQuery *Query
	}{
		{
			name: "having by col",
			s:    NewSelector[TestModel](db).Having(C("Id").LT(3)),
			wantQuery: &Query{
				SQL:  "SELECT * FROM `test_model` HAVING `id` < ?;",
				Args: []any{3},
			},
		},

		{
			name: "having by aggregate",
			s:    NewSelector[TestModel](db).Having(Avg("id").LT(1)),
			wantQuery: &Query{
				SQL:  "SELECT * FROM `test_model` HAVING AVG(`id`) < ?;",
				Args: []any{1},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			q, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantQuery, q)
		})
	}
}

func TestSelector_Limit(t *testing.T) {
	db := MemoryDB(t)
	testCases := []struct {
		name string
		s    *Selector[TestModel]

		wantErr error

		wantQuery *Query
	}{
		{
			name: "limit 1",
			s:    NewSelector[TestModel](db).Limit(1),
			wantQuery: &Query{
				SQL:  "SELECT * FROM `test_model` LIMIT ? OFFSET ?;",
				Args: []any{1, 0},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			q, err := tc.s.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}

			assert.Equal(t, tc.wantQuery, q)
		})
	}
}
