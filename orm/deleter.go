package orm

import (
	"context"
	"database/sql"
)

type Deleter[T any] struct {
	builder
	tableName string
	where     []Predicate

	db *DB
}

func NewDeleter[T any](db *DB) *Deleter[T] {
	return &Deleter[T]{
		db: db,
	}
}

func (d *Deleter[T]) Build() (*Query, error) {

	m, err := d.db.r.Get(new(T))
	if err != nil {
		return nil, err
	}

	d.m = m

	d.sb.WriteString("DELETE FROM")
	d.sb.WriteString(" ")

	/// tableName
	if len(d.tableName) == 0 {
		d.sb.WriteString("`")
		d.sb.WriteString(d.m.TableName)
		d.sb.WriteString("`")
	} else {
		d.sb.WriteString(d.tableName)
	}

	///where
	if len(d.where) > 0 {
		d.sb.WriteString(" WHERE ")

		//p := d.where[0]
		//
		//for i := 1; i < len(d.where); i++ {
		//	p = p.And(d.where[i])
		//}
		//
		if err = d.buildPredicate(d.where); err != nil {
			return nil, err
		}
	}

	d.sb.WriteString(";")

	return &Query{
		SQL:  d.sb.String(),
		Args: d.args,
	}, nil

}

//func (d *Deleter[T]) buildExpression(expression Expression) error {
//	switch exp := expression.(type) {
//	case Predicate:
//
//		var (
//			leftPredicate  bool
//			rightPredicate bool
//		)
//		_, leftPredicate = exp.left.(Predicate)
//		_, rightPredicate = exp.right.(Predicate)
//
//		if leftPredicate {
//			d.sb.WriteString("(")
//		}
//		if err := d.buildExpression(exp.left); err != nil {
//			return err
//		}
//		if leftPredicate {
//			d.sb.WriteString(")")
//		}
//
//		d.sb.WriteString(exp.op.string())
//
//		if rightPredicate {
//			d.sb.WriteString("(")
//		}
//		if err := d.buildExpression(exp.right); err != nil {
//			return err
//		}
//		if rightPredicate {
//			d.sb.WriteString(")")
//		}
//
//	case Column:
//		var (
//			f  *Field
//			ok bool
//		)
//		if f, ok = d.m.fields[exp.Name]; !ok {
//			return errs.NewErrUnknownField(exp.Name)
//		}
//		d.sb.WriteString(f.columnName)
//	case value:
//		d.sb.WriteString("?")
//		d.addArg(exp.val)
//	}
//
//	return nil
//}

//func (d *Deleter[T]) addArg(val any) {
//	if d.args == nil {
//		d.args = make([]any, 0)
//	}
//
//	d.args = append(d.args, val)
//}

func (d *Deleter[T]) From(tableName string) *Deleter[T] {
	d.tableName = tableName
	return d
}

func (d *Deleter[T]) Where(e Predicate) *Deleter[T] {
	d.where = append(d.where, e)
	return d
}

func (d *Deleter[T]) Exec(ctx context.Context) (sql.Result, error) {
	//TODO implement me
	panic("implement me")
}
