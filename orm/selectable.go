package orm

type Selectable interface {
	Select()
}
