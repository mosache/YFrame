package orm

// Assignable
// 可用于update语句的SET,ON DUPLICATE KEY UPDATE后
type Assignable interface {
	Assign()
}
