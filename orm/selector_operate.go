package orm

func (s *Selector[T]) Select(cols ...Selectable) *Selector[T] {
	s.columns = cols
	return s
}

func (s *Selector[T]) From(table TableReference) *Selector[T] {
	s.table = table
	return s
}

func (s *Selector[T]) Where(e Predicate) *Selector[T] {
	s.where = append(s.where, e)
	return s
}

func (s *Selector[T]) GroupBy(groupBy ...Expression) *Selector[T] {
	s.groupBy = groupBy
	return s
}

func (s *Selector[T]) Having(having ...Predicate) *Selector[T] {
	s.having = having
	return s
}

func (s *Selector[T]) Limit(limit int) *Selector[T] {
	s.limit = limit
	return s
}

func (s *Selector[T]) OffSet(offSet int) *Selector[T] {
	s.offSet = offSet
	return s
}
