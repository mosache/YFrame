package orm

type Expression interface {
	expr()
}

// RawExpression raw expression
// will not do anything to this
type RawExpression struct {
	Expr string
	Args []any
}

func (r RawExpression) Select() {}
func (r RawExpression) expr()   {}

func Raw(expr string, args ...any) RawExpression {
	return RawExpression{
		Expr: expr,
		Args: args,
	}
}

func (r RawExpression) AsPredicate() Predicate {
	return Predicate{
		left: r,
	}
}
