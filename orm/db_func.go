package orm

import (
	"context"
	"database/sql"
)

func (db *DB) BeginTx(ctx context.Context, opts *sql.TxOptions) (*Tx, error) {

	tx, err := db.db.BeginTx(ctx, opts)

	if err != nil {
		return nil, err
	}

	return &Tx{tx: tx}, nil
}
