package sql

import (
	"context"
	"errors"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"log"
	"testing"
	"time"
)

func TestSqlMock(t *testing.T) {
	db, mock, err := sqlmock.New()
	require.NoError(t, err)
	defer db.Close()

	mockRows := sqlmock.NewRows([]string{"id", "first_name"})
	mockRows.AddRow(1, "tom")
	mock.ExpectQuery("SELECT id,first_name FROM `user`.*").WillReturnRows(mockRows)
	mock.ExpectQuery("SELECT id FROM `user`.*").WillReturnError(errors.New("mock err"))

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	rows, err := db.QueryContext(ctx, "SELECT id,first_name FROM `user` WHERE id = 1")
	require.NoError(t, err)
	for rows.Next() {
		tm := reflect2.TestModel{}
		err = rows.Scan(&tm.Id, &tm.FirstName)
		require.NoError(t, err)
		log.Println(tm)
	}

	_, err = db.QueryContext(ctx, "SELECT id FROM `user` WHERE id = 1")
	require.Error(t, errors.New("mock err"), err)

	cancel()
}
