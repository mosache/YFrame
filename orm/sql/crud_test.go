package sql

import (
	"context"
	"database/sql"
	"gitee.com/mosache/YFrame/orm/reflect2"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestDB(t *testing.T) {
	db, err := sql.Open("sqlite3", "file:test.db?cache=shared&mode=memory")
	require.NoError(t, err)

	db.Ping()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	_, err = db.ExecContext(ctx, `
		CREATE TABLE IF NOT EXISTS test_model(
		    id INTEGER PRIMARY KEY,
		    first_name TEXT NOT NULL,
		    age INTEGER,
		    last_name TEXT NOT NULL 
		)
	`)
	require.NoError(t, err)

	res, err := db.ExecContext(ctx, `INSERT INTO test_model VALUES (?,?,?,?)`, 1, "tom", 18, "jack")
	require.NoError(t, err)

	affected, err := res.RowsAffected()
	require.NoError(t, err)
	t.Logf("受影响行数：%d\n", affected)

	id, err := res.LastInsertId()
	require.NoError(t, err)

	t.Logf("最后插入id：%d\n", id)

	row := db.QueryRowContext(ctx, `SELECT * FROM test_model WHERE id = ?`, 1)
	require.NoError(t, row.Err())
	m := reflect2.TestModel{}
	err = row.Scan(&m.Id, &m.FirstName, &m.Age, &m.LastName)
	require.NoError(t, err)

	row = db.QueryRowContext(ctx, `SELECT * FROM test_model WHERE id = ?`, 2)
	require.NoError(t, row.Err())
	m = reflect2.TestModel{}
	err = row.Scan(&m.Id, &m.FirstName, &m.Age, &m.LastName)
	require.Error(t, sql.ErrNoRows, err)
	cancel()

}
