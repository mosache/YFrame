package queryLog

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/orm"
)

type MiddlewareBuilder struct {
	logFunc func(sql string, arg []any)
}

func New() *MiddlewareBuilder {
	return &MiddlewareBuilder{logFunc: func(sql string, arg []any) {
		fmt.Printf("SQL:%s,arg:%v", sql, arg)
	}}
}

func (m *MiddlewareBuilder) LogFunc(logFunc func(sql string, arg []any)) *MiddlewareBuilder {
	m.logFunc = logFunc
	return m
}

func (m *MiddlewareBuilder) Build() orm.Middleware {
	return func(next orm.Handler) orm.Handler {
		return func(ctx context.Context, qc *orm.QueryContext) *orm.QueryResult {
			q, err := qc.Builder.Build()
			if err != nil {
				return &orm.QueryResult{
					Result: nil,
					Err:    err,
				}
			}
			m.logFunc(q.SQL, q.Args)
			res := next(ctx, qc)
			return res
		}
	}
}
