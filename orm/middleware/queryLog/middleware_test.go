package queryLog

import (
	"context"
	"database/sql"
	"gitee.com/mosache/YFrame/orm"
	"gitee.com/mosache/YFrame/orm/reflect2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

func TestMiddlewareBuilder(t *testing.T) {
	var query string
	var args []any
	m := New().LogFunc(func(sql string, arg []any) {
		query = sql
		args = arg
	}).Build()

	db, err := orm.Open("sqlite3", "file:test.db?cache=shared&mode=memory", orm.DBWithMdls(m))
	require.NoError(t, err)

	_, err = orm.NewSelector[reflect2.TestModel](db).Where(orm.C("Id").Eq(1)).Get(context.Background())
	assert.Equal(t, "SELECT * FROM `test_model` WHERE `id` = ?;", query)
	assert.Equal(t, []any{1}, args)

	_ = orm.NewInserter[reflect2.TestModel](db).Values(&reflect2.TestModel{
		Id:        1,
		FirstName: "hehe",
		Age:       19,
		LastName: &sql.NullString{
			String: "haa",
			Valid:  true,
		},
	}).Exec(context.Background())
	assert.Equal(t, "INSERT INTO `test_model`(`id`,`first_name`,`age`,`last_name`) VALUES (?,?,?,?);", query)
	assert.Equal(t, []any{int64(1), "hehe", int8(19), &sql.NullString{
		String: "haa",
		Valid:  true,
	}}, args)

}
