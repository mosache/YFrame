package orm

import (
	"context"
	"database/sql"
)

type RawQuerier[T any] struct {
	sql  string
	args []any
	core
	sess session
}

func RawQuery[T any](sess session, sql string, args ...any) *RawQuerier[T] {
	return &RawQuerier[T]{
		sql:  sql,
		args: args,
		core: sess.getCore(),
		sess: sess,
	}
}

func (r *RawQuerier[T]) Exec(ctx context.Context) Result {
	res := exec(ctx, r.sess, r.core, &QueryContext{
		Type:    Insert,
		Builder: r,
	})

	if res.Result != nil {
		return Result{
			res: res.Result.(sql.Result),
			err: res.Err,
		}
	}

	return Result{
		res: nil,
		err: res.Err,
	}
}

func (r *RawQuerier[T]) Build() (*Query, error) {
	return &Query{
		SQL:  r.sql,
		Args: r.args,
	}, nil
}

func (r *RawQuerier[T]) Get(ctx context.Context) (*T, error) {

	res := get[T](ctx, r.sess, r.core, &QueryContext{
		Type:    RAW,
		Builder: r,
	})

	if res.Result != nil {
		return res.Result.(*T), res.Err
	}

	return nil, res.Err

	if res.Result != nil {
		return res.Result.(*T), res.Err
	}

	return nil, res.Err
}

func (r *RawQuerier[T]) GetMulti(ctx context.Context) ([]*T, error) {
	//TODO implement me
	panic("implement me")
}
