package orm

type Assignment struct {
	Column string
	Val    any
}

func Assign(col string, val any) Assignment {
	return Assignment{
		Column: col,
		Val:    val,
	}
}

func (Assignment) Assign() {}
