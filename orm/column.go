package orm

type Column struct {
	Name  string
	Alias string         // 别名
	Table TableReference // table
}

func (c Column) expr()   {}
func (c Column) Select() {}
func (Column) Assign()   {}

func C(columnName string) Column {
	return Column{
		Name: columnName,
	}
}

func (c Column) As(alias string) Column {
	return Column{
		Name:  c.Name,
		Alias: alias,
	}
}

func (c Column) Plus(val any) Predicate {
	return Predicate{
		left:  c,
		op:    opPlus,
		right: valueOf(val),
	}
}

func (c Column) Eq(val any) Predicate {
	return Predicate{
		left:  c,
		op:    opEq,
		right: valueOf(val),
	}
}

func (c Column) LT(val any) Predicate {
	return Predicate{
		left:  c,
		op:    opLt,
		right: value{val: val},
	}
}

func valueOf(arg any) Expression {
	switch val := arg.(type) {
	case Expression:
		return val
	default:
		return value{val: val}
	}
}
