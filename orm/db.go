package orm

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"gitee.com/mosache/YFrame/orm/internal/errs"
	"gitee.com/mosache/YFrame/orm/internal/valuer"
	"gitee.com/mosache/YFrame/orm/schema"
	"log"
)

type DB struct {
	core
	db *sql.DB
}

type DBOption func(db *DB)

func DBWithDialect(dialect Dialect) DBOption {
	return func(db *DB) {
		db.dialect = dialect
		db.quoter = dialect.Quoter()
	}
}

func DBWithRegister(r *schema.Register) DBOption {
	return func(db *DB) {
		db.r = r
	}
}

func DBWithCreator(creator valuer.Creator) DBOption {
	return func(db *DB) {
		db.creator = creator
	}
}

func DBWithMdls(mdls ...Middleware) DBOption {
	return func(db *DB) {
		db.mdls = mdls
	}
}

///==================================================================================================

func Open(driveName string, dataSourceName string, opts ...DBOption) (*DB, error) {
	sqlDB, err := sql.Open(driveName, dataSourceName)
	if err != nil {
		return nil, err
	}
	dialect := StandSQL{}
	db := &DB{
		core: core{
			r:       schema.NewRegister(),
			creator: valuer.NewUnsafeValuer,
			dialect: dialect,
			quoter:  dialect.Quoter(),
		},
		db: sqlDB,
	}

	for _, e := range opts {
		e(db)
	}

	return db, nil
}

func OpenDB(sqlDB *sql.DB, opts ...DBOption) (*DB, error) {
	dialect := StandSQL{}
	db := &DB{
		db: sqlDB,
		core: core{
			r:       schema.NewRegister(),
			creator: valuer.NewUnsafeValuer,
			dialect: dialect,
			quoter:  dialect.Quoter(),
		},
	}

	for _, e := range opts {
		e(db)
	}

	return db, nil
}

func MustNewDB(driveName string, dataSourceName string, opts ...DBOption) *DB {
	db, err := Open(driveName, dataSourceName, opts...)
	if err != nil {
		panic(err)
	}

	return db
}

func (db *DB) getCore() core {
	return db.core
}

func (db *DB) queryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error) {
	return db.db.QueryContext(ctx, query, args...)
}

func (db *DB) execContext(ctx context.Context, query string, args ...any) (sql.Result, error) {
	return db.db.ExecContext(ctx, query, args...)
}

func (db *DB) DoTx(context context.Context, fn func(ctx context.Context, tx *Tx) error, opts *sql.TxOptions) (err error) {
	var tx *Tx
	tx, err = db.BeginTx(context, opts)
	if err != nil {
		return err
	}

	panicked := true
	defer func() {
		if panicked || err != nil {
			rErr := tx.RollBack()
			err = errs.NewErrRollBackErr(err, rErr, panicked)
		} else {
			err = tx.Commit()
		}
	}()

	err = fn(context, tx)
	if err != nil {
		return err
	}
	panicked = false

	return err
}

func (db *DB) Wait() error {
	err := db.db.Ping()
	for err == driver.ErrBadConn {
		log.Println("wait for db to start...")
		err = db.db.Ping()
	}
	return nil
}
