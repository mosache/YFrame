//go:build e2e

package rest

import (
	"fmt"
	"gitee.com/mosache/YFrame/core/http"
	"testing"
)

func TestServer(t *testing.T) {
	h := NewHttpServer()

	h.addRoute(http.MethodGet, "/user", func(ctx *Context) {
		println("hehe")
	})

	h.Get("/order/detail", func(ctx *Context) {
		ctx.Resp.Write([]byte("hello world"))
	})

	h.Get("/*/order/*", func(ctx *Context) {
		ctx.Resp.Write([]byte("hello world2"))
	})

	h.Get("/order/:id", func(ctx *Context) {
		ctx.Resp.Write([]byte(fmt.Sprintf("%v", ctx.Params["id"])))
	})

	h.Get("/:id", func(ctx *Context) {
		ctx.Resp.Write([]byte(fmt.Sprintf("%v", ctx.Params["id"])))
	})

	_ = h.Start(":8080")
}
