package prometheus

import (
	"gitee.com/mosache/YFrame/rest"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"math/rand"
	"net/http"
	"testing"
	"time"
)

func TestMiddlewareBuilder_Build(t *testing.T) {
	builder := MiddlewareBuilder{
		Namespace: "vurtne_test",
		Subsystem: "web",
		Name:      "http_response",
	}
	srv := rest.NewHttpServer(rest.ServerWithMiddleware(builder.Build()))

	srv.Get("/user", func(ctx *rest.Context) {
		val := rand.Intn(1000) + 1
		time.Sleep(time.Duration(val) * time.Millisecond)
		ctx.Resp.WriteHeader(200)
		ctx.Resp.Write([]byte("hehe"))
	})

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		http.ListenAndServe(":8082", nil)
	}()

	srv.Start(":8086")

}
