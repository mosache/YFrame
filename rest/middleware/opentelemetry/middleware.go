package opentelemetry

import (
	"gitee.com/mosache/YFrame/rest"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

const instrumentationName = "gitee.com/mosache/YFrame/rest/middle/opentelemetry"

type MiddlewareBuilder struct {
	Tracer trace.Tracer
}

func (m MiddlewareBuilder) Build() rest.MiddleWare {
	if m.Tracer == nil {
		m.Tracer = otel.GetTracerProvider().Tracer(instrumentationName)
	}

	return func(next rest.HandleFunc) rest.HandleFunc {
		return func(ctx *rest.Context) {

			reqCtx := ctx.Req.Context()

			reqCtx = otel.GetTextMapPropagator().Extract(reqCtx, propagation.HeaderCarrier(ctx.Req.Header))

			reqCtx, span := m.Tracer.Start(reqCtx, "unknown")
			defer span.End()

			span.SetAttributes(attribute.String("http.method", ctx.Req.Method))
			span.SetAttributes(attribute.String("http.url", ctx.Req.URL.String()))

			ctx.Req = ctx.Req.WithContext(reqCtx)

			next(ctx)

			span.SetName(ctx.MatchedRoute)

			span.SetAttributes(attribute.Int("http.status", ctx.RespStatusCode))
		}
	}
}
