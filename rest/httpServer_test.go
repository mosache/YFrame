package rest

import (
	"fmt"
	"net/http"
	"testing"
)

func Test_httpServer_ServeHTTP(t *testing.T) {
	svc := NewHttpServer()

	svc.middleWares = []MiddleWare{
		func(next HandleFunc) HandleFunc {
			return func(ctx *Context) {
				fmt.Println("first before")
				next(ctx)
				fmt.Println("first after")
			}
		},

		func(next HandleFunc) HandleFunc {
			return func(ctx *Context) {
				fmt.Println("second before")
				next(ctx)
				fmt.Println("second after")
			}
		},

		func(next HandleFunc) HandleFunc {
			return func(ctx *Context) {
				fmt.Println("third stop")
			}
		},

		func(next HandleFunc) HandleFunc {
			return func(ctx *Context) {
				fmt.Println("four can not see")
			}
		},
	}

	svc.ServeHTTP(nil, &http.Request{})
}
