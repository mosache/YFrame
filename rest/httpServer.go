package rest

import (
	"net"
	"net/http"
)

var _ Server = &httpServer{}

type httpServer struct {
	*router

	middleWares []MiddleWare
}

func (h *httpServer) addRoute(method string, path string, handleFunc HandleFunc) {
	h.router.addRoute(method, path, handleFunc)
}

type HttpServerOption func(s *httpServer)

func NewHttpServer(opts ...HttpServerOption) *httpServer {
	s := &httpServer{
		router: newRouter(),
	}

	for _, e := range opts {
		e(s)
	}

	return s
}

func ServerWithMiddleware(ms ...MiddleWare) HttpServerOption {
	return func(s *httpServer) {
		s.middleWares = append(s.middleWares, ms...)
	}
}

//==============================================================================================

func (h *httpServer) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	ctx := &Context{
		Req:  request,
		Resp: writer,
	}

	root := h.Serve

	for i := len(h.middleWares) - 1; i >= 0; i-- {
		root = h.middleWares[i](root)
	}

	root(ctx)
}

func (h *httpServer) Start(addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	return http.Serve(l, h)
}

func (h *httpServer) Serve(ctx *Context) {
	path := ctx.Req.URL.Path
	method := ctx.Req.Method
	route, ok := h.findRoute(method, path)
	if !ok || route.n.handler == nil {
		ctx.Resp.WriteHeader(404)
		_, _ = ctx.Resp.Write([]byte("not found"))
		return
	}
	ctx.Params = route.params
	ctx.MatchedRoute = route.n.path
	route.n.handler(ctx)
}

// /=================================================================================================
func (h *httpServer) Get(path string, handleFunc HandleFunc) {
	h.addRoute(http.MethodGet, path, handleFunc)
}

func (h *httpServer) Post(path string, handleFunc HandleFunc) {
	h.addRoute(http.MethodPost, path, handleFunc)
}
