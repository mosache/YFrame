package rest

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"reflect"
	"testing"
)

func Test_router_AddRoute(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}
	testRoutes := []struct {
		method     string
		path       string
		handleFunc HandleFunc
	}{
		{
			method:     http.MethodGet,
			path:       "/",
			handleFunc: mockHandler,
		},
		{
			method:     http.MethodGet,
			path:       "/user",
			handleFunc: mockHandler,
		},
		{
			method:     http.MethodGet,
			path:       "/user/home",
			handleFunc: mockHandler,
		},
		{
			method:     http.MethodGet,
			path:       "/order/detail",
			handleFunc: mockHandler,
		},

		{
			method:     http.MethodGet,
			path:       "/order/detail/:id",
			handleFunc: mockHandler,
		},

		{
			method:     http.MethodGet,
			path:       "/order/*",
			handleFunc: mockHandler,
		},

		{
			method:     http.MethodGet,
			path:       "/*/detail",
			handleFunc: mockHandler,
		},

		{
			method:     http.MethodGet,
			path:       "/*/detail/*",
			handleFunc: mockHandler,
		},

		{
			method:     http.MethodPost,
			path:       "/create",
			handleFunc: mockHandler,
		},

		{
			method:     http.MethodPost,
			path:       "/login",
			handleFunc: mockHandler,
		},
	}

	r := newRouter()

	for _, route := range testRoutes {
		r.addRoute(route.method, route.path, route.handleFunc)
	}

	wantRouter := &router{trees: map[string]*node{
		http.MethodGet: {
			path:    "/",
			handler: mockHandler,
			children: map[string]*node{
				"user": &node{
					path:    "user",
					handler: mockHandler,
					children: map[string]*node{
						"home": &node{
							path:    "home",
							handler: mockHandler,
						},
					},
				},
				"order": &node{
					path: "order",
					children: map[string]*node{"detail": &node{
						path:    "detail",
						handler: mockHandler,
						parmaChild: &node{
							path:    ":id",
							handler: mockHandler,
						},
					}},
					startChild: &node{
						path:    "*",
						handler: mockHandler,
					},
				},
			},
			startChild: &node{
				path: "*",
				children: map[string]*node{
					"detail": &node{
						path: "detail",
						startChild: &node{
							path:    "*",
							handler: mockHandler,
						},
						handler: mockHandler,
					},
				},
			},
		},
		http.MethodPost: {
			path: "/",
			children: map[string]*node{
				"create": &node{
					path:    "create",
					handler: mockHandler,
				},
				"login": &node{
					path:    "login",
					handler: mockHandler,
				},
			},
		},
	}}

	msg, ok := wantRouter.equal(r)
	assert.True(t, ok, msg)

	r = newRouter()
	assert.Panics(t, func() {
		r.addRoute(http.MethodGet, "", mockHandler)
	}, "rest : path can not be nil")

	assert.Panics(t, func() {
		r.addRoute(http.MethodGet, "a", mockHandler)
	}, "rest : path must start with /")

	assert.Panics(t, func() {
		r.addRoute(http.MethodGet, "a/", mockHandler)
	}, "rest : path cannot be end with /")

	assert.Panics(t, func() {
		r.addRoute(http.MethodGet, "//a//vb", mockHandler)
	}, "rest : path cannot contain //")

	r = newRouter()
	r.addRoute(http.MethodGet, "/", mockHandler)
	assert.Panicsf(t, func() {
		r.addRoute(http.MethodGet, "/", mockHandler)
	}, "rest:路由冲突")
	r.addRoute(http.MethodGet, "/user", mockHandler)
	assert.Panicsf(t, func() {
		r.addRoute(http.MethodGet, "/user", mockHandler)
	}, "rest:路由冲突")
	r.addRoute(http.MethodGet, "/user/a", mockHandler)
	assert.Panicsf(t, func() {
		r.addRoute(http.MethodGet, "/user/a", mockHandler)
	}, "rest:路由冲突")
}

func (r *router) equal(o *router) (string, bool) {
	//if len(r.trees) != len(o.trees) {
	//	return "tree len 不相等", false
	//}
	for k, v := range r.trees {
		if dst, ok := o.trees[k]; !ok {
			return fmt.Sprintf("找不到对应的 http method"), false
		} else {
			if msg, equal := v.equal(dst); !equal {
				return msg, false
			}
		}
	}

	return "", true
}

func (n *node) equal(o *node) (string, bool) {
	if o == nil {
		return "node 不一致", false
	}
	if n.path != o.path {
		return "节点path不一致", false
	}

	if len(n.children) != len(o.children) {
		return "子节点数量不相等", false
	}

	if n.startChild != nil {
		msg, ok := n.startChild.equal(o.startChild)
		if !ok {
			return msg, false
		}
	}

	if n.parmaChild != nil {
		msg, ok := n.parmaChild.equal(o.parmaChild)
		if !ok {
			return msg, false
		}
	}

	//if (n.startChild == nil && o.startChild != nil) || (n.startChild != nil && o.startChild == nil) {
	//	return "wildChild子节点不相等", false
	//}

	nHandler := reflect.ValueOf(n.handler)
	oHandler := reflect.ValueOf(o.handler)

	if nHandler != oHandler {
		return fmt.Sprintf("handler 不相等"), false
	}

	for path, c := range n.children {
		if dst, ok := o.children[path]; !ok {
			return fmt.Sprintf("子节点 %s 不存在", path), false
		} else {
			if msg, equal := c.equal(dst); !equal {
				return msg, false
			}
		}
	}

	return "", true
}

func Test_Route_findRoute(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}
	testRoutes := []struct {
		method string
		path   string
	}{
		{
			method: http.MethodGet,
			path:   "/",
		},
		{
			method: http.MethodGet,
			path:   "/order/detail",
		},
		{
			method: http.MethodGet,
			path:   "/order/detail/:id",
		},

		{
			method: http.MethodGet,
			path:   "/order/*",
		},

		{
			method: http.MethodGet,
			path:   "/*/prod/*",
		},
	}

	r := newRouter()

	for _, route := range testRoutes {
		r.addRoute(route.method, route.path, mockHandler)
	}

	testCase := []struct {
		name string

		method string
		path   string

		wantFound bool
		wantNode  *node
	}{
		{name: "method not found", method: http.MethodOptions, path: "/", wantFound: false},
		{
			name:      "order detail",
			method:    http.MethodGet,
			path:      "/order/detail",
			wantFound: true,
			wantNode: &node{
				path:       "detail",
				parmaChild: &node{path: ":id", handler: mockHandler},
				handler:    mockHandler,
			},
		},
		{
			name:      "order",
			method:    http.MethodGet,
			path:      "/order",
			wantFound: true,
			wantNode: &node{
				path: "order",
				children: map[string]*node{
					"detail": &node{
						path:       "detail",
						parmaChild: &node{path: ":id", handler: mockHandler},
						handler:    mockHandler,
					},
				},
				startChild: &node{
					path:    "*",
					handler: mockHandler,
				},
			},
		},

		{
			name:      "root",
			method:    http.MethodGet,
			path:      "/",
			wantFound: true,
			wantNode: &node{
				startChild: &node{
					path: "*",
					children: map[string]*node{
						"prod": &node{
							path: "prod",
							startChild: &node{
								path:    "*",
								handler: mockHandler,
							},
						},
					},
					startChild: nil,
					handler:    nil,
				},
				path:    "/",
				handler: mockHandler,
				children: map[string]*node{"order": &node{
					path: "order",
					children: map[string]*node{
						"detail": &node{
							path: "detail",
							parmaChild: &node{
								path:    ":id",
								handler: mockHandler,
							},
							handler: mockHandler,
						},
					},
					startChild: &node{
						path:    "*",
						handler: mockHandler,
					},
				}},
			},
		},

		{
			name:      "通配符",
			method:    http.MethodGet,
			path:      "/order/*",
			wantFound: true,
			wantNode: &node{
				path:    "*",
				handler: mockHandler,
			},
		},
		{
			name:      "通配符 * 第一级路径",
			method:    http.MethodGet,
			path:      "/*/prod/*",
			wantFound: true,
			wantNode: &node{
				path:    "*",
				handler: mockHandler,
			},
		},

		{
			name:      "路径参数",
			method:    http.MethodGet,
			path:      "/order/detail/1",
			wantFound: true,
			wantNode: &node{
				path:    ":id",
				handler: mockHandler,
			},
		},
	}

	for _, tc := range testCase {
		t.Run(tc.name, func(t *testing.T) {
			info, found := r.findRoute(tc.method, tc.path)
			assert.Equal(t, tc.wantFound, found)
			if !found {
				return
			}
			msg, b := info.n.equal(tc.wantNode)
			assert.True(t, b, msg)
		})
	}

}
