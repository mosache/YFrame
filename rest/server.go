package rest

import (
	"net/http"
)

type Server interface {
	http.Handler
	Start(addr string) error

	addRoute(method string, path string, handleFunc HandleFunc)
}

type HandleFunc func(ctx *Context)
