package main

import (
	"gitee.com/mosache/YFrame/cache"
	"gitee.com/mosache/YFrame/rest"
)

type SSOMiddleBuilder struct {
	mySession cache.Cache
}

func (m SSOMiddleBuilder) Build() rest.MiddleWare {
	return func(next rest.HandleFunc) rest.HandleFunc {
		return func(ctx *rest.Context) {

			next(ctx)
		}
	}
}
