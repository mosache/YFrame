package main

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/cache/localCache"
	"gitee.com/mosache/YFrame/rest"
	"github.com/google/uuid"
	"html/template"
	"net/http"
	"time"
)

var (
	redirectMap = map[string]string{
		"app1": "http://app1.yd.com:8081/token",
		"app2": "http://app2.yd.com:8082/token",
	}
)

func main() {
	c := localCache.New()
	server := rest.NewHttpServer(rest.ServerWithMiddleware(SSOMiddleBuilder{c}.Build()))
	ts, err := template.ParseGlob("/Users/vurtne/Public/GolangProject/YFrame/sso/sso_server/temp/*.gohtml")
	if err != nil {
		panic(err)
	}

	tokenCache := localCache.New(localCache.WithCheckInterval(time.Minute))

	server.Get("/login", func(ctx *rest.Context) {
		source := ctx.Req.URL.Query().Get("source")
		ctx.Render(ts, "login", map[string]string{"source": source})
	})

	server.Post("/check_login", func(ctx *rest.Context) {
		source := ctx.Req.URL.Query().Get("source")
		cookie, err := ctx.Req.Cookie("sessid")
		if err != nil {
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
			return
		}
		val := cookie.Value
		session, err := c.Get(context.Background(), val)
		if err != nil {
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
			return
		}

		s, ok := session.(Session)
		if !ok {
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
			return
		}

		if s.Uid == 123 {
			/// 已经登录过
			var token string
			token = uuid.New().String()
			ctx.Redirect(fmt.Sprintf("%s?token=%s", redirectMap[source], token))
			tokenCache.Set(context.Background(), token, Session{Uid: 123}, time.Minute)
			return
		}

		http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
	})

	server.Get("/check_login", func(ctx *rest.Context) {
		source := ctx.Req.URL.Query().Get("source")
		cookie, err := ctx.Req.Cookie("sessid")
		if err != nil {
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
			return
		}
		val := cookie.Value
		session, err := c.Get(context.Background(), val)
		if err != nil {
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
			return
		}

		s, ok := session.(Session)
		if !ok {
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
			return
		}

		if s.Uid == 123 {
			/// 已经登录过
			var token string
			token = uuid.New().String()
			ctx.Redirect(fmt.Sprintf("%s?token=%s", redirectMap[source], token))
			tokenCache.Set(context.Background(), token, Session{Uid: 123}, time.Minute)
			return
		}

		http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("/login?source=%s", source), http.StatusTemporaryRedirect)
	})

	server.Post("/token/validate", func(ctx *rest.Context) {
		token := ctx.Query("token")
		_, err := tokenCache.Get(context.Background(), token)
		if err != nil {
			ctx.RespString("token验证失败", http.StatusForbidden)
			return
		}

		ctx.RespString("123", http.StatusOK)

	})

	server.Post("/login", func(ctx *rest.Context) {
		var req struct {
			UserName string `form:"username"`
			Pwd      string `form:"pwd"`
			Source   string `form:"source"`
		}
		if err = ctx.Bind(&req); err != nil {
			ctx.Resp.Write([]byte(err.Error()))
			return
		}

		/// login success
		if req.UserName == "admin" && req.Pwd == "123" {
			ssid := uuid.New().String()
			c.Set(context.Background(), ssid, Session{Uid: 123}, time.Hour)
			http.SetCookie(ctx.Resp, &http.Cookie{
				Name:   "sessid",
				Value:  ssid,
				Domain: "sso.yd.com",
			})

			var token string
			token = uuid.New().String()
			http.Redirect(ctx.Resp, ctx.Req, fmt.Sprintf("%s?token=%s", redirectMap[req.Source], token), http.StatusTemporaryRedirect)
			tokenCache.Set(context.Background(), token, Session{Uid: 123}, time.Minute)
			return
		}
	})

	server.Start(":8080")
}

type Session struct {
	Uid int
}
