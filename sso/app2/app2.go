package main

import (
	"context"
	"fmt"
	"gitee.com/mosache/YFrame/cache/localCache"
	"gitee.com/mosache/YFrame/rest"
	"github.com/google/uuid"
	"io"
	"net/http"
	"time"
)

var (
	tokenValidateURL = "http://sso.yd.com:8080/token/validate"
)

func main() {
	c := localCache.New()
	server := rest.NewHttpServer(rest.ServerWithMiddleware(LoginMiddleBuilder{c, "app2"}.Build()))

	server.Get("/profit", func(ctx *rest.Context) {
		ctx.Resp.Write([]byte("app2,enter"))
	})

	server.Post("/token", func(ctx *rest.Context) {
		token := ctx.Query("token")

		resp, err := http.Post(fmt.Sprintf("%s?token=%s", tokenValidateURL, token), "application/json", nil)
		if err != nil {
			ctx.RespString("服务器内部错误", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			ctx.RespString("服务器内部错误", http.StatusInternalServerError)
			return
		}
		if string(data) == "123" {
			ssid := uuid.New().String()
			c.Set(context.Background(), ssid, Session{
				Uid: 123,
			}, time.Minute*15)
			http.SetCookie(ctx.Resp, &http.Cookie{
				Name:   "ssid",
				Value:  ssid,
				Domain: "app2.yd.com",
			})
			ctx.RespString("登录完成", http.StatusOK)
		} else {
			ctx.RespString("无权限", http.StatusForbidden)
		}
	})

	server.Get("/token", func(ctx *rest.Context) {
		token := ctx.Query("token")

		resp, err := http.Post(fmt.Sprintf("%s?token=%s", tokenValidateURL, token), "application/json", nil)
		if err != nil {
			ctx.RespString("服务器内部错误", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			ctx.RespString("服务器内部错误", http.StatusInternalServerError)
			return
		}
		if string(data) == "123" {
			ssid := uuid.New().String()
			c.Set(context.Background(), ssid, Session{
				Uid: 123,
			}, time.Minute*15)
			http.SetCookie(ctx.Resp, &http.Cookie{
				Name:   "ssid",
				Value:  ssid,
				Domain: "app2.yd.com",
			})
			ctx.RespString("登录完成", http.StatusOK)
		} else {
			ctx.RespString("无权限", http.StatusForbidden)
		}
	})

	server.Start(":8082")
}

type Session struct {
	Uid int
}
