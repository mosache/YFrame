package main

import (
	"fmt"
	"gitee.com/mosache/YFrame/cache"
	"gitee.com/mosache/YFrame/rest"
)

type LoginMiddleBuilder struct {
	mySession cache.Cache
	source    string
}

var (
	ssoURL = "http://sso.yd.com:8080/check_login"
)

func (m LoginMiddleBuilder) Build() rest.MiddleWare {
	return func(next rest.HandleFunc) rest.HandleFunc {
		return func(ctx *rest.Context) {
			if ctx.Req.URL.Path == "/token" || ctx.Req.URL.Path == "/favicon.ico" {
				next(ctx)
				return
			}

			cookie, err := ctx.Req.Cookie("ssid")

			if err != nil {
				ctx.Redirect(fmt.Sprintf("%s?source=%s", ssoURL, m.source))
				return
			}
			ssid := cookie.Value
			_, err = m.mySession.Get(ctx.Req.Context(), ssid)
			if err != nil {
				ctx.Redirect(fmt.Sprintf("%s?source=%s", ssoURL, m.source))
				return
			}
			next(ctx)
		}
	}
}
