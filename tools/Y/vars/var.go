package vars

const (
	CodeConfigDir        = "internal/config" // config类目录
	CodeConfigFileName   = "config.go"       // config类文件名
	ConfigFileName       = "config.yaml"
	ConfigDir            = "appConfig"
	GoCtlCodeEtcDir      = "etc"      // goctl生成的etc目录
	CodeResponseDir      = "response" // response目录
	CodeResponseFileName = "response.go"
	CodeModelsDir        = "models"
	CodeTaskFileName     = "Taskfile.yaml"
	GitignoreFileName    = ".gitignore"
	ApiFileDir           = "api"
	ApiBaseFile          = "base.api"

	CodeErrCodeDir      = "internal/errCode"
	CodeErrCodeFileName = "errCode.go"
)
