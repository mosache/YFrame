package etcgen

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

const (
	etcTemp = `Name: %s
Host: 0.0.0.0
Port: 8080
Log:
  Encoding: plain
Db:
  Main:
    Writer:
      Host: "test"
      Port: 123456
      Username: "root"
      Password: "123456"
      DataBaseName: test
    Reader:
      Host: "test"
      Port: 123456
      Username: "root"
      Password: "123456"
      DataBaseName: test
Redis:
  Host: 127.0.0.1
  Port: 6379
  DB: 0
`
)

func GenEtc(modName string) error {
	if _, err := os.Stat(vars.ConfigDir); err != nil {
		if os.IsNotExist(err) {
			/// create the appConfig dir
			err = os.Mkdir(vars.ConfigDir, os.ModePerm)
			if err != nil {
				return err
			}

			/// create config.yaml
			if err = os.WriteFile(fmt.Sprintf("%s/%s", vars.ConfigDir, vars.ConfigFileName), []byte(fmt.Sprintf(etcTemp, modName)), os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("etc dir is ready exist")
}
