package gitignoregen

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

const gitignoreTpl = `.idea/
*.exe
`

func CreateGitignoreFile() error {
	if _, err := os.Stat(vars.GitignoreFileName); err != nil {
		if os.IsNotExist(err) {
			/// create gitignore
			if err = os.WriteFile(fmt.Sprintf("%s", vars.GitignoreFileName), []byte(gitignoreTpl), os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("Taskfile is ready exist ")
}
