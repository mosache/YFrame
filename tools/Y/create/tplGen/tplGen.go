/*
*
复制.tpl文件到
*/
package tplGen

import (
	"embed"
	"io"
	"io/fs"
	"os"
	"path"
	"strings"
)

//go:embed ".tpls/*"
var tplDir embed.FS

var dst = "."

func TplGen(modName string) error {
	var (
		err   error
		f     *os.File
		bs    []byte
		items []fs.DirEntry
	)
	items, err = tplDir.ReadDir(".")
	if err != nil {
		return err
	}

	for _, e := range items {
		if err = copyDir(e.Name(), e.Name()); err != nil {
			return err
		}
	}

	if f, err = os.OpenFile("./.tpls/api/handler.tpl", os.O_TRUNC|os.O_RDWR, fs.ModePerm); err != nil {
		return err
	}

	if bs, err = io.ReadAll(f); err != nil {
		return err
	}

	content := string(bs)
	newContent := strings.ReplaceAll(content, "{{.ModName}}", modName)
	if _, err = f.WriteString(newContent); err != nil {
		return err
	}

	return nil
}

func copyDir(src string, dst string) error {
	/// 文件夹
	if err := os.Mkdir(dst, os.ModePerm); err != nil {
		return err
	}

	items, err := tplDir.ReadDir(src)
	if err != nil {
		return err
	}

	for _, e := range items {
		/// 文件
		if !e.IsDir() {
			if err = copyFile(path.Join(src, e.Name()), path.Join(dst, e.Name())); err != nil {
				return err
			}

			continue
		}

		///// 文件夹
		//if err = os.Mkdir(path.Join(src, e.Name()), os.ModePerm); err != nil {
		//	return err
		//}

		if err = copyDir(path.Join(src, e.Name()), path.Join(dst, e.Name())); err != nil {
			return err
		}
	}

	return nil
}

func copyFile(src string, dst string) error {
	var (
		srcFile fs.File
		dstFile *os.File
		err     error
	)
	if srcFile, err = tplDir.Open(src); err != nil {
		return err
	}
	defer srcFile.Close()

	if dstFile, err = os.Create(dst); err != nil {
		return err
	}
	defer dstFile.Close()

	if _, err = io.Copy(dstFile, srcFile); err != nil {
		return err
	}

	return nil
}
