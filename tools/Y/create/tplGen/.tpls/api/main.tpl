package main

import (
	"flag"
	"fmt"
    "github.com/zeromicro/go-zero/core/logx"
    "github.com/zeromicro/go-zero/rest/httpx"
	{{.importPackages}}
)

var configFile = flag.String("f", "appConfig/config.yaml", "the config file")

func main() {
	flag.Parse()

	var c config.Config
	conf.MustLoad(*configFile, &c)

	config.C = c

    /// 关闭stat日志
    logx.DisableStat()

    httpx.SetErrorHandler(func(err error) (int, interface{}) {
        fmt.Printf("err : %s", err.Error())
        return 200, map[string]interface{}{
            "state": 2,
            "msg":   err.Error(),
        }
    })

	ctx := svc.NewServiceContext(c)
	server := rest.MustNewServer(c.RestConf,rest.WithCors())
	defer server.Stop()

	handler.RegisterHandlers(server, ctx)

	fmt.Printf("Starting server at %s:%d...\n", c.Host, c.Port)
	server.Start()
}
