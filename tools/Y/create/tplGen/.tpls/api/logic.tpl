package {{.pkgName}}

import (
	{{.imports}}
	"gitee.com/mosache/YFrame/core/common/bizx"
)

type {{.logic}} struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func New{{.logic}}(ctx context.Context, svcCtx *svc.ServiceContext) *{{.logic}} {
	return &{{.logic}}{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *{{.logic}}) {{.function}}({{.request}}) {{.responseType}} {
    resp = bizx.InitPtrStruct(resp)
	// todo: add your logic here and delete this line

	{{.returnString}}
}
