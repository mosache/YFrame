package config

import {{.authImport}}

var (
	C Config
)

type Config struct {
	rest.RestConf
    {{.auth}}
    {{.jwtTrans}}
	Redis redis.Config
	Db    DBConnConfig
}

func (c Config) GetConfigs() []*mysql.Config {
	cs := []*mysql.Config{
		{
			Host:         c.Db.Main.Writer.Host,
			Port:         c.Db.Main.Writer.Port,
			UserName:     c.Db.Main.Writer.Username,
			PassWord:     c.Db.Main.Writer.Password,
			DataBaseName: c.Db.Main.Writer.DataBaseName,
		},
	}

	if c.Db.Main.Reader.Port != 0 {
		cs = append(cs, &mysql.Config{
			Host:         c.Db.Main.Reader.Host,
			Port:         c.Db.Main.Reader.Port,
			UserName:     c.Db.Main.Reader.Username,
			PassWord:     c.Db.Main.Reader.Password,
			DataBaseName: c.Db.Main.Reader.DataBaseName,
		})
	}
	return cs
}

type DBConnConfig struct {
	Main       DBSourceConfig
	YDErpExtend DBSubSourceConfig
	OfferTool   DBSubSourceConfig
}

type DBSourceConfig struct {
	Reader DBConfig
	Writer DBConfig
}

type DBSubSourceConfig struct {
	Name string
}

type DBConfig struct {
	Host         string
	Port         int
	Username     string
	Password     string
	DataBaseName string
}
