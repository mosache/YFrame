package modelsGen

import (
	"errors"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

func GenModelsDir() error {
	if _, err := os.Stat(vars.CodeModelsDir); err != nil {
		if os.IsNotExist(err) {
			/// create dir
			err = os.MkdirAll(vars.CodeModelsDir, os.ModePerm)
			if err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("config dir is ready exist")
}
