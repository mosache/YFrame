package configgen

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

const (
	configTemp = `package config

import (
	"gitee.com/mosache/YFrame/core/stores/mysql"
	"gitee.com/mosache/YFrame/core/stores/redis"
	"github.com/zeromicro/go-zero/rest"
)

var (
	C Config
)

type Config struct {
	rest.RestConf

	Redis redis.Config
	Db    *DBConnConfig
}

func (c Config) GetConfigs() []*mysql.Config {
	cs := []*mysql.Config{
		{
			Host:         c.Db.Main.Writer.Host,
			Port:         c.Db.Main.Writer.Port,
			UserName:     c.Db.Main.Writer.Username,
			PassWord:     c.Db.Main.Writer.Password,
			DataBaseName: c.Db.Main.Writer.DataBaseName,
		},
	}

	if c.Db.Main.Reader.Port != 0 {
		cs = append(cs, &mysql.Config{
			Host:         c.Db.Main.Reader.Host,
			Port:         c.Db.Main.Reader.Port,
			UserName:     c.Db.Main.Reader.Username,
			PassWord:     c.Db.Main.Reader.Password,
			DataBaseName: c.Db.Main.Reader.DataBaseName,
		})
	}
	return cs
}

func (c Config) GetConfigs2() []*mysql.ConnConfig {

	dbConn := reflect.ValueOf(c.Db).Elem()
	dbTypeConn := reflect.TypeOf(c.Db).Elem()
	cs := make([]*mysql.ConnConfig, 0)

	for i := 0; i < dbConn.NumField(); i++ {
		dbConnField := dbConn.Field(i).Elem()

		filedName := dbTypeConn.Field(i).Name

		writeConn := dbConnField.FieldByName("Writer")
		readConn := dbConnField.FieldByName("Reader")

		var (
			write *mysql.Config
			read  *mysql.Config
		)
		if !writeConn.IsNil() {
			write = &mysql.Config{
				Host:         writeConn.Elem().FieldByName("Host").String(),
				Port:         int(writeConn.Elem().FieldByName("Port").Int()),
				UserName:     writeConn.Elem().FieldByName("Username").String(),
				PassWord:     writeConn.Elem().FieldByName("Password").String(),
				DataBaseName: writeConn.Elem().FieldByName("DataBaseName").String(),
				UseMetrics:   false,
				ConnName:     filedName,
			}
		}

		if !readConn.IsNil() {
			read = &mysql.Config{
				Host:         readConn.Elem().FieldByName("Host").String(),
				Port:         int(readConn.Elem().FieldByName("Port").Int()),
				UserName:     readConn.Elem().FieldByName("Username").String(),
				PassWord:     readConn.Elem().FieldByName("Password").String(),
				DataBaseName: readConn.Elem().FieldByName("DataBaseName").String(),
				UseMetrics:   false,
				ConnName:     filedName,
			}
		}

		cs = append(cs, &mysql.ConnConfig{
			ConnName: filedName,
			Write:    write,
			Read:     read,
		})
	}
	return cs
}

type DBConnConfig struct {
	Main *DBSourceConfig
}

type DBSourceConfig struct {
	Reader *DBConfig
	Writer *DBConfig
}

type DBSubSourceConfig struct {
	Name string
}

type DBConfig struct {
	Host         string
	Port         int
	Username     string
	Password     string
	DataBaseName string
}
`
)

func GenConfig() error {
	if _, err := os.Stat(vars.CodeConfigDir); err != nil {
		if os.IsNotExist(err) {
			/// create the code config dir
			err = os.MkdirAll(vars.CodeConfigDir, os.ModePerm)
			if err != nil {
				return err
			}

			/// create config.go
			if err = os.WriteFile(fmt.Sprintf("%s/%s", vars.CodeConfigDir, vars.CodeConfigFileName), []byte(configTemp), os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("config dir is ready exist")
}
