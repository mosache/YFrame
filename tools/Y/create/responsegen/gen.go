package responsegen

import (
	_ "embed"
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

//go:embed "response.tpl"
var responseTpl []byte

func GenResponse() error {
	if _, err := os.Stat(vars.CodeResponseDir); err != nil {
		if os.IsNotExist(err) {
			/// create the code config dir
			err = os.MkdirAll(vars.CodeResponseDir, os.ModePerm)
			if err != nil {
				return err
			}

			/// create config.go
			if err = os.WriteFile(fmt.Sprintf("%s/%s", vars.CodeResponseDir, vars.CodeResponseFileName), responseTpl, os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("response dir is ready exist")
}
