package response

import (
	"github.com/zeromicro/go-zero/rest/httpx"
	"net/http"
)

type Body struct {
	State   int         `json:"state" example:"1"` // 1.成功 2.失败
	Message string      `json:"msg"`               // 错误信息
	Data    interface{} `json:"data"`              // 返回的数据
}

func Response(w http.ResponseWriter, resp interface{}, err error) {
	var body Body
	if err != nil {
		body.State = 2
		body.Message = err.Error()
	} else {
		body.State = 1
		body.Data = resp
	}
	httpx.OkJson(w, body)
}

func SimpleResponse(w http.ResponseWriter, resp interface{}, err error) {
	if err != nil {
		httpx.WriteJson(w, 500, map[string]interface{}{"code": "FAIL", "message": "失败"})
	} else {
		w.WriteHeader(200)
		w.Write([]byte(resp.(string)))
	}
}
