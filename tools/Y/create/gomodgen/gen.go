package gomodgen

import (
	"os/exec"
)

func CreateGoModFile(modName string) error {
	cmd := exec.Command("go", "mod", "init", modName)

	err := cmd.Run()

	return err
}
