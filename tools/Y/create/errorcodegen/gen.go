package errorcodegen

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

const (
	errCodeTemp = `package errCode

//go:generate stringer -type ErrCode -linecomment -output code_string.go
type ErrCode int

func (e ErrCode) Error() string {
	return e.String()
}

func (e ErrCode) custom() {
}

type CustomErr interface {
	custom()
}

type ValidateError struct {
	S string
}

func (v ValidateError) Error() string {
	return v.S
}
func (v ValidateError) custom() {}

const (
)

`
)

func GenErrorCode() error {
	if _, err := os.Stat(vars.CodeErrCodeDir); err != nil {
		if os.IsNotExist(err) {
			/// create the code config dir
			err = os.MkdirAll(vars.CodeErrCodeDir, os.ModePerm)
			if err != nil {
				return err
			}

			/// create config.go
			if err = os.WriteFile(fmt.Sprintf("%s/%s", vars.CodeErrCodeDir, vars.CodeErrCodeFileName), []byte(errCodeTemp), os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("errCode dir is ready exist")
}
