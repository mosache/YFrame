package apigen

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

const (
	apiBaseTemp = `syntax = "v1"

`
)

func GenApiDir() error {
	if _, err := os.Stat(vars.ApiFileDir); err != nil {
		if os.IsNotExist(err) {
			/// create the code config dir
			err = os.MkdirAll(vars.ApiFileDir, os.ModePerm)
			if err != nil {
				return err
			}

			/// create base.api
			if err = os.WriteFile(fmt.Sprintf("%s/%s", vars.ApiFileDir, vars.ApiBaseFile), []byte(apiBaseTemp), os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("api dir is ready exist")
}
