package taskGen

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/vars"
	"os"
)

const taskFileTpl = `version: '3'

tasks:
  build:
    cmds:
      - Y api gen && Y api doc && Y model`

func GenTaskFile() error {
	if _, err := os.Stat(vars.CodeTaskFileName); err != nil {
		if os.IsNotExist(err) {
			/// create Taskfile.go
			if err = os.WriteFile(fmt.Sprintf("%s", vars.CodeTaskFileName), []byte(taskFileTpl), os.ModePerm); err != nil {
				return err
			}

			return nil

		} else {
			return err
		}
	}

	return errors.New("Taskfile is ready exist ")
}
