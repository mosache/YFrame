package cobraErr

import (
	"fmt"
	"github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"os"
)

func Panic(cmd *cobra.Command, message string) {
	cmd.Println(message)
	os.Exit(1)
}

func Err(cmd *cobra.Command, message string) {
	cmd.Println(aurora.Red(fmt.Sprintf("[err] %s", message)))
	os.Exit(1)
}

func ErrNoPanic(cmd *cobra.Command, message string) {
	cmd.Println(aurora.Red(fmt.Sprintf("[err] %s", message)))
}
