package gomodx

import (
	"github.com/zeromicro/go-zero/core/filex"
	"os"
	"strings"
)

// GoModName 获取mod名称
func GoModName(filePath string) string {

	if len(filePath) == 0 {
		filePath = "./go.mod"
	}

	firstLine, _ := filex.FirstLine(filePath)

	sps := strings.Split(strings.TrimSpace(firstLine), " ")

	if len(sps) != 2 {
		panic("go.mod file err")
	}

	return sps[1]
}

// 是否在项目根目录（根据是否有go.mod）
func InProjectRoot() bool {
	goModFilePath := "./go.mod"

	if _, err := os.Stat(goModFilePath); err != nil {
		return false
	}

	return true
}
