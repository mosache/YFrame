package utils

import (
	"fmt"
	"os"
	"runtime"
	"strings"
)

var (
	goDataType = []string{"int", "int8", "int16", "int32", "int64",
		"uint", "uint8", "uint16", "uint32", "uint64",
		"float", "float32", "float64",
		"string", "interface{}",
	}
)

/*
判断是go的内置数据类型
*/
func IsGoDataType(dataType string) bool {
	for _, t := range goDataType {
		if dataType == t {
			return true
		}
	}
	return false

}

var osType = runtime.GOOS

func isWindow() bool {
	return strings.Contains(osType, "windows")
}

/*
获取当前目录名称
*/
func GetPwd() string {
	pwd, err := os.Getwd()

	if err != nil {
		fmt.Println("[get pwd err] :", err.Error())
		return ""
	}

	var pathFlag = "/"
	if isWindow() {
		pathFlag = "\\"
	}

	idx := strings.LastIndex(pwd, pathFlag)

	if idx == -1 {
		return pwd
	}

	return pwd[idx+1:]
}
