package main

import (
	"gitee.com/mosache/YFrame/tools/Y/api"
	"gitee.com/mosache/YFrame/tools/Y/create"
	"gitee.com/mosache/YFrame/tools/Y/dep"
	"gitee.com/mosache/YFrame/tools/Y/docker"
	"gitee.com/mosache/YFrame/tools/Y/initization"
	"gitee.com/mosache/YFrame/tools/Y/model"
	"gitee.com/mosache/YFrame/tools/Y/version"
	"github.com/spf13/cobra"
	"os"
)

func main() {
	rootCmd := cobra.Command{Use: "Y"}

	// create
	rootCmd.AddCommand(create.Cmd)

	// init
	rootCmd.AddCommand(initization.Cmd)

	// api
	rootCmd.AddCommand(api.Cmd)

	//version
	rootCmd.AddCommand(version.Cmd)

	//docker
	rootCmd.AddCommand(docker.Cmd)

	// model
	rootCmd.AddCommand(model.Cmd)

	// dep
	rootCmd.AddCommand(dep.Cmd)

	if _, err := rootCmd.ExecuteC(); err != nil {
		os.Exit(1)
	}
}
