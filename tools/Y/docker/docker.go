package docker

import (
	"bytes"
	"errors"
	"gitee.com/mosache/YFrame/tools/Y/utils/cobraErr"
	"github.com/spf13/cobra"
	"os/exec"
)

var Cmd = &cobra.Command{Use: "docker",
	Long: `generate Dockerfile`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := dockerFileGen(); err != nil {
			cobraErr.Panic(cmd, err.Error())
		}
	},
}

func dockerFileGen() error {
	var (
		in     bytes.Buffer
		out    bytes.Buffer
		errOut bytes.Buffer
		err    error
	)

	c := exec.Command("goctl", "docker",
		"--home", "./.tpls")

	c.Stdin = &in
	c.Stdout = &out
	c.Stderr = &errOut

	if err = c.Run(); err != nil {
		if errOut.Len() > 0 {
			return errors.New(errOut.String())
		}
		return err
	}

	if errOut.Len() > 0 {
		return errors.New(errOut.String())
	}

	return nil
}
