package api

import "github.com/spf13/cobra"

var Cmd = &cobra.Command{Use: "api",
	Long: `api`,
}

func init() {
	Cmd.AddCommand(apiCreateCmd)

	Cmd.AddCommand(apiGenCmd)

	Cmd.AddCommand(apiDocCmd)
}
