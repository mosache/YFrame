package api

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/utils/cobraErr"
	"github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"os/exec"
)

var apiGenCmd = &cobra.Command{Use: "gen",
	Short: "gen from api file",
	Long:  `gen from api file`,
	Args:  cobra.ExactArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := apiFileGen(cmd); err != nil {
			cobraErr.Err(cmd, err.Error())
		}
		return nil
	},
}

func apiFileGen(cmd *cobra.Command) error {
	var (
		in     bytes.Buffer
		out    bytes.Buffer
		errOut bytes.Buffer
		err    error
	)

	c := exec.Command("goctl", "api",
		"go", "--home", "./.tpls", "--api", "./api/base.api", "--dir", ".")

	c.Stdin = &in
	c.Stdout = &out
	c.Stderr = &errOut

	if err = c.Run(); err != nil {
		if errOut.Len() > 0 {
			return errors.New(errOut.String())
		}
		return err
	}

	if errOut.Len() > 0 {
		return errors.New(errOut.String())
	}

	fmt.Println(aurora.Green("DONE!"))
	return nil
}
