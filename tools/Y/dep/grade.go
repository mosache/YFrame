package dep

import (
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/utils/cobraErr"
	"github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"os"
	"os/exec"
)

var depUpGradeCmd = &cobra.Command{Use: "upgrade",
	Short: "upgrade dep",
	Long:  `upgrade dep Y dep`,
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		if err := depUpGrade(); err != nil {
			cobraErr.Err(cmd, err.Error())
		}
	},
}

func depUpGrade() error {
	os.Setenv("GOPRIVATE", "gitee.com/mosache")

	os.Setenv("GOPROXY", "https://goproxy.cn,direct")

	/// YFrame
	if err := exec.Command("go", "install", "gitee.com/mosache/YFrame/tools/Y@master").Run(); err != nil {
		return err
	}

	fmt.Println(aurora.Green(fmt.Sprintf("upgrade YFrame success...")))

	/// go-ctl
	if err := exec.Command("go", "install", "github.com/zeromicro/go-zero/tools/goctl@latest").Run(); err != nil {
		return err
	}

	fmt.Println(aurora.Green(fmt.Sprintf("upgrade go-ctl success...")))

	/// go-task
	if err := exec.Command("go", "install", "github.com/go-task/task/v3/cmd/task@latest").Run(); err != nil {
		return err
	}

	fmt.Println(aurora.Green(fmt.Sprintf("upgrade go-task success...")))

	return nil
}
