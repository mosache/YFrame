package dep

import "github.com/spf13/cobra"

var Cmd = &cobra.Command{Use: "dep",
	Long: `dep`,
}

func init() {
	Cmd.AddCommand(depUpGradeCmd)
}
