package version

import (
	"fmt"
	"github.com/spf13/cobra"
)

const (
	version = "v0.1.51"
)

var Cmd = &cobra.Command{
	Use:   "version",
	Short: "show YFrame version info",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Println(fmt.Sprintf("version %s", version))
	},
}
