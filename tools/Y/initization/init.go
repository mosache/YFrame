package initization

import (
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/create/apigen"
	"gitee.com/mosache/YFrame/tools/Y/create/configgen"
	"gitee.com/mosache/YFrame/tools/Y/create/etcgen"
	"gitee.com/mosache/YFrame/tools/Y/create/gitignoregen"
	"gitee.com/mosache/YFrame/tools/Y/create/gomodgen"
	"gitee.com/mosache/YFrame/tools/Y/create/modelsGen"
	"gitee.com/mosache/YFrame/tools/Y/create/responsegen"
	"gitee.com/mosache/YFrame/tools/Y/create/taskGen"
	"gitee.com/mosache/YFrame/tools/Y/create/tplGen"
	"gitee.com/mosache/YFrame/tools/Y/utils/cobraErr"
	"github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"os"
	"os/exec"
)

var Cmd = &cobra.Command{Use: "init",
	Long: `init is for init project in exist dir.
modName will use in go.mod as module name`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		modName := args[0]
		if err := initProject(modName); err != nil {
			cobraErr.Err(cmd, err.Error())
		}
	},
}

func initProject(modName string) error {
	var err error
	/// go.mod
	if err = gomodgen.CreateGoModFile(modName); err != nil {
		return fmt.Errorf("[go.mod create err] : %s", err.Error())
	}

	/// .gitignore
	if err = gitignoregen.CreateGitignoreFile(); err != nil {
		return fmt.Errorf("[gitignore create err] : %s", err.Error())
	}

	/// config
	if err = configgen.GenConfig(); err != nil {
		return fmt.Errorf("[config config err] : %s", err.Error())
	}

	/// etc
	if err = etcgen.GenEtc(modName); err != nil {
		return fmt.Errorf("[create etc err] : %s", err.Error())
	}

	/// api
	if err = apigen.GenApiDir(); err != nil {
		return fmt.Errorf("[create api dir err] : %s", err.Error())
	}

	/// copy .tpl
	if err = tplGen.TplGen(modName); err != nil {
		return fmt.Errorf("[copy tpl dir err] : %s", err.Error())
	}

	/// response
	if err = responsegen.GenResponse(); err != nil {
		return err
	}

	/// models dir
	if err = modelsGen.GenModelsDir(); err != nil {
		return err
	}

	/// Taskfile.yaml
	if err = taskGen.GenTaskFile(); err != nil {
		return err
	}

	/// fetch deps
	if err = fetchDeps(); err != nil {
		return fmt.Errorf("[fetch deps err] : %s", err.Error())
	}

	fmt.Println(aurora.Green(fmt.Sprintf("project [%s] init success...", modName)))
	return nil
}

func fetchDeps() error {
	os.Setenv("GOPRIVATE", "gitee.com/mosache")

	os.Setenv("GOPROXY", "https://goproxy.cn,direct")

	/// YFrame
	if err := exec.Command("go", "get", "-u", "gitee.com/mosache/YFrame").Run(); err != nil {
		return err
	}

	/// go-zero
	if err := exec.Command("go", "get", "-u", "github.com/zeromicro/go-zero").Run(); err != nil {
		return err
	}

	/// go-ctrl
	if err := exec.Command("go", "install", "-u", "github.com/zeromicro/go-zero/tools/goctl@latest").Run(); err != nil {
		return err
	}

	/// go-ctl
	if err := exec.Command("go", "install", "github.com/zeromicro/go-zero/tools/goctl@latest").Run(); err != nil {
		return err
	}
	/// go-task
	if err := exec.Command("go", "install", "github.com/go-task/task/v3/cmd/task@latest").Run(); err != nil {
		return err
	}
	if err := exec.Command("go", "mod", "tidy").Run(); err != nil {
		return err
	}

	return nil
}
