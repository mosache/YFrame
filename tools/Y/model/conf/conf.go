package conf

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Configuration struct {
	UserName string `yaml:"username"`
	Password string `yaml:"password"`
	Host     string `yaml:"host"`
	Port     int64  `yaml:"port"`
	DataBase string `yaml:"database"`
	ModName  string `yaml:"modname"`
}

var (
	Config *Configuration
)

func InitConf(confFilePath string) error {

	if len(confFilePath) == 0 {
		confFilePath = "./gomg.yaml"
	}

	_, err := os.Stat(confFilePath)

	/// 配置文件不存在或读取出错
	if err != nil {
		return err
	}

	bytes, err := os.ReadFile(confFilePath)

	if err != nil {
		return err
	}

	err = yaml.UnmarshalStrict(bytes, &Config)

	if err != nil {
		return err
	}

	return nil
}
