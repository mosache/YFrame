package gen

type ColumnInfoModel struct {
	TableName     string `gorm:"column:table_name"`
	ColumnName    string `gorm:"column:column_name"`
	ColumnType    string `gorm:"column:column_type"`
	DataType      string `gorm:"column:data_type"`
	IsPrimaryKey  int64  `gorm:"column:is_primary_key"` // 1 是主键 2 不是
	ColumnComment string `gorm:"column:column_comment"`
	Position      int64  `gorm:"column:position"`
	TableComment  string `gorm:"column:table_comment"`
}
