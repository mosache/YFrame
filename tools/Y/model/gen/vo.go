package gen

import (
	"fmt"
	"strings"
)

type tableNameAndCommentFilterMapKey struct {
	TableName    string
	TableComment string
}

type tableInfo struct {
	TableName       string
	TableComment    string
	Package         string
	TableNamePrefix string
	ModName         string
	IsNew           bool
	Columns         []tableColumnInfo
}

type tableColumnInfo struct {
	ColumnName    string
	DataType      string // go 中的type
	ColumType     string
	ColumnComment string
	IsPrimaryKey  bool
	Tag           string
}

func _getGoDataType(sqlDataType string) string {
	switch sqlDataType {
	case "bigint", "int", "tinyint":
		return "int64"
	case "decimal":
		return "float64"
	case "varchar", "text", "time", "mediumtext":
		return "string"
	default:
		return ""
	}
}

func _getFieldComment(comment string) string {
	if len(comment) == 0 {
		return ""
	}
	return fmt.Sprintf("// %s", strings.ReplaceAll(strings.ReplaceAll(
		strings.ReplaceAll(
			strings.ReplaceAll(
				comment, "\r\n", " "), "\n", " "), "\r\n\r", " "), "\r", ""))
}

func _getIsPrimaryKey(sqlIsPrimaryKey int64) bool {
	if sqlIsPrimaryKey == 1 {
		return true
	}
	return false
}

func _getModelFieldTag(columnName string, isPrimaryKey bool) string {
	priStr := ""
	if isPrimaryKey {
		priStr = ";primary_key"
	}
	return fmt.Sprintf("`gorm:\"column:%s%s\"`", columnName, priStr)
}
