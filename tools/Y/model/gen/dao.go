package gen

import (
	sql2 "database/sql"
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/model/db"
)

func getColumnInfo(database string, tables []string, tableRegex []string) ([]*ColumnInfoModel, error) {
	r := make([]*ColumnInfoModel, 0)

	sql := `
		SELECT 
			c.TABLE_NAME AS table_name,
			c.COLUMN_NAME AS column_name,
			c.DATA_TYPE AS data_type,
			c.COLUMN_TYPE AS column_type,
			IF(c.COLUMN_KEY = 'PRI',1,2) AS is_primary_key,
			c.COLUMN_COMMENT AS column_comment,
			c.ORDINAL_POSITION AS position,
			t.TABLE_COMMENT AS table_comment
		FROM
		information_schema.COLUMNS AS c
		LEFT JOIN information_schema.TABLES AS t ON t.TABLE_NAME = c.TABLE_NAME AND t.TABLE_SCHEMA = @database 
		WHERE
		c.TABLE_SCHEMA = @database 
	`

	if len(tables) > 0 {
		sql += "AND c.TABLE_NAME IN @tables "
	}

	if len(tableRegex) > 0 {

		likeFilter := " AND "
		for idx, e := range tableRegex {
			if idx != 0 {
				likeFilter += " OR "
			}
			likeFilter += fmt.Sprintf("(c.TABLE_NAME LIKE '%s')", e)
		}

		sql += likeFilter
		sql += " "
	}

	sql += "ORDER BY c.TABLE_NAME,c.ORDINAL_POSITION"

	if err := db.DB.Raw(sql, sql2.Named("database", database),
		sql2.Named("tables", tables)).Scan(&r).Error; err != nil {
		return nil, err
	}

	return r, nil
}
