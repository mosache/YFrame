package gen

var modelTpl = `package {{.Package}}
{{if .TableNamePrefix}}
import (
	{{- if .IsNew }}
	"{{.ModName}}/internal/config"
	{{ else }}
	"{{.ModName}}/config"
	{{ end -}}
	"fmt"
)
{{end}}
type {{GetGoStylePropertyName .TableName}}Model struct {
	{{- range .Columns}}
	{{GetGoStylePropertyName .ColumnName}} {{.DataType}} {{.Tag}} {{FormatComment .ColumnComment}} 
	{{- end}}
}

func ({{GetGoStylePropertyName .TableName}}Model) TableName() string {
{{- if .TableNamePrefix }}
	return fmt.Sprintf("%s.{{.TableName}}",config.C.{{.TableNamePrefix}}.Name)
{{ else }}
	return "{{.TableName}}"
{{end -}}
}
`
