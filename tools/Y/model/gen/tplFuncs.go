package gen

import "strings"

func GetGoStylePropertyName(columnName string) string {
	return ToCamelCase(columnName)
}

func GetGoStyleTableDaoName(columnName string) string {
	tableDirName := ToCamelCase(columnName)
	runes := []rune(tableDirName)
	strs := make([]string, len(runes))
	for idx, e := range runes {
		if idx == 0 {
			strs = append(strs, ToLower(string(e)))
		} else {
			strs = append(strs, string(e))
		}

	}
	return strings.Join(strs, "")
}

func GetFieldCommentStr(comment string) string {
	return strings.ReplaceAll(comment, "//", "")
}

func FormatComment(comment string) string {
	return strings.ReplaceAll(comment, "\r\n", "")
}
