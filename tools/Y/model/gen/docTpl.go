package gen

var DocTpl = `
{{ range .tables}}
## {{.TableComment}}  {{.TableName}}

| 字段   | 类型   | 含义   | 说明   | 
|:----|:----|:----|:----|
{{range .Columns -}}
  |{{.ColumnName}} | {{.ColumType}}|{{GetFieldCommentStr .ColumnComment}}||
{{end -}}

{{end}}

`
