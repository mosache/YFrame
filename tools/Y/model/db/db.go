package db

import (
	"fmt"
	"gitee.com/mosache/YFrame/tools/Y/model/conf"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	DB *gorm.DB
)

func InitDB(conf *conf.Configuration) error {
	var (
		dsn string
		err error
	)

	dsn = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8", conf.UserName,
		conf.Password, conf.Host, conf.Port, conf.DataBase)

	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		return err
	}

	return nil

}
