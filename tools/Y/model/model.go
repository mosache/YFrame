package model

import (
	"gitee.com/mosache/YFrame/orm/until/strx"
	"gitee.com/mosache/YFrame/tools/Y/model/conf"
	"gitee.com/mosache/YFrame/tools/Y/model/db"
	"gitee.com/mosache/YFrame/tools/Y/model/gen"
	"gitee.com/mosache/YFrame/tools/Y/utils"
	"gitee.com/mosache/YFrame/tools/Y/utils/cobraErr"
	"github.com/spf13/cobra"
)

var (
	databaseName   string   // 数据库名
	tables         []string // 表名
	tableRegex     []string // 表名正则表达式
	configFilePath string   // 配置文件地址
	targetPath     string   // 生成文件路径
	packageName    string   // 包名
	tablePrefix    string   // 表名前缀
	isDoc          bool     // 是否是doc
	isOld          bool     // 是否是老结构
)

var Cmd = &cobra.Command{Use: "model",
	Long: `generate model file`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := modelGen(); err != nil {
			cobraErr.Err(cmd, err.Error())
		}
	},
}

func init() {
	Cmd.Flags().StringVarP(&tablePrefix, "tablePrefix", "f", "", "table prefix for generate")
	Cmd.Flags().StringVarP(&databaseName, "database", "d", "", "database name for generate")
	Cmd.Flags().StringArrayVarP(&tables, "table", "t", []string{}, "tables to scan for generate")
	Cmd.Flags().StringArrayVarP(&tableRegex, "tableregex", "r", []string{}, "tables regex")
	Cmd.Flags().StringVarP(&configFilePath, "conf", "c", "./appConfig/gomg.yaml", "conf file path")
	Cmd.Flags().StringVarP(&targetPath, "target", "", "", "generate file target path")
	Cmd.Flags().StringVarP(&packageName, "package", "p", utils.GetPwd(), "package name")
	Cmd.Flags().BoolVarP(&isDoc, "isDoc", "", false, "is doc ")
	Cmd.Flags().BoolVarP(&isOld, "isOld", "o", false, "is old system ")
}

func modelGen() error {
	if err := conf.InitConf(configFilePath); err != nil {
		return err
	}

	if err := db.InitDB(conf.Config); err != nil {
		return err
	}

	opts := make([]gen.Option, 0)

	/**
	指定了database参数
	一定不是项目的主数据库
	需要在table() 方法里指定数据库
	*/
	if len(databaseName) > 0 {
		opts = append(opts, gen.WithDataBaseName(databaseName))
		if len(tablePrefix) > 0 {
			opts = append(opts, gen.WithTablePrefix(tablePrefix))
		} else {
			opts = append(opts, gen.WithTablePrefix(strx.ToCapitalize(utils.ToCamelCase(databaseName))))
		}

	}

	if len(tables) > 0 {
		opts = append(opts, gen.WithTables(tables))
	}

	if len(tableRegex) > 0 {
		opts = append(opts, gen.WithTableRegex(tableRegex))
	}

	g, err := gen.NewGenerator(opts...)
	if err != nil {
		return err
	}
	if err = g.Generate(targetPath, packageName, isDoc, !isOld); err != nil {
		return err
	}

	return nil
}
