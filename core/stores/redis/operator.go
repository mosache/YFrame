package redis

import (
	"encoding/json"
	"fmt"
	"gitee.com/mosache/YFrame/core/common/mapx"
	"gitee.com/mosache/YFrame/util/ctx"
	r2 "github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
	"time"
)

func Set(key, value interface{}, expireTime int64) (err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		if r, err := r2.String(conn.Do("SET", key, value, "EX", expireTime)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r != "OK" {
				return errors.Wrap(err, fmt.Sprintf("set key : [%s] result is not OK ", key))
			}
		}
	}

	return nil
}

func DoCmd(cmd string, params ...any) (reply any, err error) {
	var (
		conn r2.Conn
		r    any
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return nil, errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		if r, err = conn.Do(cmd, params...); err != nil {
			return nil, errors.Wrap(err, "command err")
		} else {
			return r, nil
		}
	}
}

func SetWithOutExpire(key, value interface{}) (err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		if r, err := r2.String(conn.Do("SET", key, value)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r != "OK" {
				return errors.Wrap(err, fmt.Sprintf("set key : [%s] result is not OK ", key))
			}
		}
	}

	return nil
}

func IsExist(key string) (isExist bool, err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return false, errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()

		if isExist, err := r2.Int64(conn.Do("EXISTS", key)); err != nil {
			return false, errors.Wrap(err, fmt.Sprintf("do [EXISTS %s] command error", key))
		} else {
			return isExist == 1, nil
		}
	}

}

func GetString(key string) (value string, err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return "", errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		r, err := r2.String(conn.Do("GET", key))
		if err != nil {
			return "", err
		}

		return r, nil
	}
}

func SetInt64s(key string, value []int64, expireTime int64) (err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()

		args := []interface{}{key, value}

		if expireTime != -1 {
			args = append(args, "EX", expireTime)
		}

		if r, err := r2.String(conn.Do("SET", args)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r != "OK" {
				return errors.Wrap(err, fmt.Sprintf("set key : [%s] result is not OK ", key))
			}
		}
	}
	return nil
}

func GetInt64s(key string) (value []int64, err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return nil, errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		r, err := r2.Int64s(conn.Do("GET", key))
		if err != nil {
			return nil, err
		}

		return r, nil
	}
}

func SetJSONObjectWithNoExpireTime(key string, value interface{}) (err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()

		jsonData, err := json.Marshal(value)

		if err != nil {
			return err
		}

		if r, err := conn.Do("SET", key, string(jsonData)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r != "OK" {
				return errors.Wrap(err, fmt.Sprintf("set key : [%s] result is not OK ", key))
			}
		}
	}
	return nil
}

func GetJSONObject(key string, dest interface{}) error {
	var (
		conn r2.Conn
		err  error
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		values, err := r2.Bytes(conn.Do("GET", key))
		if err != nil {
			return err
		}

		err = json.Unmarshal(values, dest)
		if err != nil {
			return err
		}
		return nil
	}
}

func GeoAdd(key, longitude, latitude, member string) (err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()
		if r, err := r2.Int(conn.Do("GEOADD", key, longitude, latitude, member)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r < 1 {
				return errors.Wrap(err, fmt.Sprintf("GEOADD key : [%s] result is not OK ", key))
			}
		}
	}

	return
}

func GeoRemove(key, member string) (err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		if r, err := r2.Int64(conn.Do("ZREM", key, member)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r < 1 {
				return errors.Wrap(err, fmt.Sprintf("georemove key : [%s] result is not OK ", key))
			}
		}
	}

	return nil
}

type GeoRadiusResult struct {
	Member     string
	Distance   string
	Coordinate GeoRadiusCoordinate
}
type GeoRadiusCoordinate struct {
	Longitude string
	Latitude  string
}

func GeoRadius(key, longitude, latitude, distance, unit string) (result []GeoRadiusResult, err error) {
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return nil, errors.Wrap(err, "get redis conn error!")
	} else {
		defer conn.Close()
		if r, err := r2.Values(conn.Do("GEORADIUS", key, longitude, latitude, distance, unit, "WITHCOORD", "WITHDIST", "ASC")); err != nil {
			return nil, errors.Wrap(err, "command err")
		} else {
			// 返回的数组
			result = make([]GeoRadiusResult, 0)

			// 数组数据变成struct数组
			for _, e := range r {
				// e 是 []interface
				geoR := GeoRadiusResult{}
				src := e.([]interface{})
				geoR.Member, _ = r2.String(src[0], nil)
				geoR.Distance, _ = r2.String(src[1], nil)
				geoR.Coordinate.Longitude, _ = r2.String(src[2].([]interface{})[0], nil)
				geoR.Coordinate.Latitude, _ = r2.String(src[2].([]interface{})[1], nil)
				result = append(result, geoR)
			}

			if err != nil {
				return nil, err
			}
			return result, nil
		}
	}
}

func GeoRadiusWithLua(key, longitude, latitude, distance, unit string, page, limit int64) (result []int64, err error) {
	if len(longitude) == 0 || len(latitude) == 0 {
		return []int64{}, nil
	}

	start := (page - 1) * limit
	end := start + (limit - 1)
	const geoRadiusLua = `
		redis.call('GEORADIUS',KEYS[1],ARGV[1],ARGV[2],ARGV[3],ARGV[4],'DESC','STOREDIST','geo_re_temp')
		return redis.call('ZRANGE','geo_re_temp',ARGV[5],ARGV[6])
	`
	//'RedisGeoKey'
	//'14.361389','39.115556'
	var (
		conn r2.Conn
	)
	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return nil, errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()
		if r, err := r2.Int64s(r2.NewScript(1, geoRadiusLua).Do(conn, key, longitude, latitude, distance, unit, start, end)); err != nil {
			return nil, errors.Wrap(err, "command err")
		} else {
			return r, nil
		}
		//if r, err := r2.Values(conn.Do("GEORADIUS", key, longitude, latitude, distance, unit)); err != nil {
		//	return nil, errors.Wrap(err, "command err")
		//} else {
		//	// 返回的数组
		//	result = make([]GeoRadiusResult, 0)
		//
		//	// 数组数据变成struct数组
		//	for _, e := range r {
		//		// e 是 []interface
		//		geoR := GeoRadiusResult{}
		//		src := e.([]interface{})
		//		geoR.Member, _ = r2.String(src[0], nil)
		//		geoR.Distance, _ = r2.String(src[1], nil)
		//		geoR.Coordinate.Longitude, _ = r2.String(src[2].([]interface{})[0], nil)
		//		geoR.Coordinate.Latitude, _ = r2.String(src[2].([]interface{})[1], nil)
		//		result = append(result, geoR)
		//	}
		//
		//	if err != nil {
		//		return nil, err
		//	}
		//	return result, nil
		//}
	}
}

func GetCounter(key string) (result int64, err error) {
	var (
		conn r2.Conn
	)

	lua := `
		local exists = redis.call('exists',KEYS[1]);
		if (exists == 1) then
			return redis.call('GET',KEYS[1]);
		end
		return -1;
	`

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return 0, errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		if r, err := r2.Int64(r2.NewScript(1, lua).Do(conn, key)); err != nil {
			return 0, errors.Wrap(err, "command err")
		} else {
			if r == -1 {
				/// 不存在
				return -1, nil
				//return errors.Wrap(err, fmt.Sprintf("incr key : [%s] result is not OK ", key))
			} else {
				return r, nil
			}
		}
	}
}

func IncreaseCounter(key string) (result int64, err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return 0, errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		if r, err := r2.Int64(conn.Do("INCR", key)); err != nil {
			return 0, errors.Wrap(err, "command err")
		} else {
			return r, nil
		}
	}
}

func DecreaseCounter(key string) (result int64, err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return 0, errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		if r, err := r2.Int64(conn.Do("DECR", key)); err != nil {
			return 0, errors.Wrap(err, "command err")
		} else {
			return r, nil
		}
	}
}

func XAdd(key string, message interface{}) (messageID string, err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return "", errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		params := mapx.MapToStringSlice(mapx.StructToMap(message))

		args := r2.Args{key, "*"}

		args = args.Add(params...)

		if r, err := r2.String(conn.Do("XADD", args...)); err != nil {
			return "", errors.Wrap(err, "command err")
		} else {
			return r, nil
		}
	}
}

func XReadBlock(key, groupName, consumer string, message IMessage) (err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		var xReadResult any
		xReadResult, err = conn.Do("XREADGROUP", "GROUP", groupName, consumer, "block", "1000", "streams", key, ">")
		if err != nil {
			return errors.Wrap(err, "command err")
		}

		/// 没有最新消息，直接返回
		if xReadResult == nil {
			return ErrMessageNotFoundErr
		}

		values, err := r2.Values(xReadResult, nil)
		if err != nil {
			return errors.Wrap(err, "command err")
		}

		if len(values) == 0 {
			return nil
		}

		messageIDSlice := values[0].([]interface{})[1].([]interface{})[0].([]interface{})[0].([]byte)
		vSlice := values[0].([]interface{})[1].([]interface{})[0].([]interface{})[1].([]interface{})

		//var m struct {
		//	ID  int64  `redis:"id"`
		//	Msg string `redis:"msg"`
		//}

		messageID := string(messageIDSlice)

		err = r2.ScanStruct(vSlice, message)
		if err != nil {
			return errors.Wrap(err, "command err")
		}

		message.SetMessageID(messageID)

		return err
	}
}

func XGroupCreate(key string, groupName string) (err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		if r, err := r2.String(conn.Do("XGROUP", "CREATE", key, groupName, "$", "MKSTREAM")); err != nil {
			if IsBUSYGROUPErr(err) {
				return nil
			}
			return errors.Wrap(err, "command err")
		} else {
			if r != "OK" {
				return nil
			}
			return nil
		}
	}
}

func XACK(key, groupName, messageID string) (err error) {
	var (
		conn r2.Conn
	)

	if conn, err = pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx); err != nil {
		return errors.Wrap(err, "get redis conn error!")
	} else {
		defer func() {
			_ = conn.Close()
		}()

		if r, err := r2.Int(conn.Do("XACK", key, groupName, messageID)); err != nil {
			return errors.Wrap(err, "command err")
		} else {
			if r == 0 {
				fmt.Printf("[XACK] no message xacked")
				return nil
			}
			return nil
		}
	}
}
