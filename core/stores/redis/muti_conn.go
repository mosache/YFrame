package redis

import (
	"errors"
	"fmt"
	red "github.com/gomodule/redigo/redis"
	"strings"
	"time"
)

var (
	poolMap = make(map[string]*red.Pool)
)

func Register(conf *Config, key string) error {
	redisDialOpts := make([]red.DialOption, 0)
	redisDialOpts = append(redisDialOpts, red.DialDatabase(conf.DB))
	if len(strings.TrimSpace(conf.Password)) > 0 {
		//redisDialOpts = append(redisDialOpts, red.DialUsername(conf.UserName))
		redisDialOpts = append(redisDialOpts, red.DialPassword(conf.Password))
	}

	newPool := &red.Pool{
		Dial: func() (red.Conn, error) {
			return red.Dial("tcp", fmt.Sprintf("%s:%d",
				conf.Host, conf.Port),
				redisDialOpts...)
		},
		MaxIdle:         100,
		MaxActive:       500,
		IdleTimeout:     20 * time.Second,
		MaxConnLifetime: 60 * time.Second,
		TestOnBorrow: func(c red.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}

	if newPool == nil {
		return errors.New("init redis error")
	}
	err := ping2(newPool)
	if err != nil {
		return err
	}
	poolMap[key] = newPool
	return nil
}

func Get(key string) (*red.Pool, error) {
	if pool, ok := poolMap[key]; ok {
		return pool, nil
	} else {
		return nil, errors.New("key not exist")
	}
}

func closePool() {
	for _, p := range poolMap {
		p.Close()
	}
}
