package redis

type Config struct {
	Host     string `json:",default=127.0.0.1"`
	Port     int    `json:",default=6379"`
	UserName string `json:",optional"`
	Password string `json:",optional"`
	DB       int    `json:",default=3"`
}
