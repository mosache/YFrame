package redis

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/util/ctx"
	red "github.com/gomodule/redigo/redis"
	"strings"
	"time"
)

var (
	pool *red.Pool
)

func InitRedis(conf *Config) error {
	redisDialOpts := make([]red.DialOption, 0)
	redisDialOpts = append(redisDialOpts, red.DialDatabase(conf.DB))
	if len(strings.TrimSpace(conf.Password)) > 0 {
		//redisDialOpts = append(redisDialOpts, red.DialUsername(conf.UserName))
		redisDialOpts = append(redisDialOpts, red.DialPassword(conf.Password))
	}

	pool = &red.Pool{
		Dial: func() (red.Conn, error) {
			return red.Dial("tcp", fmt.Sprintf("%s:%d",
				conf.Host, conf.Port),
				redisDialOpts...)
		},
		MaxIdle:         100,
		MaxActive:       500,
		IdleTimeout:     20 * time.Second,
		MaxConnLifetime: 60 * time.Second,
		TestOnBorrow: func(c red.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}

	if pool == nil {
		return errors.New("init redis error")
	}

	return ping()
}

func Close() error {
	if pool != nil {
		pool.Close()
	}

	closePool()

	return nil
}

func ping() error {
	conn, err := pool.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx)

	if err != nil {
		return err
	}

	r, err := red.String(conn.Do("PING"))

	if err != nil {
		return err
	}

	if r != "PONG" {
		return errors.New("ping err")
	}

	return nil
}

func ping2(mp *red.Pool) error {
	conn, err := mp.GetContext(ctx.WithTimeOut(5 * time.Second).Ctx)

	if err != nil {
		return err
	}

	r, err := red.String(conn.Do("PING"))

	if err != nil {
		return err
	}

	if r != "PONG" {
		return errors.New("ping err")
	}

	return nil
}
