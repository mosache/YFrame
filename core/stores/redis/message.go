package redis

type IMessage interface {
	SetMessageID(messageID string)
}
