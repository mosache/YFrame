package redis

import (
	"errors"
	"strings"
)

var (
	ErrMessageNotFoundErr = errors.New("messageNotFound")
)

func IsMessageNotFound(err error) bool {
	return errors.Is(err, ErrMessageNotFoundErr)
}

func IsBUSYGROUPErr(err error) bool {
	return strings.Contains(err.Error(), "BUSYGROUP")
}
