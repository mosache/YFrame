package mysql

import (
	"errors"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

func Initialize(c Configure) error {
	cs := c.GetConfigs()

	cCount := len(cs)

	if cCount == 0 {
		return errors.New("must has at least one dsn")
	}
	sourceDSN := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", cs[0].UserName, cs[0].PassWord, cs[0].Host, cs[0].Port, cs[0].DataBaseName)
	var dbOpenErr error
	DB, dbOpenErr = gorm.Open(mysql.Open(sourceDSN), &gorm.Config{SkipDefaultTransaction: true})
	if dbOpenErr != nil {
		return dbOpenErr
	}

	if cCount > 1 {
		dbResolverConfig := dbresolver.Config{}

		cons := make([]gorm.Dialector, 0)

		for idx, c := range cs {
			/// idx ==0 已经注册为默认连接
			if idx != 0 {
				cons = append(cons,
					mysql.Open(fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", c.UserName, c.PassWord, c.Host, c.Port, c.DataBaseName)))
			}
		}

		dbResolverConfig.Replicas = cons

		return DB.Use(dbresolver.Register(dbResolverConfig))
	}

	return nil
}
