package mysql

type Config struct {
	Host         string // 数据库连接地址
	Port         int    // 端口
	UserName     string // 用户名
	PassWord     string // 密码
	DataBaseName string // 库名
	UseMetrics   bool   // 是否使用metrics
	ConnName     string // 连接名
}

type ConnConfig struct {
	ConnName string
	Write    *Config
	Read     *Config
}
