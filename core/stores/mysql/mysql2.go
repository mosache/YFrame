package mysql

import (
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/core/common/slicex"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

const (
	defaultMainConnName = "Main"
)

var (
	DbConnects map[string]*gorm.DB
	DB         *gorm.DB // 主库（主要为了兼容以前的版本）
)

func Get(connName string) *gorm.DB {
	r, ok := DbConnects[connName]
	if !ok {
		panic("failed to get conn database")
	}
	return r
}

func Initialization(c Configure2) error {
	DbConnects = make(map[string]*gorm.DB)

	cs := c.GetConfigs2()

	cCount := len(cs)

	if cCount == 0 {
		return errors.New("must has at least one dsn")
	}

	/// main
	mainConnList := slicex.Filter[*ConnConfig](cs, func(s *ConnConfig) bool {
		return s.ConnName == defaultMainConnName
	})

	/// 有main
	if len(mainConnList) == 1 {

		var mainDB *gorm.DB

		mainConn := mainConnList[0]

		if mainConn.Write == nil {
			return errors.New("must have write connection")
		}

		var (
			writeConn *Config
			readConn  *Config
		)
		writeConn = mainConn.Write

		if mainConn.Read != nil {
			readConn = mainConn.Read
		}

		sourceDSN := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", writeConn.UserName, writeConn.PassWord, writeConn.Host, writeConn.Port, writeConn.DataBaseName)
		var dbOpenErr error
		mainDB, dbOpenErr = gorm.Open(mysql.Open(sourceDSN), &gorm.Config{SkipDefaultTransaction: true})
		if dbOpenErr != nil {
			return dbOpenErr
		}

		if readConn != nil {
			dbResolverConfig := dbresolver.Config{}

			cons := make([]gorm.Dialector, 0)

			cons = append(cons,
				mysql.Open(fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", readConn.UserName, readConn.PassWord, readConn.Host, readConn.Port, readConn.DataBaseName)))

			dbResolverConfig.Replicas = cons

			err := mainDB.Use(dbresolver.Register(dbResolverConfig))
			if err != nil {
				return err
			}
		}

		DB = mainDB
		DbConnects[defaultMainConnName] = mainDB
	}

	/// others
	othersConnList := slicex.Filter[*ConnConfig](cs, func(s *ConnConfig) bool {
		return s.ConnName != defaultMainConnName
	})

	for _, e := range othersConnList {
		var dbConn *gorm.DB

		if e.Write == nil {
			return fmt.Errorf("[%s] must have write connection", e.ConnName)
		}

		var (
			writeConn *Config
			readConn  *Config
		)
		writeConn = e.Write

		if e.Read != nil {
			readConn = e.Read
		}

		sourceDSN := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", writeConn.UserName, writeConn.PassWord, writeConn.Host, writeConn.Port, writeConn.DataBaseName)
		var dbOpenErr error
		dbConn, dbOpenErr = gorm.Open(mysql.Open(sourceDSN), &gorm.Config{SkipDefaultTransaction: true})
		if dbOpenErr != nil {
			return dbOpenErr
		}

		if readConn != nil {
			dbResolverConfig := dbresolver.Config{}

			cons := make([]gorm.Dialector, 0)

			cons = append(cons,
				mysql.Open(fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", readConn.UserName, readConn.PassWord, readConn.Host, readConn.Port, readConn.DataBaseName)))

			dbResolverConfig.Replicas = cons

			err := dbConn.Use(dbresolver.Register(dbResolverConfig))
			if err != nil {
				return err
			}
		}

		DbConnects[e.ConnName] = dbConn
	}

	return nil
}
