package httpx

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
)

/*
读取http body,读取后再放回r.body中
*/
func ReadBody(r *http.Request) ([]byte, error) {
	var (
		bodyBytes []byte
		err       error
	)

	if bodyBytes, err = io.ReadAll(r.Body); err != nil {
		fmt.Printf("[read request body err]:%s\n", err)
		return nil, err
	}

	defer r.Body.Close()

	r.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))

	return bodyBytes, nil
}
