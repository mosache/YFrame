package slicex

func SliceExclude[T comparable](source, exclude []T) []T {
	r := make([]T, 0)

	excludeMap := make(map[T]struct{})

	for _, e := range exclude {
		excludeMap[e] = struct{}{}
	}

	for _, e := range source {
		if _, ok := excludeMap[e]; !ok {
			r = append(r, e)
		}
	}

	return r
}

func Unique[T comparable](source []T) []T {
	r := make([]T, 0)

	uniqueMap := make(map[T]struct{})

	for _, e := range source {
		if _, ok := uniqueMap[e]; !ok {
			r = append(r, e)
			uniqueMap[e] = struct{}{}
		}
	}

	return r
}

func Contains[T comparable](source []T, target T) bool {
	if len(source) == 0 {
		return false
	}
	var r bool

	for _, e := range source {
		if e == target {
			r = true
			break
		}
	}

	return r
}

func New[T any]() []T {
	r := make([]T, 0)
	return r
}

/*
GroupBy
将slice按照key分组
*/
func GroupBy[K comparable, V any, T []V](keyFunc func(V) K, source T) map[K]T {
	m := make(map[K]T)
	for _, e := range source {
		key := keyFunc(e)

		if _, ok := m[key]; ok {
			m[key] = append(m[key], e)
		} else {
			m[key] = T{e}
		}
	}

	return m
}

func Insert[T any](source []T, element T, index int) []T {
	length := len(source)
	if index > length {
		panic("index out of range")
	}

	newSlice := make([]T, length+1)

	newSlice[index] = element
	copy(newSlice[:index], source[:index])
	copy(newSlice[index+1:], source[index:])
	return newSlice
}
