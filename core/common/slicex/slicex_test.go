package slicex

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInsert(t *testing.T) {
	type args[T any] struct {
		source  []T
		element T
		index   int
	}
	type testCase[T any] struct {
		name string
		args args[T]
		want []T
	}
	tests := []testCase[int]{
		{
			name: "0",
			args: args[int]{
				source:  []int{1, 2},
				element: 1,
				index:   0,
			},
			want: []int{1, 1, 2},
		},

		{
			name: "1",
			args: args[int]{
				source:  []int{1, 2},
				element: 3,
				index:   1,
			},
			want: []int{1, 3, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, Insert(tt.args.source, tt.args.element, tt.args.index), "Insert(%v, %v, %v)", tt.args.source, tt.args.element, tt.args.index)
		})
	}
}
