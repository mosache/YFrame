package slicex

type Slice[T any, ReturnType any] []T

func (s Slice[T, ReturnType]) Map(mapFunc func(T) ReturnType) []ReturnType {
	r := make([]ReturnType, 0)

	for _, e := range s {
		value := mapFunc(e)
		r = append(r, value)
	}

	return r
}

func Map[SourceType, ReturnType any](source []SourceType, mapFunc func(SourceType) ReturnType) []ReturnType {
	r := make([]ReturnType, 0)

	for _, e := range source {
		value := mapFunc(e)
		r = append(r, value)
	}

	return r
}

func Filter[SourceType any](source []SourceType, filter func(s SourceType) bool) []SourceType {
	r := make([]SourceType, 0)

	for _, e := range source {
		if filter(e) {
			r = append(r, e)
		}
	}
	return r
}

func Reduce[T comparable, Source any](initVal T, reducer func(val T, v Source) T, source []Source) T {
	for _, e := range source {
		initVal = reducer(initVal, e)
	}

	return initVal
}
