/*
切片转字符串相关
*/
package slicex

import (
	"gitee.com/mosache/YFrame/core/common/strx"
	"strings"
)

func ToStrSlice[T any](s []T) []string {
	var r []string

	slice := make(Slice[T, string], 0)

	for _, e := range s {
		slice = append(slice, e)
	}

	r = slice.Map(func(e T) string {
		return strx.ToStr(e)
	})

	return r

}

func Join[T any](source []T, sep string) string {
	if source == nil {
		return ""
	}

	return strings.Join(ToStrSlice(source), sep)
}
