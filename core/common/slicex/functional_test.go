package slicex

import (
	"fmt"
	"gitee.com/mosache/YFrame/core/common/strx"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMap(t *testing.T) {
	array := strx.ToStrArray("1,2,3,4", ",")
	ints := Map(array, func(s string) int {
		return strx.ToInt(s)
	})

	assert.Equal(t, ints, []int{1, 2, 3, 4})

}

func TestReduce(t *testing.T) {

	type args[T comparable, Source any] struct {
		initVal T
		reducer func(val T, v Source) T
		source  []Source
	}
	type testCase[T comparable, Source any] struct {
		name string
		args args[T, Source]
		want T
	}

	tests := []testCase[any, any]{
		{
			name: "number",
			args: args[any, any]{
				initVal: 0,
				reducer: func(val any, v any) any {
					return val.(int) + v.(int)
				},
				source: []any{1, 2, 3},
			},
			want: 6,
		},

		{
			name: "str",
			args: args[any, any]{
				initVal: "",
				reducer: func(val any, v any) any {
					return fmt.Sprintf("%s%s", val.(string), v.(string))
				},
				source: []any{"h", "e", "l", "l", "o"},
			},
			want: "hello",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, Reduce(tt.args.initVal, tt.args.reducer, tt.args.source), "Reduce(%v, %v, %v)", tt.args.initVal, tt.args.reducer, tt.args.source)
		})
	}
}
