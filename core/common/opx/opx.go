package opx

import "reflect"

/**
opx 操作符
*/

func Optional[T any](source T, defaultVal T) T {
	typ := reflect.TypeOf(source)

	if typ.Kind() == reflect.Ptr {
		if reflect.ValueOf(source).IsZero() {
			return defaultVal
		}

		return source
	}

	switch typ.Kind() {
	case reflect.String:
		if length := reflect.ValueOf(source).Len(); length == 0 {
			return defaultVal
		}
	default:
		return source
	}
	return source
}
