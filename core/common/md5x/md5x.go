package md5x

import (
	"crypto/md5"
	"fmt"
)

func GetMd5(source string) string {
	sum := md5.Sum([]byte(source))
	return fmt.Sprintf("%X", sum)
}
