package strx

import (
	"strconv"
	"strings"
)

func Deref(s *string) string {
	if s == nil {
		return ""
	}

	return *s
}

func ToStr(s any) string {
	var r string
	switch s.(type) {
	case float64:
		value := s.(float64)
		r = strconv.FormatFloat(value, 'E', -1, 64)
	case float32:
		value := s.(float32)
		r = strconv.FormatFloat(float64(value), 'E', -1, 32)
	case int:
		value := s.(int)
		r = strconv.Itoa(value)
	case int64:
		value := s.(int64)
		r = strconv.FormatInt(value, 10)
	case string:
		r = s.(string)
	}
	return r
}

func ToStrArray(s string, sep string) []string {
	r := make([]string, 0)

	split := strings.Split(s, sep)

	for _, e := range split {
		if len(e) > 0 {
			r = append(r, e)
		}
	}

	return r
}

func ToInt(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		return 0
	}

	return i
}

func ToFloat64(s string) float64 {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0
	}

	return f
}
