package timex

var (
	weekDayShortMap = map[string]string{
		"Mon": "周一",
		"Tue": "周二",
		"Wed": "周三",
		"Thu": "周四",
		"Fri": "周五",
		"Sat": "周六",
		"Sun": "周日",
	}
)

func WeekToChinese(week string) string {
	return weekDayShortMap[week]
}
