package uuidx

import uuid "github.com/satori/go.uuid"

func UUID() string {
	u := uuid.NewV4()
	return u.String()
}
