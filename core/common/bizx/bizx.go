// Package bizx
// 业务相关函数
// /*
package bizx

import (
	"context"
	"fmt"
	"reflect"
)

/*
InitPtrStruct
初始化指针，有slice就初始化一个空的slice
*/
func InitPtrStruct[T any](target T) T {
	tType := reflect.TypeOf(target)

	allocTarget := reflect.New(tType.Elem()).Interface().(T)

	t := reflect.ValueOf(allocTarget)

	for i := 0; i < t.Elem().NumField(); i++ {
		field := t.Elem().Field(i)
		if field.Type().Kind() == reflect.Slice { /// 是slice的话，要初始化
			if field.CanSet() {
				field.Set(reflect.MakeSlice(field.Type(), 0, 0))
			}
		}

		if field.Type().Kind() == reflect.Ptr { /// 如果是指针，去初始化
			value := reflect.New(field.Type()).Elem().Interface()
			value = InitPtrStruct(value)
			if field.CanSet() {
				field.Set(reflect.ValueOf(value))
			}
		}
	}

	return allocTarget

}

func GetValueFromContext[T any](ctx context.Context, key string) T {
	val, ok := ctx.Value(key).(T)
	if !ok {
		panic(fmt.Sprintf("[get value from ctx err] key:%s", key))
	}

	return val
}
