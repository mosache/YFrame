package intx

type INTS interface {
	int | int8 | int16 | int32 | int64
}

func IntToBool[T INTS](i T) bool {
	if i == 1 {
		return true
	}

	return false
}
