package mrx

import "github.com/zeromicro/go-zero/core/mr"

func Finish(funcs []func() error, max int) error {
	return mr.MapReduceVoid(func(source chan<- func() error) {
		for _, fn := range funcs {
			source <- fn
		}
	}, func(fn func() error, writer mr.Writer[any], cancel func(error)) {
		if err := fn(); err != nil {
			cancel(err)
		}
	}, func(pipe <-chan any, cancel func(error)) {
	}, mr.WithWorkers(max))
}
