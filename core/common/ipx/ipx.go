package ipx

import (
	"errors"
	"gitee.com/mosache/YFrame/core/http"
	gHttp "net/http"
)

const (
	headerIPKey = "X-Real-Ip"
)

func IPFromHeader(header gHttp.Header) string {
	realIP := header.Get(headerIPKey)
	return realIP
}

func IPLocation(ip string) (string, error) {
	return getIpLocation(ip)
}

///===================================================================================

/*
get ip from https://ip.useragentinfo.com/json?ip=218.81.119.141
*/
func getIpLocation(ip string) (string, error) {
	var r IPResult
	if err := http.DoReq("https://ip.useragentinfo.com/json", map[string]string{"ip": ip}, http.MethodGet, &r); err != nil {
		return "", err
	}

	if r.Code != 200 {
		return "", errors.New(r.Desc)
	}

	return r.Province, nil
}

type IPResult struct {
	Country   string `json:"country"`
	ShortName string `json:"short_name"`
	Province  string `json:"province"`
	City      string `json:"city"`
	Area      string `json:"area"`
	Isp       string `json:"isp"`
	Net       string `json:"net"`
	Ip        string `json:"ip"`
	Code      int    `json:"code"`
	Desc      string `json:"desc"`
}
