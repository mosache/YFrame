package http

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/mosache/YFrame/core/common/slicex"
	"io/ioutil"
	"net/http"
	url2 "net/url"
	"strings"
	"time"
)

const (
	MethodGet  = "GET"
	MethodPost = "POST"
)

type RequestMethod string

var (
	Client http.Client
)

func init() {
	Client = http.Client{}
}

type ReqOptions func(req *http.Request)

func DoReq(url string, params map[string]string, method RequestMethod, respStruct interface{}, options ...ReqOptions) error {
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)

	ps := make([]string, 0)

	///有参数
	if len(params) > 0 {
		for k, v := range params {
			ps = append(ps, fmt.Sprintf("%s=%s", k, v))
		}
	}

	var (
		req    *http.Request
		reqErr error
	)

	if method == MethodGet {
		if len(params) > 0 {
			url = fmt.Sprintf("%s?%s", url, strings.Join(slicex.Map(ps, func(s string) string {
				sps := strings.Split(s, "=")
				if len(sps) != 2 {
					return ""
				}
				return fmt.Sprintf("%s=%s", sps[0], url2.QueryEscape(sps[1]))
			}), "&"))
		}
		req, reqErr = http.NewRequestWithContext(ctx, string(method), url, nil)
	} else if method == MethodPost {
		body := strings.NewReader(strings.Join(ps, "&"))
		req, reqErr = http.NewRequestWithContext(ctx, string(method), url, body)
	}

	if reqErr != nil {
		return reqErr
	}

	for _, o := range options {
		o(req)
	}

	resp, respErr := Client.Do(req)

	if respErr != nil {
		return respErr
	}

	defer resp.Body.Close()

	respBytes, respReadErr := ioutil.ReadAll(resp.Body)

	if respReadErr != nil {
		return respReadErr
	}

	if err := json.Unmarshal(respBytes, respStruct); err != nil {
		return err
	}

	return nil
}

func PostJson(url string, params interface{}, respStruct interface{}, options ...ReqOptions) error {
	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)

	var (
		req    *http.Request
		reqErr error
	)

	bodyBytes, jsonErr := json.Marshal(params)

	if jsonErr != nil {
		return jsonErr
	}

	body := bytes.NewReader(bodyBytes)
	req, reqErr = http.NewRequestWithContext(ctx, "POST", url, body)

	if reqErr != nil {
		return reqErr
	}

	for _, o := range options {
		o(req)
	}

	resp, respErr := Client.Do(req)

	if respErr != nil {
		return respErr
	}

	defer resp.Body.Close()

	respBytes, respReadErr := ioutil.ReadAll(resp.Body)

	if respReadErr != nil {
		return respReadErr
	}

	if err := json.Unmarshal(respBytes, respStruct); err != nil {
		return err
	}

	return nil
}

func DoBufferReq(url string, params map[string]interface{}, method RequestMethod, options ...ReqOptions) ([]byte, error) {
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)

	ps := make([]string, 0)

	///有参数
	if len(params) > 0 {
		for k, v := range params {
			ps = append(ps, fmt.Sprintf("%s=%s", k, v))
		}
	}

	var (
		req    *http.Request
		reqErr error
	)

	if method == MethodGet {
		if len(params) > 0 {
			url = fmt.Sprintf("%s?%s", url, strings.Join(ps, "&"))
		}
		req, reqErr = http.NewRequestWithContext(ctx, string(method), url, nil)
	} else if method == MethodPost {
		body, _ := json.Marshal(params)
		bs := strings.NewReader(string(body))
		req, reqErr = http.NewRequestWithContext(ctx, string(method), url, bs)
	}

	if reqErr != nil {
		return nil, reqErr
	}

	for _, o := range options {
		o(req)
	}

	resp, respErr := Client.Do(req)

	if respErr != nil {
		return nil, respErr
	}

	defer resp.Body.Close()

	respBytes, respReadErr := ioutil.ReadAll(resp.Body)

	if respReadErr != nil {
		return nil, respReadErr
	}

	/// 出错了。tx返回了错误
	if strings.Contains(resp.Header.Get("Content-Type"), "application/json") {
		rMap := make(map[string]interface{})
		getErrError := json.Unmarshal(respBytes, &rMap)
		if getErrError != nil {
			return nil, getErrError
		}

		errMsg, ok := rMap["errmsg"].(string)
		if !ok {
			return nil, errors.New("小程序码获取错误信息失败")
		}
		return nil, errors.New(errMsg)

	}

	return respBytes, nil
}
