package gPlus

import (
	gMysql "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"testing"
)

type testModel struct{}

func TestQuerier_Select(t *testing.T) {
	db, _ := gorm.Open(gMysql.Open("root:123456@tcp(127.0.0.1:3306)/test?charset=utf8&parseTime=True&loc=Local"))

	testCases := []struct {
		name    string
		columns []expr
		q       func() *Querier[testModel]
	}{
		{
			name:    "normal",
			columns: []expr{Column("id")},
			q: func() *Querier[testModel] {
				q := newQuerier[testModel](db)
				return q
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			sql := tc.q().Select(tc.columns...).SQL()
			t.Log(sql)
		})
	}
}
