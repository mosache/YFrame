package gPlus

type Query[T any] interface {
	Save(m *T) error // 保存model或[]model

	SaveIgnore(m *T) error // 保存model或[]model

	ItemByField(params FieldMap) (*T, error)

	Item() (*T, error) // 获取单个

	Lists() ([]T, error) // 获取多个

	List(params FieldMap) ([]T, error)

	Count(params FieldMap) (int64, error)

	In(params FieldMap) ([]T, error)

	Paging(page, limit int, params FieldMap) ([]T, int, error)

	Table(table string) *Querier[T]

	Where(params FieldMap) *Querier[T]

	Having(params FieldMap) *Querier[T]

	OR(params FieldMap) *Querier[T]

	Between(c column, start, end any) *Querier[T]

	Raw(sql string, values ...interface{}) *Querier[T]

	SQL() string

	Update(params FieldMap) error

	Delete(params FieldMap) error

	Del(params FieldMap) error

	In2(c column, data any) *Querier[T]

	Debug() *Querier[T]
}
