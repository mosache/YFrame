/*
gPlus gorm封装
*/
package gPlus

import (
	"gitee.com/mosache/YFrame/core/stores/mysql"
	"gorm.io/gorm"
)

type gp struct {
	db *gorm.DB
}

var g *gp

func Initialize(c mysql.Configure) error {
	//// mysql
	if err := mysql.Initialize(c); err != nil {
		return err
	}

	g = &gp{
		db: mysql.DB,
	}

	return nil
}

func Initialization(c mysql.Configure2) error {
	//// mysql
	if err := mysql.Initialization(c); err != nil {
		return err
	}

	g = &gp{
		db: mysql.DB,
	}

	return nil
}

func New[M T](opts ...Option[M]) *Querier[M] {
	return newQuerier[M](g.db)
}
