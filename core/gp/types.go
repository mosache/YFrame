package gp

import "fmt"

type expr interface {
	expr() string
}

type column struct {
	name  string
	alias string
}

func (c column) expr() string {
	if len(c.alias) > 0 {
		return fmt.Sprintf("%s AS %s", c.name, c.alias)
	}
	return c.name
}

func (c column) AS(alias string) column {
	return column{
		name:  c.name,
		alias: alias,
	}
}
