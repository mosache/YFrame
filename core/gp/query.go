package gp

type Query[T any] interface {
	Save(m *T) error // 保存model或[]model

	Item() (*T, error)
	List() (T, error)
	Count() (int64, error)
	Paging(page, limit int, params FieldMap) ([]T, int, error)

	Where(params FieldMap) *Querier[T]
	In(params FieldMap) *Querier[T]
	Between(c column, start, end any) *Querier[T]
	Update(params FieldMap) error
	Delete(params FieldMap) error
	Del(params FieldMap) error

	SQL() string

	Debug() *Querier[T]
}
