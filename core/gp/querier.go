package gp

import (
	"fmt"
	"gitee.com/mosache/YFrame/core/common/slicex"
	"gitee.com/mosache/YFrame/core/stores/mysql"
	"github.com/zeromicro/go-zero/core/mr"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"reflect"
)

type T interface {
	any
}

type Querier[M T] struct {
	tx *gorm.DB
}

func newQuerier[M T](db *gorm.DB) *Querier[M] {
	q := &Querier[M]{
		tx: db,
	}

	return q
}

type Option[M T] func(*Querier[M])

type FieldMap map[string]any

func (q *Querier[T]) WithTxOption(tx *gorm.DB) *Querier[T] {
	q.tx = tx
	return q
}

// ==========================================================================
func (q *Querier[T]) Select(columns ...expr) *Querier[T] {
	cols := slicex.Map(columns, func(v expr) string {
		return v.expr()
	})
	q.tx = q.tx.Select(cols)
	return q
}

func (q *Querier[T]) OrderBy(orderBy string) *Querier[T] {
	q.tx = q.tx.Order(orderBy)
	return q
}

func (q *Querier[T]) Limit(limit int) *Querier[T] {
	q.tx = q.tx.Limit(limit)
	return q
}

func (q *Querier[T]) OffSet(offSet int) *Querier[T] {
	q.tx = q.tx.Offset(offSet)
	return q
}

func (q *Querier[T]) Where(params FieldMap) *Querier[T] {
	var r T

	q.tx = q.tx.Model(&r)

	for k, v := range params {
		if len(k) > 0 {
			q.tx = q.tx.Where(k, v)
		} else {
			q.tx = q.tx.Where(v)
		}
	}
	return q
}

func (q *Querier[T]) Between(c column, start, end any) *Querier[T] {
	q.tx = q.tx.Where(fmt.Sprintf("%s BETWEEN ? AND ?", c.name), start, end)
	return q
}

func (q *Querier[T]) In(params FieldMap) *Querier[T] {

	for k, v := range params {
		if len(k) == 0 {
			q.tx = q.tx.Where(fmt.Sprintf("%v", v))
		} else {
			q.tx = q.tx.Where(fmt.Sprintf("%s IN ?", k), v)
		}
	}

	return q
}

// /=======================================================================================================
var _ Query[T] = &Querier[T]{}

func (q *Querier[T]) Update(params FieldMap) error {
	for k, v := range params {
		q.tx = q.tx.UpdateColumn(k, fmt.Sprintf("%v", v))
	}
	return q.tx.Error
}

func (q *Querier[T]) Save(m *T) error {
	val := reflect.ValueOf(m)
	if val.Kind() != reflect.Ptr {
		return fmt.Errorf("m must be a pointer to a struct")
	}

	modelVal := val.Elem()
	eKind := modelVal.Kind()
	if eKind == reflect.Slice {
		if modelVal.Len() == 0 {
			/// 空数组直接返回
			return nil
		}
	}
	return q.tx.Clauses(clause.OnConflict{UpdateAll: true}).Create(m).Error
}

func (q *Querier[T]) Item() (*T, error) {
	var r T

	db := q.tx.Model(&r)

	if err := db.Scan(&r).Error; err != nil {
		return nil, err
	}

	return &r, nil
}
func (q *Querier[T]) List() (T, error) {
	var r T

	r = reflect.MakeSlice(reflect.TypeOf(r), 0, 0).Interface().(T)

	db := q.tx.Model(&r)

	if err := db.Scan(&r).Error; err != nil {
		return r, err
	}

	return r, nil
}

func (q *Querier[M]) Count() (int64, error) {
	var (
		result int64
		r      M
	)

	db := q.tx.Model(&r)

	if err := db.Count(&result).Error; err != nil {
		return 0, err
	}

	return result, nil

}

func (q *Querier[T]) Paging(page, limit int, params FieldMap) ([]T, int, error) {
	var (
		r     = make([]T, 0)
		total int64
	)

	db := q.tx.Model(&r)
	//db2 := q.tx.Model(&r)
	for k, v := range params {
		if len(k) > 0 {
			db.Where(k, v)
		} else {
			db.Where(v)
		}
	}

	if err := mr.Finish(func() error {
		offSet := (page - 1) * limit
		if err := db.Offset(offSet).Limit(limit).Scan(&r).Error; err != nil {
			return err
		}
		return nil
	}, func() error {
		totalDB := mysql.DB.Model(&r)
		for k, v := range params {
			if len(k) > 0 {
				totalDB.Where(k, v)
			} else {
				totalDB.Where(v)
			}
		}

		if err := totalDB.Count(&total).Error; err != nil {
			return err
		}
		return nil
	}); err != nil {
		return nil, 0, err
	}

	return r, int(total), nil
}

func (q *Querier[T]) Delete(params FieldMap) error {
	db := q.tx.Model(new(T))

	for k, v := range params {
		if len(k) == 0 {
			db.Where(fmt.Sprintf("%v", v))
		} else {
			db.Where(fmt.Sprintf("%s IN ?", k), v)
		}
	}

	if err := db.Delete(new(T)).Error; err != nil {
		return err
	}

	return nil
}

func (q *Querier[T]) Del(params FieldMap) error {
	db := q.tx.Model(new(T))

	for k, v := range params {
		if len(k) == 0 {
			db.Where(fmt.Sprintf("%v", v))
		} else {
			db.Where(fmt.Sprintf("%s = ?", k), v)
		}
	}

	if err := db.Delete(new(T)).Error; err != nil {
		return err
	}

	return nil
}

func (q *Querier[T]) SQL() string {
	return q.tx.Statement.SQL.String()
}

func (q *Querier[T]) Debug() *Querier[T] {
	q.tx = q.tx.Debug()
	return q
}
