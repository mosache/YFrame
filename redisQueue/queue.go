package queue

import "gitee.com/mosache/YFrame/core/stores/redis"

type Queue interface {
	/*
		消息入队，返回消息id
	*/
	Enqueue(key string, message redis.IMessage) (string, error)
	Read(key, groupName string, message redis.IMessage) error
	Start(queueKey, queueGroupName string) error
}

var (
	q Queue
)
