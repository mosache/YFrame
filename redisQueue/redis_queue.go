package queue

import (
	"gitee.com/mosache/YFrame/core/stores/redis"
	"github.com/zeromicro/go-zero/core/netx"
)

const (
	outPackageNumNeedNum = 50
)

type redisQueue struct {
}

func StartRedisQueue(queueKey, queueGroupName string) error {
	q = redisQueue{}

	return q.Start(queueKey, queueGroupName)
}

func (r redisQueue) Enqueue(key string, message redis.IMessage) (string, error) {
	messageID, err := redis.XAdd(key, message)
	if err != nil {
		return "", err
	}
	return messageID, nil
}

func (r redisQueue) Read(key, groupName string, message redis.IMessage) error {
	//var message message.ShareClickLogMessage

	consumer := netx.InternalIp()
	err := redis.XReadBlock(key, groupName, consumer, message)
	if err != nil {
		return err
	}

	return nil
}

func (r redisQueue) Start(queueKey, queueGroupName string) error {
	/// 新建空队列和消费组

	err := redis.XGroupCreate(queueKey, queueGroupName)

	if err != nil {
		return err
	}

	err = redis.XGroupCreate(queueKey, queueGroupName)

	if err != nil {
		return err
	}

	return nil
}
